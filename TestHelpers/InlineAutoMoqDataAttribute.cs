﻿using AutoFixture.Xunit2;

namespace TestHelpers
{
    public class InlineAutoMoqDataAttribute : InlineAutoDataAttribute
    {
        public InlineAutoMoqDataAttribute(params object[] objects) : base(new AutoMoqDataAttribute(), objects) { }
    }

    //public class MemberDataAutoMoqDataAttribute : CompositeDataAttribute
    //{
    //    public MemberDataAutoMoqDataAttribute(string memberName, params object[] parameters) : 
    //        base(new MemberDataAttribute(memberName, parameters), new AutoMoqDataAttribute())
    //    {
            
    //    }
    //    public MemberDataAutoMoqDataAttribute(string memberName, Type memberType, params object[] parameters) :
    //        base(new MemberDataAttribute(memberName, parameters) { MemberType = memberType }, new AutoMoqDataAttribute())
    //    {

    //    }

    //}
}
