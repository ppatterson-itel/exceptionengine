﻿namespace TestHelpers
{
    public class PropertyValueMatchesResult
    {
        public bool PropertyValueMatches { get; set; }
        public string ResultMessage { get; set; }
    }
}
