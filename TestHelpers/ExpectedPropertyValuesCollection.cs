﻿using System.Collections.Generic;

namespace TestHelpers
{
    public class ExpectedPropertyValuesCollection
    {
        private Dictionary<int, ExpectedPropertyValues> _expectedPropertyValuesDictionary = new Dictionary<int, ExpectedPropertyValues>();

        public ExpectedPropertyValues GetExpectedPropertyValues(int primaryKey)
        {
            if (!_expectedPropertyValuesDictionary.ContainsKey(primaryKey))
            {
                return null;
            }

            return _expectedPropertyValuesDictionary[primaryKey];
        }

        public void Add(int primaryKey, ExpectedPropertyValues expectedPropertyValues)
        {
            if (!_expectedPropertyValuesDictionary.ContainsKey(primaryKey))
            {
                _expectedPropertyValuesDictionary.Add(primaryKey, expectedPropertyValues);
            }
            else
            {
                _expectedPropertyValuesDictionary[primaryKey] = expectedPropertyValues;
            }
        }
    }
}
