﻿using System.Collections.Generic;
using System.Linq;

namespace TestHelpers
{
    public static class PropertyValuesValidator
    {
        public static IEnumerable<string> GetInvalidProperties<T>(T objectToValidate,
            ExpectedPropertyValues expectedPropertyValues)
        {
            List<string> unmatchedProperties = new List<string>();

            var properties = objectToValidate.GetType().GetProperties()
                .Where(pi => !(typeof(System.Collections.IEnumerable).IsAssignableFrom(pi.PropertyType) && (pi.PropertyType != typeof(string))))
                .ToArray();

            foreach (var property in properties)
            {
                var actual = property.GetValue(objectToValidate, null);
                var propertyMatchesResponse = expectedPropertyValues.PropertyValueMatches(property.Name, actual);

                if(!propertyMatchesResponse.PropertyValueMatches)
                    unmatchedProperties.Add(propertyMatchesResponse.ResultMessage);
            }

            return unmatchedProperties;
        }

        public static IEnumerable<string> GetUnmatchedProperties<T>(T objectToValidate,
            ExpectedPropertyValuesCollection expectedPropertyValuesCollection,
            int primaryKey)
        {
            var expectedPropertyValues = expectedPropertyValuesCollection.GetExpectedPropertyValues(primaryKey);

            if (expectedPropertyValues == null)
                return new List<string>()
                {
                    $"Primary key {primaryKey} wasn't found in the expected property values collection."
                };

            return GetInvalidProperties(objectToValidate, expectedPropertyValues);
        }
    }
}
