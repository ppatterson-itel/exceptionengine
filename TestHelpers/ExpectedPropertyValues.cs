﻿using System.Collections.Generic;

namespace TestHelpers
{
    public class ExpectedPropertyValues
    {
        private readonly IDictionary<string, dynamic> _expectedPropertyValues = new Dictionary<string, dynamic>();

        public void Add(string propertyName, dynamic propertyValue)
        {
            var lookupKey = propertyName.ToLowerInvariant();

            if (!_expectedPropertyValues.ContainsKey(lookupKey))
            {
                _expectedPropertyValues.Add(lookupKey, propertyValue);
            }
            else
            {
                // maybe throw an exception instead?
                _expectedPropertyValues[lookupKey] = propertyValue;
            }
        }

        public PropertyValueMatchesResult PropertyValueMatches(string propertyName, dynamic actualPropertyValue)
        {
            var lookupKey = propertyName.ToLowerInvariant();

            if (!_expectedPropertyValues.ContainsKey(lookupKey))
                return GetValueMatchesResult(false, $"Property {propertyName} wasn't found in the expected values collection.");

            var expectedPropertyValue = _expectedPropertyValues[lookupKey];
            if (expectedPropertyValue == null)
            {
                if (actualPropertyValue != null)
                {
                    return GetValueMatchesResult(false, $"Property mismatch for [{propertyName}]; expectedValue [{expectedPropertyValue}], actual value [{actualPropertyValue}]");
                }
            }
            else if (!expectedPropertyValue.Equals(actualPropertyValue))
            {
                return GetValueMatchesResult(false, $"Property mismatch for [{propertyName}]; expectedValue [{expectedPropertyValue}], actual value [{actualPropertyValue}]");
            }

            return GetValueMatchesResult(true, null);
        }

        private PropertyValueMatchesResult GetValueMatchesResult(bool matches, string resultMessage)
        {
            return new PropertyValueMatchesResult
            {
                PropertyValueMatches = matches,
                ResultMessage = resultMessage
            };
        }
    }
}
