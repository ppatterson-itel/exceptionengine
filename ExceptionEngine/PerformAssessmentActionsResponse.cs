﻿using System.Collections.ObjectModel;

namespace ExceptionEngine
{
    public class PerformAssessmentActionsResponse
    {
        public Collection<ActionsResponse> ActionsResponses { get; } = new Collection<ActionsResponse>();
    }
}