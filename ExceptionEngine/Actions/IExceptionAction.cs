﻿namespace ExceptionEngine.Actions
{
    public interface IExceptionAction
    {
        ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId);
    }
}
