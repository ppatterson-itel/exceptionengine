﻿using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExceptionEngine.Actions
{
    public class SuppressReportSectionAction : ExceptionActionBase
    {
        public SuppressReportSectionAction(IActionRepository actionRepository, ExceptionActionRequest action, int assessmentId, int exceptionCaseId, string description)
            : base(actionRepository, action, assessmentId, exceptionCaseId, description)
        {
            
        }

        public override ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId)
        {
            if (exceptionAlreadyProcessed)
                return new ExceptionActionResponse
                {
                    ActionSkipped = true,
                    AssessmentExceptionId = assessmentExceptionId
                };

            var exceptionSuppressReportSections = ActionRepository.LoadExceptionSuppressReportSections(ExceptionActionRequest.ExceptionMappingId);
            var assessmentExceptionSuppressReportSections = ActionRepository.LoadAssessmentExceptionSuppressReportSections(AssessmentId).ToList();

            var actionsDescription = new StringBuilder();
            actionsDescription.Append("Suppress Report Section: ");

            ExceptionActionResponse response = new ExceptionActionResponse
            {
                AssessmentExceptionId = assessmentExceptionId
            };

            foreach (var reportSection in exceptionSuppressReportSections)
            {
                if (IsDuplicateReportSection(reportSection, assessmentExceptionSuppressReportSections)) continue;

                actionsDescription.Append($"{reportSection.ReportSection}; ");

                response.AssessmentExceptionId = ActionRepository.CreateAssessmentSuppressReport(new AssessmentSuppressReportRequest
                {
                    AssessmentId = AssessmentId,
                    Description = Description,
                    ExceptionCaseId = ExceptionCaseId,
                    ReportSection = reportSection.ReportSection
                });
            }

            response.ActionsTaken = actionsDescription.ToString();

            return response;

        }

        private static bool IsDuplicateReportSection(ExceptionSuppressReportSection reportSection, IEnumerable<AssessmentExceptionSuppressReportSection> assessmentExceptionSuppressReportSections)
        {
            return assessmentExceptionSuppressReportSections.Any(assessmentReportSection => EqualityEvaluator.AreEqual(reportSection.ReportSection, assessmentReportSection.ReportSection));
        }
    }
}