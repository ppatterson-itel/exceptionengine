﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Actions
{
    public class ReportCommentAction : ExceptionActionBase
    {
        private readonly int _userId;
        public ReportCommentAction(IActionRepository actionRepository, ExceptionActionRequest exceptionActionRequest, int assessmentId, 
            int exceptionCaseId, string description, int userId) 
            : base(actionRepository, exceptionActionRequest, assessmentId, exceptionCaseId, description)
        {
            _userId = userId;
        }

        public override ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId)
        {
            if (exceptionAlreadyProcessed)
                return new ExceptionActionResponse
                {
                    ActionSkipped = true
                };

            var exceptionReportComments = ActionRepository.LoadExceptionReportComments(ExceptionActionRequest.ExceptionMappingId);
            IEnumerable<AssessmentComment> assessmentComments = ActionRepository.LoadAssessmentComments(AssessmentId).ToList();

            var actionsDescription = new StringBuilder();
            actionsDescription.Append("Report Comment: ");

            var response = new ExceptionActionResponse
            {
                AssessmentExceptionId = assessmentExceptionId
            };

            foreach (var exceptionReportComment in exceptionReportComments)
            {
                var createNew = assessmentComments.All(assessmentComment => !CommentsAreEqual(exceptionReportComment, assessmentComment));

                if (!createNew) continue;

                if (exceptionReportComment != null)
                    actionsDescription.Append($"{exceptionReportComment.Comment}; ");

                response.AssessmentExceptionId = ActionRepository.CreateAssessmentComment(new AssessmentCommentRequest
                {
                    AssessmentId = AssessmentId,
                    Comment = exceptionReportComment?.Comment,
                    Description = Description,
                    ExceptionCaseId = ExceptionCaseId,
                    ReportSection = exceptionReportComment?.ReportSection,
                    SourceCommentId = exceptionReportComment?.ReportCommentId,
                    UserId = _userId
                });
            }

            response.ActionsTaken = actionsDescription.ToString();

            return response;

        }

        private static bool CommentsAreEqual(ExceptionReportComment exceptionReportComment, AssessmentComment assessmentComment)
        {
            return EqualityEvaluator.AreEqual(exceptionReportComment.Comment, assessmentComment.Comment)
                && EqualityEvaluator.AreEqual(exceptionReportComment.ReportSection, assessmentComment.ReportSection);
        }
    }
}
