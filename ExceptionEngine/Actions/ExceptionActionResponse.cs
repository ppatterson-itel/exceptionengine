﻿namespace ExceptionEngine.Actions
{
    public class ExceptionActionResponse
    {
        public string ConditionsMet { get; set; }
        public string ActionsTaken { get; internal set; }
        public bool ActionSkipped { get; set; }
        public int? AssessmentExceptionId { get; set; }
    }
}