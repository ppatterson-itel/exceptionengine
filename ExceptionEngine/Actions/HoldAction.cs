﻿using System;
using System.Text;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Actions
{
    public class HoldAction : ExceptionActionBase
    {
        private readonly int _userId;
        private readonly SystemSettings _systemSettings;

        public HoldAction(IActionRepository actionRepository, ExceptionActionRequest exceptionActionRequest, int assessmentId, 
            int exceptionCaseId, string description, int userId, SystemSettings systemSettings) 
            : base(actionRepository, exceptionActionRequest, assessmentId, exceptionCaseId, description)
        {
            _userId = userId;
            _systemSettings = systemSettings ?? throw new ArgumentNullException(nameof(systemSettings));
        }

        public override ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId)
        {
            if (ExceptionActionRequest.IncludeNoteWithException)
            {
                ActionRepository.CreateAssessmentNote(CreateAssessmentNote());
            }

            if (exceptionAlreadyProcessed)
                return new ExceptionActionResponse
                {
                    ActionSkipped = true
                };

            var actionsDescription = new StringBuilder();
            actionsDescription.Append(ClearAssessmentHolds());

            var holdType = ActionRepository.LoadHoldType(ExceptionActionRequest.ExceptionMappingId);

            if (!ActionRepository.AssessmentHoldExists(AssessmentId, holdType.HoldTypeId))
            {
                var holdReason = ActionRepository.LoadHoldReason(ExceptionActionRequest.ExceptionMappingId);
                actionsDescription.Append($"Create Hold: {holdType.DisplayText}; ");
                int? holdReasonId = null;
                if (null != holdReason)
                {
                    actionsDescription.Append($"HoldReason: {holdReason.Reason}; ");
                    holdReasonId = holdReason.HoldReasonId;
                }

                ActionRepository.CreateAssessmentHold(new AssessmentHoldRequest
                {
                    AssessmentId = AssessmentId,
                    HoldTypeId = holdType.HoldTypeId,
                    UserId = _userId,
                    IsManualHold = false,
                    HoldReasonId = holdReasonId
                });
                actionsDescription.Append(CreateAssessmentHoldCommentNotes());
            }

            ActionRepository.SetAssessmentStatusToHold(AssessmentId, _systemSettings.HoldStatusName);

            return new ExceptionActionResponse
            {
                ActionsTaken = actionsDescription.ToString(),
                AssessmentExceptionId = assessmentExceptionId
            };

        }

        private string CreateAssessmentHoldCommentNotes()
        {
            var holdCommentsDescription = new StringBuilder();

            var exceptionHoldComments = ActionRepository.LoadExceptionHoldComments(ExceptionActionRequest.ExceptionMappingId);

            holdCommentsDescription.Append("Report Comments for Hold: ");

            foreach (var mappingHoldComment in exceptionHoldComments)
            {
                holdCommentsDescription.Append($"{mappingHoldComment.Comment}; ");

                ActionRepository.CreateAssessmentNote(CreateAssessmentNote(mappingHoldComment.Comment));
            }

            return holdCommentsDescription.ToString();
        }

        private string ClearAssessmentHolds()
        {
            var clearHoldsStringBuilder = new StringBuilder();

            var exceptionHoldTypes = ActionRepository.LoadExceptionHoldTypes(ExceptionActionRequest.ExceptionMappingId);
            var firstHoldAdded = false;

            foreach (var mappingHoldType in exceptionHoldTypes)
            {
                var numberOfHoldsCleared = ActionRepository.ClearHoldsByHoldType(AssessmentId, mappingHoldType.HoldTypeId, GetUserId(_userId, _systemSettings.SystemUserId));

                if (numberOfHoldsCleared <= 0) continue;

                if (!firstHoldAdded)
                {
                    clearHoldsStringBuilder.Append("Clear Assessment Holds before validation with type: ");
                }

                if (firstHoldAdded)
                {
                    clearHoldsStringBuilder.Append(", ");
                }

                clearHoldsStringBuilder.Append(mappingHoldType.DisplayText);
                firstHoldAdded = true;
            }

            if (firstHoldAdded)
            {
                clearHoldsStringBuilder.Append("; ");
            }

            return clearHoldsStringBuilder.ToString();
        }

        private AssessmentNoteRequest CreateAssessmentNote(string comment)
        {
            return new AssessmentNoteRequest
            {
                UserId = _userId,
                Note = comment,
                AssessmentId = AssessmentId,
                NoteType = "HOLDNOTE"
            };
        }

        private AssessmentNoteRequest CreateAssessmentNote()
        {
            return CreateAssessmentNote(GenerateAssessmentNote());
        }

        private string GenerateAssessmentNote()
        {
            var exceptionCaseData = ActionRepository.LoadExceptionCaseData(ExceptionCaseId);
            var assessmentNoteBuilder = new StringBuilder();

            assessmentNoteBuilder.Append(!string.IsNullOrWhiteSpace(exceptionCaseData.ExceptionCaseName)
                ? $"{exceptionCaseData.ExceptionName}. {exceptionCaseData.ExceptionCaseName}. "
                : $"{exceptionCaseData.ExceptionName}. ");

            assessmentNoteBuilder.Append(exceptionCaseData.GetConditionsMet(ActionRepository, ExceptionCaseId));

            var exceptionHoldReason = ActionRepository.LoadHoldReason(ExceptionActionRequest.ExceptionMappingId);

            if (exceptionHoldReason != null)
            {
                assessmentNoteBuilder.Append($"Hold Reason: {exceptionHoldReason.Reason}");
            }

            return assessmentNoteBuilder.ToString();
        }
    }
}
