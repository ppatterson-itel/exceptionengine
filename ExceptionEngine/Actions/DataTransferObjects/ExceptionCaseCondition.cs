﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class ExceptionCaseCondition 
    {
        public string Key { get; set; }
        public FilterOperator Operator { get; set; }
        public dynamic ValueFrom { get; set; }
        public dynamic ValueTo { get; set; }
        public string LookupTable { get; set; }
        public string LookupColumn { get; set; }
        public string LookupReturnColumn { get; set; }
    }
}
