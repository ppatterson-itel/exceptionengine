﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class ExceptionHoldComment
    {
        public int ExceptionMappingHoldCommentId { get; set; }
        public int ExceptionMappingId { get; set; }
        public int? HoldCommentId { get; set; }
        public string Comment { get; set; }
    }
}
