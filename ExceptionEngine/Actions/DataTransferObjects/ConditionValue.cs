﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class ConditionValue
    {
        public dynamic Value { get; set; }
    }
}
