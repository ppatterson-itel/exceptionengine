﻿using System;

namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class AssessmentComment
    {
        public int AssessmentCommentId { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
        public string Comment { get; set; }
        public string ReportSection { get; set; }
        public int? SourceCommentId { get; set; }
        public int AssessmentId { get; set; }
        public int? AssessmentExceptionId { get; set; }
        public int? ProductTypeId { get; set; }
        public int? Grouping { get; set; }
        public bool ShowOnPage2 { get; set; }
        public int DisplayOrder { get; set; }
        public bool QuickReference { get; set; }
    }
}
