﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class HoldReason
    {
        public string Reason { get; set; }
        public int HoldReasonId { get; set; }
    }
}
