﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class ExceptionReportComment
    {
        public int ExceptionMappingReportCommentId { get; set; }
        public int ExceptionMappingId { get; set; }
        public int? ReportCommentId { get; set; }
        public string Comment { get; set; }
        public string ReportSection { get; set; }
    }
}