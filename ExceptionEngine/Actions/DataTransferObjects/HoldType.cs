﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class HoldType
    {
        public int HoldTypeId { get; set; }
        public string DisplayText { get; set; }
    }
}
