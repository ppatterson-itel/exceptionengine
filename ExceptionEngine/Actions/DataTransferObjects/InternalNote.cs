﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class InternalNote
    {
        public int ExceptionMappingInternalNoteId { get; set; }
        public int ExceptionMappingId { get; set; }
        public string Note { get; set; }
        public int? InternalNoteId { get; set; }
        public string NoteType { get; set; }
    }
}