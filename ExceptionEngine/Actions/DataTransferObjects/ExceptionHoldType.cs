﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class ExceptionHoldType
    {
        public int ExceptionMappingHoldTypeId { get; set; }
        public int ExceptionMappingId { get; set; }
        public int HoldTypeId { get; set; }
        public string DisplayText { get; set; }
    }
}
