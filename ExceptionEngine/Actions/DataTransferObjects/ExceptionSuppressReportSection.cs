﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class ExceptionSuppressReportSection
    {
        public int ExceptionMappingSuppressReportSectionId { get; set; }
        public string ReportSection { get; set; }
    }
}