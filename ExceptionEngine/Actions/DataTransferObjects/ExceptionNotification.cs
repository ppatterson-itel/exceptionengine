﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class ExceptionNotification
    {
        public int ExceptionMappingNotificationId { get; set; }
        public int ExceptionMappingId { get; set; }
        public int? UserId { get; set; }
        public string EmailTo { get; set; }
        public string EmailAddress { get; set; }
        public bool ProcessingInspector { get; set; }
    }
}
