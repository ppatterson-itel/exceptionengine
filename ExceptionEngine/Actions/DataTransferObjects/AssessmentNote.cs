﻿using System;

namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class AssessmentNote
    {
        public int AssessmentNoteId { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
        public string Note { get; set; }
        public int? SourceNoteId { get; set; }
        public int AssessmentId { get; set; }
        public string NoteType { get; set; }
        public int? ProductTypeId { get; set; }
        public int? Grouping { get; set; }
    }
}
