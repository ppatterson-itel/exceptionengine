﻿using System;

namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class AssessmentExceptionNotification
    {
        public int AssessmentExceptionNotificationId { get; set; }
        public int AssessmentExceptionId { get; set; }
        public int? UserId { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfProcessing { get; set; }
        public int ExceptionMappingId { get; set; }
        public int? ParentAssessmentExceptionNotificationId { get; set; }
        public int ExceptionCaseId { get; set; }
    }
}
