﻿namespace ExceptionEngine.Actions.DataTransferObjects
{
    public class AssessmentExceptionSuppressReportSection
    {
        public int AssessmentExceptionSuppressReportSectionId { get; set; }
        public int AssessmentExceptionId { get; set; }
        public int AssessmentId { get; set; }
        public string ReportSection { get; set; }
    }
}
