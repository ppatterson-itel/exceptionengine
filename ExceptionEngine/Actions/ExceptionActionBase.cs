﻿using ExceptionEngine.Repositories;
using System;

namespace ExceptionEngine.Actions
{
    public abstract class ExceptionActionBase : IExceptionAction
    {
        protected ExceptionActionBase(IActionRepository actionRepository, ExceptionActionRequest exceptionActionRequest, int assessmentId,
            int exceptionCaseId, string description)
        {
            ActionRepository = actionRepository ?? throw new ArgumentNullException(nameof(actionRepository));
            ExceptionActionRequest = exceptionActionRequest ?? throw new ArgumentNullException(nameof(exceptionActionRequest));
            AssessmentId = assessmentId;
            ExceptionCaseId = exceptionCaseId;
            Description = description ?? throw new ArgumentNullException(nameof(description));
        }

        public int AssessmentId
        {
            get;
        }

        public int ExceptionCaseId
        {
            get;
        }

        public string Description
        {
            get;
        }
        public IActionRepository ActionRepository { get; }
        public ExceptionActionRequest ExceptionActionRequest { get; }

        public abstract ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId);

        public int GetUserId(int userId, string systemUserId)
        {
            if (!ExceptionActionRequest.IsSystemUser) return userId;

            if (string.IsNullOrWhiteSpace(systemUserId))
                return userId;

            return int.TryParse(systemUserId, out var id) ? id : userId;
        }
    }
}
