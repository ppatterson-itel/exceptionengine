﻿using System;
using ExceptionEngine.Actions.DataTransferObjects;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public class LessFormatter : IExceptionCaseConditionFormatter
    {
        public string FormatExceptionCaseCondition(ExceptionCaseCondition exceptionCaseCondition)
        {
            if (exceptionCaseCondition == null) throw new ArgumentNullException(nameof(exceptionCaseCondition));

            if (exceptionCaseCondition.ValueFrom is DateTime time)
                return "< " + time.ToString(@"MM'/'dd'/'yyyy HH':'mm");
            return $"< {exceptionCaseCondition.ValueFrom}";
        }
    }
}