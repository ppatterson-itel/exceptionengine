﻿using ExceptionEngine.Actions.DataTransferObjects;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public class ValueIsAbsentFormatter : IExceptionCaseConditionFormatter
    {
        public string FormatExceptionCaseCondition(ExceptionCaseCondition exceptionCaseCondition)
        {
            return "value is absent";
        }
    }
}
