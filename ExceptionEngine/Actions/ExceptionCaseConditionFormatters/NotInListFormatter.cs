﻿using System;
using System.Text;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public class NotInListFormatter : IExceptionCaseConditionFormatter
    {
        private readonly IActionRepository _actionRepository;
        private readonly int _exceptionCaseId;

        public NotInListFormatter(IActionRepository actionRepository, int exceptionCaseId)
        {
            _actionRepository = actionRepository ?? throw new ArgumentNullException(nameof(actionRepository));
            _exceptionCaseId = exceptionCaseId;
        }

        public string FormatExceptionCaseCondition(ExceptionCaseCondition exceptionCaseCondition)
        {
            if (exceptionCaseCondition == null) throw new ArgumentNullException(nameof(exceptionCaseCondition));

            var stringBuilder = new StringBuilder();

            stringBuilder.Append("isn't in list[");
            var conditionValues = _actionRepository.LoadConditionValues(_exceptionCaseId);
            var firstAdded = false;

            foreach (var condition in conditionValues)
            {
                if (firstAdded)
                    stringBuilder.Append(", ");

                stringBuilder.Append(condition.Value);
                firstAdded = true;
            }

            stringBuilder.Append("]");

            return stringBuilder.ToString();
        }
    }
}