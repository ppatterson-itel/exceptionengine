﻿using System;
using System.Collections.Generic;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public class ExceptionCaseFormatterFactory
    {
        private readonly Dictionary<FilterOperator, IExceptionCaseConditionFormatter> _lookupDictionary = new Dictionary<FilterOperator, IExceptionCaseConditionFormatter>();

        public ExceptionCaseFormatterFactory(IActionRepository actionRepository, int exceptionCaseId)
        {
            if (actionRepository == null) throw new ArgumentNullException(nameof(actionRepository));

            _lookupDictionary.Add(FilterOperator.ValueExists, new ValueExistsFormatter());
            _lookupDictionary.Add(FilterOperator.ValueIsAbsent, new ValueIsAbsentFormatter());
            _lookupDictionary.Add(FilterOperator.Equal, new EqualFormatter(actionRepository));
            _lookupDictionary.Add(FilterOperator.NotEqual, new NotEqualFormatter(actionRepository));
            _lookupDictionary.Add(FilterOperator.Less, new LessFormatter());
            _lookupDictionary.Add(FilterOperator.Greater, new GreaterFormatter());
            _lookupDictionary.Add(FilterOperator.Between, new BetweenFormatter());
            _lookupDictionary.Add(FilterOperator.NotBetween, new NotBetweenFormatter());
            _lookupDictionary.Add(FilterOperator.StartsWith, new StartsWithFormatter());
            _lookupDictionary.Add(FilterOperator.Contains, new ContainsFormatter());
            _lookupDictionary.Add(FilterOperator.DoesNotContain, new DoesNotContainFormatter());
            _lookupDictionary.Add(FilterOperator.FitRegularExpression, new FitRegularExpressionFormatter());
            _lookupDictionary.Add(FilterOperator.NotFitRegularExpression, new NotFitRegularExpressionFormatter());
            _lookupDictionary.Add(FilterOperator.InList, new InListFormatter(actionRepository, exceptionCaseId));
            _lookupDictionary.Add(FilterOperator.NotInList, new NotInListFormatter(actionRepository, exceptionCaseId));
        }

        public IExceptionCaseConditionFormatter GetFormatter(FilterOperator filterOperator)
        {
            if (_lookupDictionary.ContainsKey(filterOperator))
                return _lookupDictionary[filterOperator];
            throw new ArgumentException($"No formatter defined for filterOperator '{filterOperator:G}'.", nameof(filterOperator));
        }
    }
}
