﻿using ExceptionEngine.Actions.DataTransferObjects;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public interface IExceptionCaseConditionFormatter
    {
        string FormatExceptionCaseCondition(ExceptionCaseCondition exceptionCaseCondition);
    }
}
