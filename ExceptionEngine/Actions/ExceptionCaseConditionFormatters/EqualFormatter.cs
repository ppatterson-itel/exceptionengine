﻿using System;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public class EqualFormatter : IExceptionCaseConditionFormatter
    {
        private readonly IActionRepository _actionRepository;

        public EqualFormatter(IActionRepository actionRepository)
        {
            _actionRepository = actionRepository ?? throw new ArgumentNullException(nameof(actionRepository));
        }

        public string FormatExceptionCaseCondition(ExceptionCaseCondition exceptionCaseCondition)
        {
            if (exceptionCaseCondition == null) throw new ArgumentNullException(nameof(exceptionCaseCondition));

            if (!string.IsNullOrEmpty(exceptionCaseCondition.LookupTable))
                return $"= {GetComparisonValue(exceptionCaseCondition)}";

            switch (exceptionCaseCondition.ValueFrom)
            {
                case bool b:
                    return b ? "is checked" : "is unchecked";
                case DateTime time:
                    return $"= {time:MM\'/\'dd\'/\'yyyy HH\':\'mm}";
                default:
                    return $"= {exceptionCaseCondition.ValueFrom}";
            }
        }

        private string GetComparisonValue(ExceptionCaseCondition condition)
        {
            if (condition.LookupTable.StartsWith("#"))
            {
                return _actionRepository.LoadPickListDisplayTextById(int.Parse(condition.LookupTable.Substring(1)), condition.ValueFrom.ToString());
            }

            if (ShouldLookupByViewName(condition))
            {
                return _actionRepository.LoadPickListDisplayTextByViewName(condition.LookupColumn, condition.ValueFrom.ToString());
            }
            return _actionRepository.LoadPickListDisplayTextDynamic(condition.LookupTable, condition.LookupColumn, condition.LookupReturnColumn,
                condition.ValueFrom.ToString());
        }

        private static bool ShouldLookupByViewName(ExceptionCaseCondition condition)
        {
            return condition.LookupTable.StartsWith("vwplist_", StringComparison.InvariantCultureIgnoreCase)
                   && !condition.LookupColumn.Equals("code", StringComparison.InvariantCultureIgnoreCase)
                   && !condition.LookupReturnColumn.Equals("displaytext", StringComparison.InvariantCultureIgnoreCase);
        }
    }
}