﻿using System;
using ExceptionEngine.Actions.DataTransferObjects;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public class StartsWithFormatter : IExceptionCaseConditionFormatter
    {
        public string FormatExceptionCaseCondition(ExceptionCaseCondition exceptionCaseCondition)
        {
            if (exceptionCaseCondition == null) throw new ArgumentNullException(nameof(exceptionCaseCondition));

            return $"starts with '{exceptionCaseCondition.ValueFrom}'";
        }
    }
}