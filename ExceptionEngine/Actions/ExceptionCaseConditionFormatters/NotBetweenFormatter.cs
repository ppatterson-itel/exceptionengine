﻿using System;
using ExceptionEngine.Actions.DataTransferObjects;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public class NotBetweenFormatter : IExceptionCaseConditionFormatter
    {
        public string FormatExceptionCaseCondition(ExceptionCaseCondition exceptionCaseCondition)
        {
            if (exceptionCaseCondition == null) throw new ArgumentNullException(nameof(exceptionCaseCondition));

            if (exceptionCaseCondition.ValueFrom is DateTime time)
                return $"is not between {time:MM\'/\'dd\'/\'yyyy HH\':\'mm} and {(DateTime)exceptionCaseCondition.ValueTo:MM\'/\'dd\'/\'yyyy HH\':\'mm}";
            return $"is not between {exceptionCaseCondition.ValueFrom} and {exceptionCaseCondition.ValueTo}";
        }
    }
}