﻿using ExceptionEngine.Actions.DataTransferObjects;

namespace ExceptionEngine.Actions.ExceptionCaseConditionFormatters
{
    public class ValueExistsFormatter : IExceptionCaseConditionFormatter
    {
        public string FormatExceptionCaseCondition(ExceptionCaseCondition exceptionCaseCondition)
        {
            return "value exists";
        }
    }
}
