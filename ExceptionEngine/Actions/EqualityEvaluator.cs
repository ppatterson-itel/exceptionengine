﻿namespace ExceptionEngine.Actions
{
    public static class EqualityEvaluator
    {
        public static bool AreEqual<T1,T2>(T1 firstValue, T2 secondValue)
        {
            if (typeof(T1) != typeof(T2))
                return false;
            if (firstValue == null && secondValue == null)
                return true;
            if (firstValue == null || secondValue == null)
                return false;
            return firstValue.Equals(secondValue);
        }

        public static bool AreEqual(dynamic firstValue, dynamic secondValue)
        {
            if (firstValue == null && secondValue == null)
                return true;
            if (firstValue == null || secondValue == null)
                return false;
            return firstValue.Equals(secondValue);
        }
    }
}
