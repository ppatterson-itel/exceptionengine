﻿using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExceptionEngine.Actions
{
    public class InternalNoteAction : ExceptionActionBase
    {
        private readonly int _userId;

        public InternalNoteAction(IActionRepository actionRepository, ExceptionActionRequest exceptionActionRequest, int assessmentId,
            int exceptionCaseId, string description, int userId)
            : base(actionRepository, exceptionActionRequest, assessmentId, exceptionCaseId, description)
        {
            _userId = userId;
        }

        public override ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId)
        {
            var response = new ExceptionActionResponse
            {
                AssessmentExceptionId = assessmentExceptionId
            };

            if (ExceptionActionRequest.IncludeNoteWithException)
            {
                ActionRepository.CreateAssessmentNote(CreateAssessmentNote());
            }
            if (!exceptionAlreadyProcessed)
            { 
                var actionsDescription = new StringBuilder();
                actionsDescription.Append("Internal Note: ");

                var exceptionActionInternalNotes = ActionRepository.LoadExceptionActionInternalNotes(ExceptionActionRequest.ExceptionMappingId);
                var assessmentNotes = ActionRepository.LoadAssessmentNotes(AssessmentId).ToList();

                foreach (var note in exceptionActionInternalNotes)
                {
                    var createNew = ExceptionActionRequest.AllowDuplicates;

                    if (!createNew)
                    {
                        createNew = IsDuplicateNote(note, assessmentNotes);
                    }

                    if (!createNew) continue;

                    actionsDescription.Append(note.Note ?? "");
                    ActionRepository.CreateAssessmentNote(new AssessmentNoteRequest
                    {
                        UserId = _userId,
                        Note = note.Note,
                        SourceNoteId = note.InternalNoteId,
                        AssessmentId = AssessmentId,
                        NoteType = note.NoteType
                    });
                }

                response.ActionsTaken = actionsDescription.ToString();
            }
            else
            {
                response.ActionSkipped = true;
            }

            return response;
        }

        private static bool IsDuplicateNote(InternalNote note, IEnumerable<AssessmentNote> assessmentNotes)
        {
            return assessmentNotes.All(assessmentNote => !EqualityEvaluator.AreEqual(note.Note, assessmentNote.Note) || !EqualityEvaluator.AreEqual(note.NoteType, assessmentNote.NoteType));
        }

        private AssessmentNoteRequest CreateAssessmentNote()
        {
            return new AssessmentNoteRequest
            {
                UserId = _userId,
                Note = GenerateAssessmentNote(),
                AssessmentId = AssessmentId,
                NoteType = "VALNOTE"
            };
        }

        private string GenerateAssessmentNote()
        {
            var exceptionCaseData = ActionRepository.LoadExceptionCaseData(ExceptionCaseId);
            var assessmentNoteBuilder = new StringBuilder();

            assessmentNoteBuilder.Append(!string.IsNullOrWhiteSpace(exceptionCaseData.ExceptionCaseName)
                ? $"{exceptionCaseData.ExceptionName}. {exceptionCaseData.ExceptionCaseName}. "
                : $"{exceptionCaseData.ExceptionName}.");

            assessmentNoteBuilder.Append(exceptionCaseData.GetConditionsMet(ActionRepository, ExceptionCaseId));

            if (!string.IsNullOrWhiteSpace(Description))
            {
                assessmentNoteBuilder.Append($"{Description.TrimEnd('.')}. ");
            }

            return assessmentNoteBuilder.ToString();
        }
    }
}
