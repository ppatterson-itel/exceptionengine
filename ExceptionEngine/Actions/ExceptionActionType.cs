﻿namespace ExceptionEngine.Actions
{
    public enum ExceptionActionType
    {
        AlertMessage = 1,
        EmailNotification = 2,
        InternalNote = 3,
        ReportComment = 4,
        Hold = 5,
        CustomAction = 6,
        SuppressReportSection = 7,
        None = 0
    }
}
