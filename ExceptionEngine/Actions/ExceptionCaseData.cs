﻿using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System.Collections.ObjectModel;
using System.Text;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;

namespace ExceptionEngine.Actions
{
    public class ExceptionCaseData
    {
        public string ExceptionCaseName { get; set; }
        public string ExceptionName { get; set; }

        public Collection<ExceptionCaseCondition> Conditions { get; }

        public Collection<dynamic> ConditionValues { get; }

        public ExceptionCaseData()
        {
            Conditions = new Collection<ExceptionCaseCondition>();
            ConditionValues = new Collection<dynamic>();
        }

        public string GetConditionsMet(IActionRepository actionRepository, int exceptionCaseId)
        {
            var conditionsBuilder = new StringBuilder();

            conditionsBuilder.Append("Conditions Met: ");

            var i = 0;
            foreach (var condition in Conditions)
            {
                if (i > 0)
                    conditionsBuilder.Append(" AND ");

                conditionsBuilder.Append(FormatCondition(condition, actionRepository, exceptionCaseId));
                i++;
            }

            conditionsBuilder.Append(". ");

            return conditionsBuilder.ToString();
        }

        private static string FormatCondition(ExceptionCaseCondition condition, IActionRepository actionRepository, int exceptionCaseId)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append($"{condition.Key} ");

            var formatterFactory = new ExceptionCaseFormatterFactory(actionRepository, exceptionCaseId);

            var formatter = formatterFactory.GetFormatter(condition.Operator);

            stringBuilder.Append(formatter.FormatExceptionCaseCondition(condition));

            return stringBuilder.ToString();
        }
    }
}
