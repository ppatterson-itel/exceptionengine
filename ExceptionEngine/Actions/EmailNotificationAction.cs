﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Actions
{
    public class EmailNotificationAction : ExceptionActionBase
    {
        public string UserEmail
        {
            get;
        }

        private readonly ISendEmails _emailSender;

        public EmailNotificationAction(IActionRepository actionRepository, ExceptionActionRequest exceptionActionRequest, int assessmentId,
            int exceptionCaseId, string description, string userEmail, ISendEmails emailSender) 
            : base(actionRepository, exceptionActionRequest, assessmentId, exceptionCaseId, description)
        {
            _emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
            UserEmail = userEmail;
        }

        public override ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId)
        {
            var exceptionActionNotifications = ActionRepository.LoadExceptionActionNotifications(ExceptionActionRequest.ExceptionMappingId);
            var assessmentExceptionNotifications = ActionRepository.LoadAssessmentExceptionNotifications(AssessmentId).ToList();
            var actionsDescription = new StringBuilder();
            actionsDescription.Append("Send Email Notification: ");

            var response = new ExceptionActionResponse();

            var displayControlNumber = ActionRepository.LoadDisplayControlNumber(AssessmentId);

            foreach (var exceptionActionNotification in exceptionActionNotifications)
            {
                if (exceptionActionNotification.ProcessingInspector) continue;

                var createNew = ExceptionActionRequest.AllowDuplicates;

                if (!createNew)
                {
                    createNew = ShouldCreateNotification(exceptionActionNotification, assessmentExceptionNotifications);
                }

                if (!createNew) continue;

                response.AssessmentExceptionId = ActionRepository.CreateAlertNotification(AssessmentId, ExceptionCaseId, Description,
                    exceptionActionNotification.UserId, exceptionActionNotification.EmailAddress, exceptionActionNotification.ExceptionMappingId,
                    assessmentExceptionId);
                if (exceptionActionNotification.EmailAddress != null)
                {
                    _emailSender.QueueNotificationEmail(exceptionActionNotification.EmailAddress, Description, $"Exception, Assessment Control # {displayControlNumber}");
                }
            }

            if (ExceptionActionRequest.Value == "plus current user" && !string.IsNullOrWhiteSpace(UserEmail))
            {
                _emailSender.QueueNotificationEmail(UserEmail, Description, $"Exception, Assessment Control # {displayControlNumber}");
            }

            response.ActionsTaken = actionsDescription.ToString();

            return response;
        }

        private bool ShouldCreateNotification(ExceptionNotification exceptionActionNotification, IEnumerable<AssessmentExceptionNotification> assessmentExceptionNotifications)
        {
            return assessmentExceptionNotifications.All(assessmentExceptionNotification => !NotificationsAreEqual(exceptionActionNotification, assessmentExceptionNotification));
        }

        private bool NotificationsAreEqual(ExceptionNotification exceptionActionNotification, AssessmentExceptionNotification assessmentExceptionNotification)
        {
            return UserIdsMatch(exceptionActionNotification.UserId, assessmentExceptionNotification.UserId) &&
                   EmailsMatch(exceptionActionNotification.EmailAddress, assessmentExceptionNotification.Email) &&
                   ExceptionCaseId == assessmentExceptionNotification.ExceptionCaseId;
        }

        private static bool EmailsMatch(string emailAddress, string email)
        {
            return EqualityEvaluator.AreEqual(emailAddress, email);
        }

        private static bool UserIdsMatch(int? userId1, int? userId2)
        {
            return EqualityEvaluator.AreEqual(userId1, userId2);
        }
    }
}
