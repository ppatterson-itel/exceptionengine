﻿using ExceptionEngine.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExceptionEngine.Actions
{
    public class ExceptionActionFactory
    {
        private readonly IActionRepository _actionRepository;
        private readonly ISendEmails _emailSender;

        public ExceptionActionFactory(IActionRepository actionRepository, ISendEmails emailSender)
        {
            _actionRepository = actionRepository;
            _emailSender = emailSender;
        }

        public IEnumerable<IExceptionAction> GetExceptionActions(int exceptionId, int exceptionCaseId, int assessmentId, string description,
            string userEmail, int userId, SystemSettings systemSettings)
        {
            var actions = _actionRepository.LoadExceptionActions(exceptionId).ToList();

            var exceptionActions = new List<IExceptionAction>(actions.Count);

            foreach (var action in actions)
            {
                switch(action.ExceptionActionType)
                {
                    case ExceptionActionType.AlertMessage:
                        var alertMessageAction = new AlertMessageAction(_actionRepository, action, assessmentId, exceptionCaseId, description);
                        exceptionActions.Add(alertMessageAction);
                        break;
                    case ExceptionActionType.EmailNotification:
                        var emailNotificationAction = new EmailNotificationAction(_actionRepository, action, assessmentId, exceptionCaseId,
                        description, userEmail, _emailSender);
                        exceptionActions.Add(emailNotificationAction);
                        break;
                    case ExceptionActionType.InternalNote:
                        var internalNoteAction = new InternalNoteAction(_actionRepository, action, assessmentId, exceptionCaseId, description, userId);
                        exceptionActions.Add(internalNoteAction);
                        break;
                    case ExceptionActionType.ReportComment:
                        var reportCommentAction = new ReportCommentAction(_actionRepository, action, assessmentId, exceptionCaseId, description, userId);
                        exceptionActions.Add(reportCommentAction);
                        break;
                    case ExceptionActionType.Hold:
                        var holdAction = new HoldAction(_actionRepository, action, assessmentId, exceptionCaseId, description, userId, systemSettings);
                        exceptionActions.Add(holdAction);
                        break;
                    case ExceptionActionType.CustomAction:
                        var customAction = new CustomAction(_actionRepository, action, exceptionCaseId, assessmentId, description);
                        exceptionActions.Add(customAction);
                        break;
                    case ExceptionActionType.SuppressReportSection:
                        var suppressAction = new SuppressReportSectionAction(_actionRepository, action, assessmentId, exceptionCaseId, description);
                        exceptionActions.Add(suppressAction);
                        break;
                    case ExceptionActionType.None:
                        throw new InvalidOperationException($"Action type not supported: {action.ExceptionActionType}.");
                    default:
                        throw new InvalidOperationException($"Unrecognized action type {action.ExceptionActionType}.");
                }
            }

            return exceptionActions;
        }
    }
}
