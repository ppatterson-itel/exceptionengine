﻿using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExceptionEngine.Actions
{
    public class AlertMessageAction : ExceptionActionBase
    {
        public AlertMessageAction(IActionRepository actionRepository, ExceptionActionRequest exceptionActionRequest, int assessmentId,
            int exceptionCaseId, string description) 
            : base(actionRepository, exceptionActionRequest, assessmentId, exceptionCaseId, description)
        {
            
        }

        public override ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId)
        {
            var exceptionActionNotifications = ActionRepository.LoadExceptionActionNotifications(ExceptionActionRequest.ExceptionMappingId);
            var assessmentExceptionNotifications = ActionRepository.LoadAssessmentExceptionNotifications(AssessmentId).ToList();
            var actionsDescription = new StringBuilder();
            actionsDescription.Append("Send Alert Messages; ");

            var response = new ExceptionActionResponse();
            var createNew = ExceptionActionRequest.AllowDuplicates;

            foreach (var exceptionActionNotification in exceptionActionNotifications)
            {
                if (exceptionActionNotification.ProcessingInspector) continue;

                if (!createNew)
                {
                    createNew = ShouldCreateNotification(exceptionActionNotification, assessmentExceptionNotifications);
                }

                if (createNew)
                {
                    response.AssessmentExceptionId = ActionRepository.CreateAlertNotification(AssessmentId, ExceptionCaseId, Description, 
                        exceptionActionNotification.UserId, exceptionActionNotification.EmailAddress, exceptionActionNotification.ExceptionMappingId,
                        assessmentExceptionId);
                }
            }

            response.ActionsTaken = actionsDescription.ToString();

            return response;
        }

        private bool ShouldCreateNotification(ExceptionNotification exceptionActionNotification, IEnumerable<AssessmentExceptionNotification> assessmentExceptionNotifications)
        {
            return assessmentExceptionNotifications.All(assessmentExceptionNotification => !NotificationsAreEqual(exceptionActionNotification, assessmentExceptionNotification));
        }

        private bool NotificationsAreEqual(ExceptionNotification exceptionActionNotification, AssessmentExceptionNotification assessmentExceptionNotification)
        {
            return UserIdsMatch(exceptionActionNotification.UserId, assessmentExceptionNotification.UserId) &&
                   EmailsMatch(exceptionActionNotification.EmailAddress, assessmentExceptionNotification.Email) &&
                   ExceptionCaseId == assessmentExceptionNotification.ExceptionCaseId;
        }

        private static bool EmailsMatch(string emailAddress, string email)
        {
            return EqualityEvaluator.AreEqual(emailAddress, email);
        
        }

        private static bool UserIdsMatch(int? userId1, int? userId2)
        {
            return EqualityEvaluator.AreEqual(userId1, userId2);
        }
    }
}
