﻿namespace ExceptionEngine.Actions
{
    public interface ISendEmails
    {
        void QueueNotificationEmail(string toAddress, string emailBody, string emailSubject);
        void SendQueuedEmails();
    }
}
