﻿using ExceptionEngine.Repositories;
using System;
using System.Text;

namespace ExceptionEngine.Actions
{
    public class CustomAction : ExceptionActionBase
    {
        public CustomAction(IActionRepository actionRepository, ExceptionActionRequest exceptionActionRequest, int assessmentId, int exceptionCaseId, string description) 
            : base(actionRepository, exceptionActionRequest, assessmentId, exceptionCaseId, description)
        {

        }

        public override ExceptionActionResponse PerformExceptionAction(bool exceptionAlreadyProcessed, int? assessmentExceptionId)
        {
            var actionsDescription = new StringBuilder();

            if (string.IsNullOrWhiteSpace(ExceptionActionRequest.Value))
                return new ExceptionActionResponse
                {
                    ActionsTaken = actionsDescription.ToString(),
                    AssessmentExceptionId = assessmentExceptionId
                };

            try
            {
                ProcessAction(ExceptionActionRequest.Value);
                actionsDescription.Append($"Custom Action: {ExceptionActionRequest.Value}; ");
            }
            catch (Exception ex)
            {
                actionsDescription.Append($"Custom Action: Method with name '{ExceptionActionRequest.Value}' have raised exception: '{ex.Message}'; ");
            }

            return new ExceptionActionResponse
            {
                ActionsTaken = actionsDescription.ToString(),
                AssessmentExceptionId = assessmentExceptionId
            };
        }

        private void ProcessAction(string methodToCall)
        {
            switch(methodToCall)
            {
                case "ClearAssessmentPricing":
                    ActionRepository.ClearAssessmentPricing(AssessmentId);
                    break;
                default:
                    throw new ArgumentException($"method {methodToCall} does not exist.", nameof(methodToCall));
            }
        }
    }
}
