﻿using System;

namespace ExceptionEngine.Actions
{
    public class ExceptionActionRequest
    {
        public int ExceptionMappingId { get; set; }
        public ExceptionActionType ExceptionActionType { get; set; }
        public string Value { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool AllowDuplicates { get; set; }
        public bool IncludeNoteWithException { get; set; }
        public bool IsSystemUser { get; set; }
        public bool MarkAllRecipientsIfProcessed { get; set; }
        public int? HoldReasonId { get; set; }
        public string Name { get; set; }
    }
}
