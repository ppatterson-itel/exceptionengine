﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace ExceptionEngine.Actions
{
    public class EmailSender : ISendEmails
    {
        private readonly SystemSettings _systemSettings;
        private readonly Dictionary<string, MailMessage> _queuedEmails = new Dictionary<string, MailMessage>();

        public EmailSender(SystemSettings systemSettings)
        {
            _systemSettings = systemSettings ?? throw new ArgumentNullException(nameof(systemSettings));
        }
        public void QueueNotificationEmail(string toAddress, string emailBody, string emailSubject)
        {
            MailMessage mailMessage;
            if (_queuedEmails.ContainsKey(toAddress))
                mailMessage = _queuedEmails[toAddress];
            else
            {
                mailMessage = new MailMessage(
                    new MailAddress(_systemSettings.EmailFromAddress, _systemSettings.EmailFromDisplayName),
                    new MailAddress(toAddress)) {Subject = emailSubject, Body = ""};
                _queuedEmails.Add(toAddress, mailMessage);
            }

            mailMessage.Body += $"{emailBody} {Environment.NewLine}";
        }

        public void SendQueuedEmails()
        {
            var mailServer = new SmtpClient(_systemSettings.SmtpHost, int.Parse(_systemSettings.SmtpPort));

            var sentEmails = new List<string>(_queuedEmails.Count);

            foreach (var email in _queuedEmails)
            {
                mailServer.Send(email.Value);
                sentEmails.Add(email.Key);
            }

            foreach(var toAddress in sentEmails)
            {
                _queuedEmails.Remove(toAddress);
            }
        }

        public int TotalEmailsQueued => _queuedEmails.Count;
    }
}
