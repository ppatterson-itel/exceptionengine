﻿using ExceptionEngine.Repositories;
using System;

namespace ExceptionEngine
{
    public class SystemSettings
    {
        private readonly IExceptionRepository _exceptionRepository;

        public SystemSettings(IExceptionRepository exceptionRepository)
        {
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
        }

        public string EmailFromAddress
        {
            get
            {
                var retrievedSettings = _exceptionRepository.LoadSystemSettings();

                return retrievedSettings["Exception_EMailNotification_FromAddress"];
            }
        }

        public string EmailFromDisplayName
        {
            get
            {
                var retrievedSettings = _exceptionRepository.LoadSystemSettings();

                return retrievedSettings["Exception_EMailNotification_FromDisplayName"];
            }
        }

        public string SmtpHost
        {
            get
            {
                var retrievedSettings = _exceptionRepository.LoadSystemSettings();

                return retrievedSettings["SMTPHost"];
            }
        }

        public string SmtpPort
        {
            get
            {
                var retrievedSettings = _exceptionRepository.LoadSystemSettings();

                return retrievedSettings["SMTPPort"];
            }
        }

        public string HoldStatusName
        {
            get
            {
                var retrievedSettings = _exceptionRepository.LoadSystemSettings();

                return retrievedSettings["Exception_Status_HoldStatusName"] ?? "ONHOLD";
            }
        }

        public string ClearHoldIfNoExceptions
        {
            get
            {
                var retrievedSettings = _exceptionRepository.LoadSystemSettings();

                return retrievedSettings["Exception_Status_ClearIfNoExceptions"] ?? "0";
            }
        }

        public string SystemUserId
        {
            get
            {
                var retrievedSettings = _exceptionRepository.LoadSystemSettings();

                return retrievedSettings["SystemUserID"];
            }
        }
    }
}
