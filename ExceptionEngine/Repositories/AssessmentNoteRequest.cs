﻿namespace ExceptionEngine.Repositories
{
    public class AssessmentNoteRequest
    {
        public int UserId { get; set; }
        public string Note { get; set; }
        public int? SourceNoteId { get; set; }
        public int AssessmentId { get; set; }
        public string NoteType { get; set; }
    }
}
