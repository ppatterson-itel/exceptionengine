﻿namespace ExceptionEngine.Repositories
{
    public class AssessmentViewProductData
    {
        public string AssessmentProductType { get; set; }
        public string ExceptionViewProductType { get; set; }
    }
}