﻿namespace ExceptionEngine.Repositories
{
    public class AssessmentSuppressReportRequest
    {
        public int AssessmentId { get; set; }
        public int ExceptionCaseId { get; set; }
        public string Description { get; set; }
        public string ReportSection { get; set; }
        public int? AssessmentExceptionId { get; set; }
    }
}
