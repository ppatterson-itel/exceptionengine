﻿using ExceptionEngine.Exceptions.DataTransferObjects;
using System.Collections.Generic;

namespace ExceptionEngine.Repositories
{
    public interface IExceptionRepository
    {
        bool AssessmentExists(int assessmentId);
        AssessmentExceptions LoadPreRuleConditions(int assessmentId);
        AssessmentExceptions LoadAssessmentExceptions(int assessmentId);
        ExceptionCaseResponse ExecuteExceptionStoredProcedure(string conditionStoredProcedure, int assessmentId, ExceptionCaseDto exceptionCase, Exception exception);
        IEnumerable<ExceptionCondition> LoadExceptionConditions(int exceptionCaseId);
        ExceptionCaseResponse CheckForAdjustHoldOnAssessment(int assessmentId, ExceptionCaseDto exceptionCase, Exception exception);
        ExceptionCaseResponse CheckForCustomerArHoldOnAssessment(int assessmentId, ExceptionCaseDto exceptionCase, Exception exception);
        IEnumerable<int> LoadAssessmentCaseOverrides(int assessmentId);
        IEnumerable<ExceptionConditionValue> LoadExceptionConditionValues(int exceptionConditionId);
        bool AssessmentCaseOverridden(int assessmentId, int exceptionCaseId);
        IDictionary<string, string> LoadSystemSettings();
        Dictionary<string, dynamic> LoadAssessmentViewData(int assessmentId, int exceptionViewId);
        AssessmentViewProductData LoadAssessmentAndViewProductData(int assessmentId, int? productTypeId);
    }
}
