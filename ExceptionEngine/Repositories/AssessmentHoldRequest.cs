﻿namespace ExceptionEngine.Repositories
{
    public class AssessmentHoldRequest
    {
        public int AssessmentId { get; set; }
        public int HoldTypeId { get; set; }
        public int UserId { get; set; }
        public bool IsManualHold { get; set; }
        public int? HoldReasonId { get; set; }
    }
}
