﻿using ExceptionEngine.Actions;
using ExceptionEngine.Actions.DataTransferObjects;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ExceptionEngine.Repositories
{
    public interface IActionRepository
    {
        IEnumerable<ExceptionActionRequest> LoadExceptionActions(int exceptionId);
        IEnumerable<AssessmentExceptionNotification> LoadAssessmentExceptionNotifications(int assessmentId);
        IEnumerable<InternalNote> LoadExceptionActionInternalNotes(int exceptionMappingId);
        IEnumerable<AssessmentNote> LoadAssessmentNotes(int assessmentId);
        IEnumerable<ExceptionSuppressReportSection> LoadExceptionSuppressReportSections(int exceptionMappingId);
        IEnumerable<ExceptionNotification> LoadExceptionActionNotifications(int exceptionMappingId);
        IEnumerable<AssessmentExceptionSuppressReportSection> LoadAssessmentExceptionSuppressReportSections(int assessmentId);
        void CreateAssessmentNote(AssessmentNoteRequest assessmentNoteRequest);
        IEnumerable<ExceptionReportComment> LoadExceptionReportComments(int exceptionMappingId);
        int CreateAlertNotification(int assessmentId, int exceptionCaseId, string description, int? userId, string emailAddress, int exceptionMappingId, int? assessmentExceptionId);
        IEnumerable<AssessmentComment> LoadAssessmentComments(int assessmentId);
        int CreateAssessmentSuppressReport(AssessmentSuppressReportRequest assessmentSuppressReportRequest);
        ExceptionCaseData LoadExceptionCaseData(int exceptionCaseId);
        int CreateAssessmentComment(AssessmentCommentRequest assessmentCommentRequest);
        IEnumerable<ExceptionHoldComment> LoadExceptionHoldComments(int exceptionMappingId);
        void ClearAssessmentPricing(int assessmentId);
        IEnumerable<ExceptionHoldType> LoadExceptionHoldTypes(int exceptionMappingId);
        HoldReason LoadHoldReason(int exceptionMappingId);
        int ClearHoldsByHoldType(int assessmentId, int holdTypeId, int userId);
        bool AssessmentHoldExists(int assessmentId, int holdTypeId);
        HoldType LoadHoldType(int exceptionMappingId);
        void CreateAssessmentHold(AssessmentHoldRequest assessmentHoldRequest);
        string LoadPickListDisplayTextById(int pickListId, string code);
        string LoadPickListDisplayTextByViewName(string viewName, string code);
        string LoadPickListDisplayTextDynamic(string lookupTable, string lookupColumn, string lookupReturnColumn, string code);
        int UpdateAssessmentException(int assessmentId, int exceptionCaseId, Collection<string> actionsTaken, string conditionsMet, int? assessmentExceptionId);
        void SetAssessmentStatusToHold(int assessmentId, string holdStatusName);
        void UpdateLastValidationAssessmentExceptionIds(int assessmentId, string assessmentExceptionIds);
        void ClearAssessmentHolds(int assessmentId, int userId);
        void ClearAssessmentStatus(int assessmentId, string holdStatusName);
        IEnumerable<ConditionValue> LoadConditionValues(int exceptionCaseId);
        string LoadDisplayControlNumber(int assessmentId);
    }
}
