﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Repositories
{
    public class ExceptionRepository : IExceptionRepository
    {
        private readonly string _connectionString;

        public ExceptionRepository(string connectionString)
        {
            if (connectionString == null)
                throw new ArgumentNullException(nameof(connectionString));
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException($"{connectionString} cannot be empty.", nameof(connectionString));

            _connectionString = connectionString;
        }

        public ExceptionCaseResponse CheckForAdjustHoldOnAssessment(int assessmentId, ExceptionCaseDto exceptionCase, Exceptions.DataTransferObjects.Exception exception)
        {
            var exceptionCaseResponse = new ExceptionCaseResponse(exceptionCase, exception)
            { Description = "No adjuster hold found.", Result = false };
            dynamic response;

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                response = cn.QueryFirstOrDefault(GetCheckForAdjusterHoldSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            if (response == null) return exceptionCaseResponse;

            exceptionCaseResponse.Description = response.Description;
            exceptionCaseResponse.Result = response.Result;

            return exceptionCaseResponse;
        }

        private static string GetCheckForAdjusterHoldSql()
        {
            return @"SELECT TOP 1 pli.DisplayText AS [Description], CAST(1 AS BIT) AS Result
FROM Assessment a
INNER JOIN Company_SHP__Contact csc ON a.AdjusterID = csc.Company_SHP__ContactID
INNER JOIN dbo.CSHPCAttribute ca ON csc.Company_SHP__ContactID = ca.Company_SHP__ContactID
INNER JOIN dbo.PickListItem pli ON ca.Value = pli.Code
LEFT JOIN dbo.CSHPCAttributeText cat ON csc.Company_SHP__ContactID = cat.Company_SHP__ContactID
LEFT JOIN dbo.CSHPCComplexAttribute coa ON ca.CSHPCAttributeID = coa.CSHPCAttributeID
WHERE a.AdjusterID > 0 AND Status <> 'DONE' AND ca.KeyID = 4 AND ISNULL(ca.Value, 'NONE') <> 'NONE'
AND pli.PickListID = 357
AND a.AssessmentID = @AssessmentID AND a.AssessmentID NOT IN (
SELECT ms.AssessmentEntID 
FROM ITELMobile.dbo.Mobile_Sample ms
WHERE ms.PaymentStatus = 3 AND ms.AssessmentEntID IS NOT NULL
)
ORDER BY 1 DESC;";
        }

        public ExceptionCaseResponse CheckForCustomerArHoldOnAssessment(int assessmentId, ExceptionCaseDto exceptionCase, Exceptions.DataTransferObjects.Exception exception)
        {
            var exceptionCaseResponse = new ExceptionCaseResponse(exceptionCase, exception)
            { Description = "No AR hold found.", Result = false };
            dynamic response;

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                response = cn.QueryFirstOrDefault(GetCheckForCustomerArHoldSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            if (response == null) return exceptionCaseResponse;

            exceptionCaseResponse.Description = response.Description;
            exceptionCaseResponse.Result = response.Result;

            return exceptionCaseResponse;
        }

        private static string GetCheckForCustomerArHoldSql()
        {
            return @"DECLARE @WorkDate DATETIME = GETDATE();

SELECT TOP 1 pli.DisplayText AS [Description], CAST(1 AS BIT) AS Result
FROM [Enterprise].[dbo].[Assessment] a
INNER JOIN [dbo].[Claim] c ON a.ClaimID = c.ClaimID
INNER JOIN [dbo].[CompanyAttribute] ca ON c.CustomerID = ca.CompanyID
INNER JOIN [dbo].[PickListItem] pli ON ca.Value = pli.Code
WHERE a.AssessmentID = @AssessmentID AND ca.KeyID = 13 
AND ISNULL(ca.Value, 'NONE') <> 'NONE'
AND Ca.StartDate <= @WorkDate AND (ca.EndDate IS NULL OR @WorkDate < ca.EndDate)
AND pli.PickListID = 357 AND a.AssessmentID NOT IN (
SELECT ms.AssessmentEntID 
FROM ITELMobile.dbo.Mobile_Sample ms
WHERE ms.PaymentStatus = 3 AND ms.AssessmentEntID IS NOT NULL
);";
        }

        public ExceptionCaseResponse ExecuteExceptionStoredProcedure(string conditionStoredProcedure, int assessmentId, ExceptionCaseDto exceptionCase, Exceptions.DataTransferObjects.Exception exception)
        {
            var exceptionCaseResponse = new ExceptionCaseResponse(exceptionCase, exception);

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@ExceptionCaseID", exceptionCase.ExceptionCaseId);
            parameters.Add("@Description", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 200);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exceptionCaseResponse.Result = cn.QueryFirstOrDefault<int>(conditionStoredProcedure, parameters,
                    commandType: System.Data.CommandType.StoredProcedure) > 0;
            }

            exceptionCaseResponse.Description = parameters.Get<string>("@Description");

            return exceptionCaseResponse;
        }

        public bool AssessmentExists(int assessmentId)
        {
            bool exists = false;
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exists = cn.QueryFirstOrDefault<bool>(GetAssessmentExistsSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return exists;
        }

        private static string GetAssessmentExistsSql()
        {
            return @"DECLARE @AssessmentExists bit = 0;

SELECT @AssessmentExists = 1
FROM dbo.Assessment
WHERE AssessmentID = @AssessmentID;

SELECT @AssessmentExists AS AssessmentExists;";
        }

        public IEnumerable<ExceptionCondition> LoadExceptionConditions(int exceptionCaseId)
        {
            IEnumerable<ExceptionCondition> exceptionConditions;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionCaseID", exceptionCaseId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exceptionConditions = cn.Query<ExceptionCondition>(GetExceptionConditionsSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return exceptionConditions;
        }

        private static string GetExceptionConditionsSql()
        {
            return @"SELECT ExceptionConditionID, ExceptionCaseID, ec.ExceptionAttributeMetadataID, Operator, ValueFrom, ValueTo,
eam.[Key] AS AttributeKey, eam.ColumnName AS AttributeColumn, eam.DataType AS DataType,
eam.LookupTable AS LookupTable, eam.LookupColumn AS LookupColumn, eam.LookupReturnColumn AS LookupReturnColumn,
eam.ExceptionViewID
FROM ExceptionCondition AS ec
INNER JOIN ExceptionAttributeMetadata eam ON ec.ExceptionAttributeMetadataID = eam.ExceptionAttributeMetadataID
WHERE(ExceptionCaseID = @ExceptionCaseID);";
        }

        public AssessmentExceptions LoadPreRuleConditions(int assessmentId)
        {
            var assessmentExceptions = new AssessmentExceptions(assessmentId);

            var exceptions = new List<Exceptions.DataTransferObjects.Exception>();

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Query< Exceptions.DataTransferObjects.Exception, ExceptionCaseDto, Exceptions.DataTransferObjects.Exception>(GetPreruleAssessmentExceptionsSql(),
                    (exception, exceptionCase) =>
                    {
                        var retrievedException = exceptions.FirstOrDefault(e => e.ExceptionId == exception.ExceptionId);

                        if (retrievedException == null)
                        {
                            exceptions.Add(exception);
                            retrievedException = exception;
                        }

                        retrievedException.ExceptionCases.Add(exceptionCase);

                        return exception;
                    },
                    splitOn: nameof(ExceptionCaseDto.ExceptionCaseId),
                    commandType: System.Data.CommandType.Text);
            }

            assessmentExceptions.AddAssessmentExceptions(exceptions);

            return assessmentExceptions;
        }

        private static string GetPreruleAssessmentExceptionsSql()
        {
            return @"SELECT e.[ExceptionID]
      ,e.[Name]
      ,e.[IsSystem]
      ,e.[ConditionStoredProcedure]
      ,e.[IsDeleted]
      ,e.[CreateDate]
      ,e.[LastModifiedDate]
      ,e.[ConditionStoredMethod]
      ,e.[ExecutionOrder]
      ,e.[Active]
      ,e.[ExceptionGroup]
      ,e.[PreRule]
	  ,ec.[ExceptionCaseID]
      ,ec.[ExceptionID]
      ,ec.[ExceptionAccessGroupID]
      ,ec.[Name]
      ,ec.[IsDeleted]
      ,ec.[CreatedByUserID]
      ,ec.[CreateDate]
      ,ec.[LastModifiedByUserID]
      ,ec.[LastModifiedDate]
      ,ec.[ExceptionViewID]
      ,ec.[AllowOverride]
      ,ec.[Description]
      ,ec.[DisplayInSummary]
      ,ec.[DisplayDetailedExceptionInSummary]
  FROM [dbo].[Exception] e
  INNER JOIN [dbo].[ExceptionCase] ec ON e.ExceptionID = ec.ExceptionID
  WHERE e.IsDeleted = 0 AND e.Active = 1 AND e.PreRule = 1 AND ec.IsDeleted = 0
  ORDER BY e.ExecutionOrder;";
        }

        public IEnumerable<ExceptionConditionValue> LoadExceptionConditionValues(int exceptionConditionId)
        {
            IEnumerable<ExceptionConditionValue> exceptionConditions;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionConditionID", exceptionConditionId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exceptionConditions = cn.Query<ExceptionConditionValue>(GetExceptionConditionValuesSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return exceptionConditions;
        }

        private static string GetExceptionConditionValuesSql()
        {
            return @"SELECT [ExceptionConditionValuesListID], [Value] AS ConditionValue
FROM [Enterprise].[dbo].[ExceptionConditionValuesList]
WHERE ExceptionConditionID = @ExceptionConditionID;";
        }

        public AssessmentExceptions LoadAssessmentExceptions(int assessmentId)
        {
            var assessmentExceptions = new AssessmentExceptions(assessmentId);

            var exceptions = new List<Exceptions.DataTransferObjects.Exception>();

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Query< Exceptions.DataTransferObjects.Exception, ExceptionCaseDto, ExceptionCondition, Exceptions.DataTransferObjects.Exception>(GetAssessmentExceptionsSql(),
                    (exception, exceptionCase, exceptionCondition) =>
                    {
                        var retrievedException = exceptions.FirstOrDefault(e => e.ExceptionId == exception.ExceptionId);

                        if (retrievedException == null)
                        {
                            exceptions.Add(exception);
                            retrievedException = exception;
                        }

                        var retrievedExceptionCase =
                            retrievedException.ExceptionCases.FirstOrDefault(exc =>
                                exc.ExceptionCaseId == exceptionCase.ExceptionCaseId);

                        if (retrievedExceptionCase == null)
                        {
                            retrievedException.ExceptionCases.Add(exceptionCase);
                            retrievedExceptionCase = exceptionCase;
                        }

                        if (exceptionCondition != null)
                            retrievedExceptionCase.ExceptionConditions.Add(exceptionCondition);

                        return exception;
                    },
                    splitOn: $"{nameof(ExceptionCaseDto.ExceptionCaseId)},{nameof(ExceptionCondition.ExceptionConditionId)}",
                    commandType: System.Data.CommandType.Text);
            }

            assessmentExceptions.AddAssessmentExceptions(exceptions);

            return assessmentExceptions;
        }

        private static string GetAssessmentExceptionsSql()
        {
            return @"SELECT e.[ExceptionID],e.[Name],e.[IsSystem],e.[ConditionStoredProcedure],e.[IsDeleted],e.[CreateDate],e.[LastModifiedDate]
,e.[ConditionStoredMethod],e.[ExecutionOrder],e.[Active],e.[ExceptionGroup],e.[PreRule],ec.[ExceptionCaseID],ec.[ExceptionID]
,ec.[ExceptionAccessGroupID],ec.[Name],ec.[IsDeleted],ec.[CreatedByUserID],ec.[CreateDate],ec.[LastModifiedByUserID]
,ec.[LastModifiedDate],ec.[ExceptionViewID],ec.[AllowOverride],ec.[Description],ec.[DisplayInSummary],ec.[DisplayDetailedExceptionInSummary]
,eco.[ExceptionConditionID],eco.[ExceptionAttributeMetadataID],eco.[Operator],eco.[ValueFrom],eco.[ValueTo]
,eam.[Key] AS AttributeKey, eam.ColumnName AS AttributeColumn, eam.DataType AS DataType,
eam.LookupTable AS LookupTable, eam.LookupColumn AS LookupColumn, eam.LookupReturnColumn AS LookupReturnColumn,
eam.ExceptionViewID
FROM [dbo].[Exception] e
INNER JOIN [dbo].[ExceptionCase] ec ON e.ExceptionID = ec.ExceptionID
LEFT JOIN [dbo].[ExceptionCondition] eco ON ec.ExceptionCaseID =  eco.ExceptionCaseID
LEFT JOIN [dbo].[ExceptionAttributeMetadata]  eam ON eco.ExceptionAttributeMetadataID = eam.ExceptionAttributeMetadataID
WHERE e.IsDeleted = 0 AND e.Active = 1 AND e.PreRule = 0 AND ec.IsDeleted = 0
ORDER BY e.ExecutionOrder;";
        }

        public bool AssessmentCaseOverridden(int assessmentId, int exceptionCaseId)
        {
            bool isOverridden;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionCaseID", exceptionCaseId);
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                isOverridden = cn.QueryFirst<bool>(GetAssessmentCaseOverriddenSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return isOverridden;
        }

        private static string GetAssessmentCaseOverriddenSql()
        {
            return @"DECLARE @IsOverridden bit = 0;

SELECT @IsOverridden = 1
FROM dbo.AssessmentException
WHERE IsOverridden = 1 AND ExceptionCaseID = @ExceptionCaseID AND AssessmentID = @AssessmentID;

SELECT @IsOverridden AS IsOverridden;";
        }

        public IDictionary<string, string> LoadSystemSettings()
        {
            IDictionary<string, string> systemSettings = new Dictionary<string, string>();
            IEnumerable<dynamic> retrievedSettings;

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                retrievedSettings = cn.Query(GetLoadSystemSettingsSql(), commandType: System.Data.CommandType.Text);
            }

            foreach(var retrievedSetting in retrievedSettings)
            {
                systemSettings.Add(retrievedSetting.Setting, retrievedSetting.Value);
            }

            return systemSettings;
        }

        private static string GetLoadSystemSettingsSql()
        {
            return @"SELECT [Setting], [Value]
FROM [Enterprise].[dbo].[Sys_Control]
WHERE Setting IN ('Exception_Status_HoldStatusName','Exception_Status_ClearIfNoExceptions', 'SystemUserID',
'SMTPHost', 'SMTPPort', 'Exception_EMailNotification_FromAddress', 'Exception_EMailNotification_FromDisplayName');";
        }

        public Dictionary<string, dynamic> LoadAssessmentViewData(int assessmentId, int exceptionViewId)
        {
            var assessmentViewData = new Dictionary<string, dynamic>();
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@ExceptionViewID", exceptionViewId);

            IDictionary<string,object> retrievedViewData;

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                retrievedViewData = (IDictionary<string, object>)cn.Query(GetLoadAssessmentViewData(),
                    parameters, commandType: System.Data.CommandType.Text).FirstOrDefault();
            }

            if (retrievedViewData == null) return assessmentViewData;

            var columnNames = retrievedViewData.Keys;

            foreach (var column in columnNames)
            {
                assessmentViewData.Add(column, retrievedViewData[column]);
            }

            return assessmentViewData;
        }

        private static string GetLoadAssessmentViewData()
        {
            return @"DECLARE @ViewName NVARCHAR(128);

SELECT @ViewName = ViewName
FROM ExceptionView ev
WHERE ev.ExceptionViewID = @ExceptionViewID;

DECLARE @SqlToExecute VARCHAR(MAX);

SELECT @SqlToExecute = 'SELECT *  FROM ' + QUOTENAME(@ViewName) + 
' WHERE AssessmentID = ' + CONVERT(VARCHAR(MAX),@AssessmentID) + ';'

EXEC (@SqlToExecute);";
        }

        public AssessmentViewProductData LoadAssessmentAndViewProductData(int assessmentId, int? productTypeId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@ProductTypeId", productTypeId);

            AssessmentViewProductData response;
            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                response = cn.QueryFirst<AssessmentViewProductData>(GetLoadAssessmentViewProductData(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return response;
        }

        private static string GetLoadAssessmentViewProductData()
        {
            return @"DECLARE @AssessmentProductType VARCHAR(10);
DECLARE @ExceptionViewProductType VARCHAR(10);

SELECT @AssessmentProductType = ProductType
FROM Assessment
WHERE AssessmentID = @AssessmentID;

SELECT @ExceptionViewProductType = ShortName
FROM ProductType
WHERE ProductTypeID = @ProductTypeID;

SELECT @AssessmentProductType AS AssessmentProductType, @ExceptionViewProductType AS ExceptionViewProductType;";
        }

        public IEnumerable<int> LoadAssessmentCaseOverrides(int assessmentId)
        {
            IEnumerable<int> assessmentCaseOverrides;

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                assessmentCaseOverrides = cn.Query<int>(GetAssessmentCaseOverridesSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return assessmentCaseOverrides;
        }

        private string GetAssessmentCaseOverridesSql()
        {
            return @"SELECT ExceptionCaseId
FROM dbo.AssessmentException
WHERE AssessmentID = @AssessmentID AND IsOverridden = 1;";
        }
    }
}
