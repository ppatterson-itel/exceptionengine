﻿using Dapper;
using ExceptionEngine.Actions;
using ExceptionEngine.Actions.DataTransferObjects;
using log4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.Text;
// ReSharper disable StringLiteralTypo

namespace ExceptionEngine.Repositories
{
    public class ActionRepository : IActionRepository
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly string _connectionString;

        public ActionRepository(string connectionString)
        {
            if (connectionString == null)
                throw new ArgumentNullException(nameof(connectionString));
            if (string.IsNullOrWhiteSpace(connectionString))
                throw new ArgumentException($"{connectionString} cannot be empty.", nameof(connectionString));

            _connectionString = connectionString;
        }

        public IEnumerable<AssessmentExceptionNotification> LoadAssessmentExceptionNotifications(int assessmentId)
        {
            IEnumerable<AssessmentExceptionNotification> assessmentExceptionNotifications;

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                assessmentExceptionNotifications = cn.Query<AssessmentExceptionNotification>(GetLoadAssessmentExceptionNotificationsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return assessmentExceptionNotifications;
        }

        private static string GetLoadAssessmentExceptionNotificationsSql()
        {
            return @"SELECT aen.AssessmentExceptionNotificationID, aen.AssessmentExceptionID, aen.UserID, aen.Email, aen.DateOfProcessing, 
aen.ExceptionMappingID, aen.ParentAssessmentExceptionNotificationID, ae.ExceptionCaseID
FROM AssessmentException AS ae INNER JOIN
AssessmentExceptionNotification AS aen ON aen.AssessmentExceptionID = ae.AssessmentExceptionID
WHERE (ae.AssessmentID = @AssessmentID)";
        }

        public IEnumerable<ExceptionNotification> LoadExceptionActionNotifications(int exceptionMappingId)
        {

            IEnumerable<ExceptionNotification> assessmentExceptionNotifications;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionMappingID", exceptionMappingId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                assessmentExceptionNotifications = cn.Query<ExceptionNotification>("[dbo].[sp_ExceptionMappingNotification_SelectByExceptionMappingID]",
                    parameters, commandType: System.Data.CommandType.StoredProcedure);
            }

            return assessmentExceptionNotifications;
    }

        public IEnumerable<ExceptionActionRequest> LoadExceptionActions(int exceptionId)
        {
            IEnumerable<ExceptionActionRequest> exceptionActions;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionID", exceptionId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exceptionActions = cn.Query<ExceptionActionRequest>(GetExceptionActionsSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return exceptionActions;
        }

        private static string GetExceptionActionsSql()
        {
            return @"SELECT em.ExceptionMappingID, em.ExceptionID, em.ExceptionActionID AS ExceptionActionType, em.Value, em.IsDeleted, 
em.CreateDate, em.LastModifiedDate, em.AllowDuplicates, em.IncludeNoteWithException, em.IsSystemUser, 
em.MarkAllRecipientsIfProcessed, em.HoldReasonID, ea.Name
FROM ExceptionMapping AS em LEFT OUTER JOIN
ExceptionAction AS ea ON em.ExceptionActionID = ea.ExceptionActionID
WHERE ea.Name IS NOT NULL AND IsDeleted = 0 AND em.ExceptionID = @ExceptionID;";
        }

        public int CreateAlertNotification(int assessmentId, int exceptionCaseId, string description, int? userId, string emailAddress, int exceptionMappingId, int? assessmentExceptionId)
        {
            var parameters = new DynamicParameters();
            parameters.AddDynamicParams(
                new
                {
                    AssessmentID = assessmentId,
                    ExceptionCaseID = exceptionCaseId,
                    Description = description,
                    UserID = userId,
                    Email = emailAddress,
                    ExceptionMappingID = exceptionMappingId,
                    AssessmentExceptionID = assessmentExceptionId
                });

            int returnedAssessmentExceptionId;

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                returnedAssessmentExceptionId = cn.QueryFirst<int>(GetCreateAlertNotificationSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return returnedAssessmentExceptionId;
        }

        private static string GetCreateAlertNotificationSql()
        {
            return @"DECLARE @AssessmentExceptionID INT;

MERGE dbo.AssessmentException AS t
USING (
SELECT @AssessmentExceptionID AS AssessmentExceptionID 
) AS s ON s.AssessmentExceptionID = t.AssessmentExceptionID
WHEN NOT MATCHED BY TARGET THEN
INSERT(AssessmentID, ExceptionCaseID, CreateDate, Description, IsOverridden, ConditionsMet, ActionsTaken)
VALUES (@AssessmentID, @ExceptionCaseID, GETDATE(), @Description, 0, '', '');

SET @AssessmentExceptionID = ISNULL(@AssessmentExceptionID, SCOPE_IDENTITY());

INSERT INTO[dbo].[AssessmentExceptionNotification]
(AssessmentExceptionID, UserID, Email, ExceptionMappingID)
VALUES(@AssessmentExceptionID, @UserID, @Email, @ExceptionMappingID);";
        }

        public IEnumerable<InternalNote> LoadExceptionActionInternalNotes(int exceptionMappingId)
        {
            IEnumerable<InternalNote> internalNotes;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionMappingID", exceptionMappingId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                internalNotes = cn.Query<InternalNote>(GetLoadExceptionActionInternalNotesSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return internalNotes;
        }

        private static string GetLoadExceptionActionInternalNotesSql()
        {
            return @"SELECT ExceptionMappingInternalNoteID, ExceptionMappingID, Note, InternalNoteID, [Type] AS NoteType
FROM ExceptionMappingInternalNote
WHERE (ExceptionMappingID = @ExceptionMappingID);";
        }

        public IEnumerable<AssessmentNote> LoadAssessmentNotes(int assessmentId)
        {
            IEnumerable<AssessmentNote> assessmentNotes;

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                assessmentNotes = cn.Query<AssessmentNote>(GetLoadAssessmentNotesSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return assessmentNotes;
        }

        private static string GetLoadAssessmentNotesSql()
        {
            return @"SELECT AssessmentNoteID, UserID, CreateDate, Note, SourceNoteID, AssessmentID, Type AS NoteType, ProductTypeID, [Grouping]
FROM AssessmentNote
WHERE (AssessmentID = @AssessmentID);";
        }

        public void CreateAssessmentNote(AssessmentNoteRequest assessmentNoteRequest)
        {
            if (assessmentNoteRequest == null)
                throw new ArgumentNullException(nameof(assessmentNoteRequest));

            var parameters = new DynamicParameters();
            parameters.AddDynamicParams(
                new
                {
                    assessmentNoteRequest.AssessmentId,
                    assessmentNoteRequest.Note,
                    assessmentNoteRequest.SourceNoteId,
                    assessmentNoteRequest.UserId,
                    assessmentNoteRequest.NoteType
                });

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Execute(GetCreateAssessmentNoteSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }
        }

        private static string GetCreateAssessmentNoteSql()
        {
            return @"DECLARE @AssessmentNoteID INT;

EXEC @AssessmentNoteID = spGetNextKeyValue 'AssessmentNote', NULL, 0;

INSERT INTO [dbo].[AssessmentNote] (AssessmentNoteID, UserID, CreateDate, Note, SourceNoteID, AssessmentID, [Type])
VALUES (@AssessmentNoteID, @UserID, GETDATE(), @Note, @SourceNoteID, @AssessmentID, @NoteType);";
        }

        public ExceptionCaseData LoadExceptionCaseData(int exceptionCaseId)
        {
            ExceptionCaseData caseData;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionCaseID", exceptionCaseId);

            IEnumerable<ExceptionCaseCondition> conditions;
            IEnumerable<dynamic> conditionValues;

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                using (var multipleQuery = cn.QueryMultiple(GetExceptionCaseDataSql(),
                    parameters, commandType: System.Data.CommandType.Text))
                {
                    caseData = multipleQuery.ReadFirst<ExceptionCaseData>();
                    conditions = multipleQuery.Read<ExceptionCaseCondition>();
                    conditionValues = multipleQuery.Read<dynamic>();
                }
            }

            foreach (var condition in conditions)
            {
                caseData.Conditions.Add(condition);
            }

            foreach (var conditionValue in conditionValues)
            {
                caseData.ConditionValues.Add(conditionValue);
            }

            return caseData;
        }

        private static string GetExceptionCaseDataSql()
        {
            return @"SELECT ec.Name AS ExceptionCaseName, e.Name AS ExceptionName FROM ExceptionCase ec
INNER JOIN Exception e ON ec.ExceptionID = e.ExceptionID
WHERE ec.ExceptionCaseID = @ExceptionCaseID;

SELECT eam.[Key], ec.Operator, ec.ValueFrom, ec.ValueTo, eam.LookupTable, eam.LookupColumn, eam.LookupReturnColumn
FROM [Enterprise].[dbo].[ExceptionCondition] ec
INNER JOIN ExceptionAttributeMetadata eam ON ec.ExceptionAttributeMetadataID = eam.ExceptionAttributeMetadataID
WHERE ExceptionCaseID = @ExceptionCaseID;

SELECT evl.Value
FROM [Enterprise].[dbo].[ExceptionCondition] ec
INNER JOIN ExceptionConditionValuesList evl ON ec.ExceptionConditionID = evl.ExceptionConditionID
WHERE ExceptionCaseID = @ExceptionCaseID;";
        }

        public IEnumerable<ConditionValue> LoadConditionValues(int exceptionCaseId)
        {
            IEnumerable<ConditionValue> conditionValues;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionCaseID", exceptionCaseId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                conditionValues = cn.Query<ConditionValue>(GetLoadConditionValuesSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return conditionValues;
        }

        private static string GetLoadConditionValuesSql()
        {
            return @"SELECT evl.Value
FROM [Enterprise].[dbo].[ExceptionCondition] ec
INNER JOIN ExceptionConditionValuesList evl ON ec.ExceptionConditionID = evl.ExceptionConditionID
WHERE ExceptionCaseID = @ExceptionCaseID;";
        }

        public IEnumerable<ExceptionReportComment> LoadExceptionReportComments(int exceptionMappingId)
        {
            IEnumerable<ExceptionReportComment> exceptionReportComments;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionMappingID", exceptionMappingId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exceptionReportComments = cn.Query<ExceptionReportComment>(GetLoadExceptionReportCommentsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return exceptionReportComments;
        }

        private static string GetLoadExceptionReportCommentsSql()
        {
            return @"SELECT [ExceptionMappingReportCommentID]
    ,[ExceptionMappingID]
    ,[ReportCommentID]
    ,[Comment]
    ,[ReportSection]
FROM [dbo].[ExceptionMappingReportComment]
WHERE [ExceptionMappingID] = @ExceptionMappingID;";
        }

        public IEnumerable<AssessmentComment> LoadAssessmentComments(int assessmentId)
        {
            IEnumerable<AssessmentComment> exceptionReportComments;

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exceptionReportComments = cn.Query<AssessmentComment>(GetLoadAssessmentCommentsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return exceptionReportComments;
        }

        private static string GetLoadAssessmentCommentsSql()
        {
            return @"SELECT [AssessmentCommentID]
      ,[UserID]
      ,[CreateDate]
      ,[Comment]
      ,[ReportSection]
      ,[SourceCommentID]
      ,[AssessmentID]
      ,[AssessmentExceptionID]
      ,[ProductTypeID]
      ,[Grouping]
      ,[ShowOnPage2]
      ,[DisplayOrder]
      ,[QuickReference]
FROM [dbo].[AssessmentComment]
WHERE [AssessmentID] = @AssessmentID;";
        }

        public int CreateAssessmentComment(AssessmentCommentRequest assessmentCommentRequest)
        {
            if (assessmentCommentRequest == null) throw new ArgumentNullException(nameof(assessmentCommentRequest));

            var parameters = new DynamicParameters();
            parameters.AddDynamicParams(
                new
                {
                    assessmentCommentRequest.AssessmentId,
                    assessmentCommentRequest.ExceptionCaseId,
                    assessmentCommentRequest.Description,
                    assessmentCommentRequest.UserId,
                    assessmentCommentRequest.Comment,
                    assessmentCommentRequest.ReportSection,
                    assessmentCommentRequest.SourceCommentId,
                    assessmentCommentRequest.AssessmentExceptionId
                });
            int returnedAssessmentExceptionId;

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                returnedAssessmentExceptionId = cn.QueryFirst<int>(GetCreateAssessmentCommentSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return returnedAssessmentExceptionId;
        }

        private static string GetCreateAssessmentCommentSql()
        {
            return @"MERGE dbo.AssessmentException AS t
USING (
SELECT @AssessmentExceptionID AS AssessmentExceptionID 
) AS s ON s.AssessmentExceptionID = t.AssessmentExceptionID
WHEN NOT MATCHED BY TARGET THEN
INSERT(AssessmentID, ExceptionCaseID, CreateDate, Description, IsOverridden, ConditionsMet, ActionsTaken)
VALUES (@AssessmentID, @ExceptionCaseID, GETDATE(), @Description, 0, '', '');

SET @AssessmentExceptionID = ISNULL(@AssessmentExceptionID, SCOPE_IDENTITY());

EXEC @AssessmentCommentID = spGetNextKeyValue 'AssessmentComment', NULL, 0;

INSERT INTO [dbo].[AssessmentComment]
([AssessmentCommentID], [UserID],[CreateDate],[Comment],[ReportSection],[SourceCommentID],[AssessmentID],[AssessmentExceptionID])
VALUES(@AssessmentCommentID, @UserID, GETDATE(), @Comment, @ReportSection, @SourceCommentID, @AssessmentID, @AssessmentExceptionID);

SELECT @AssessmentExceptionID;";
        }

        public IEnumerable<ExceptionHoldComment> LoadExceptionHoldComments(int exceptionMappingId)
        {
            IEnumerable<ExceptionHoldComment> exceptionHoldComments;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionMappingID", exceptionMappingId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exceptionHoldComments = cn.Query<ExceptionHoldComment>(GetLoadExceptionHoldCommentsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return exceptionHoldComments;
        }

        private static string GetLoadExceptionHoldCommentsSql()
        {
            return @"SELECT ExceptionMappingHoldCommentID, ExceptionMappingID, HoldCommentID, Comment
FROM [ExceptionMappingHoldComment]
WHERE [ExceptionMappingID] = @ExceptionMappingID;";
        }

        public IEnumerable<ExceptionHoldType> LoadExceptionHoldTypes(int exceptionMappingId)
        {
            IEnumerable<ExceptionHoldType> exceptionHoldTypes;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionMappingID", exceptionMappingId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exceptionHoldTypes = cn.Query<ExceptionHoldType>(GetLoadExceptionHoldTypesSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return exceptionHoldTypes;
        }

        private static string GetLoadExceptionHoldTypesSql()
        {
            return @"SELECT emht.ExceptionMappingHoldTypeID, emht.ExceptionMappingID, emht.HoldTypeID, ht.DisplayText
FROM [ExceptionMappingHoldType] emht
INNER JOIN [HoldType] ht on emht.HoldTypeID = ht.HoldTypeID
WHERE emht.[ExceptionMappingID] = @ExceptionMappingID;";
        }

        public HoldReason LoadHoldReason(int exceptionMappingId)
        {
            HoldReason holdReason;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionMappingID", exceptionMappingId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                holdReason = cn.QueryFirstOrDefault<HoldReason>(GetExceptionMappingHoldReasonSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return holdReason;
        }

        private static string GetExceptionMappingHoldReasonSql()
        {
            return @"SELECT hr.Reason, hr.HoldReasonID
FROM [ExceptionMapping] em
INNER JOIN [HoldReasons] hr ON em.HoldReasonID = hr.HoldReasonID
WHERE em.[ExceptionMappingID] = @ExceptionMappingID;";
        }

        public int ClearHoldsByHoldType(int assessmentId, int holdTypeId, int userId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@UserID", userId);
            parameters.Add("@HoldTypeID", holdTypeId);

            int holdsCleared;

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                holdsCleared = cn.Execute(GetClearHoldsByHoldTypeSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return holdsCleared;
        }

        private static string GetClearHoldsByHoldTypeSql()
        {
            return @"UPDATE [dbo].[AssessmentHold]
SET ClearedByUserID = @UserID,
ClearedDate = GETDATE()
WHERE HoldTypeID = @HoldTypeID AND 
ClearedDate IS NULL AND AssessmentID = @AssessmentID;";
        }

        public bool AssessmentHoldExists(int assessmentId, int holdTypeId)
        {
            bool exists;

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@HoldTypeID", holdTypeId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                exists = cn.QueryFirst<bool>(GetAssessmentHoldExistsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return exists;
        }

        private static string GetAssessmentHoldExistsSql()
        {
            return @"DECLARE @Exists BIT = 0;

SELECT @Exists = 1
FROM [dbo].[AssessmentHold]
WHERE HoldTypeID = @HoldTypeID AND ClearedDate IS NULL AND AssessmentID = @AssessmentID;

SELECT @Exists;";
        }

        public HoldType LoadHoldType(int exceptionMappingId)
        {
            HoldType holdType;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionMappingID", exceptionMappingId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                holdType = cn.QueryFirstOrDefault<HoldType>(GetLoadHoldTypeSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return holdType;
        }

        private static string GetLoadHoldTypeSql()
        {
            return @"SELECT em.Value AS HoldTypeID, ht.DisplayText
FROM ExceptionMapping em
LEFT JOIN dbo.HoldType ht ON em.Value = ht.HoldTypeID
WHERE ExceptionMappingID = @ExceptionMappingID";
        }

        public void CreateAssessmentHold(AssessmentHoldRequest assessmentHoldRequest)
        {
            if (assessmentHoldRequest == null) throw new ArgumentNullException(nameof(assessmentHoldRequest));

            var parameters = new DynamicParameters();
            parameters.AddDynamicParams(
                new
                {
                    assessmentHoldRequest.AssessmentId,
                    assessmentHoldRequest.HoldTypeId,
                    assessmentHoldRequest.UserId,
                    assessmentHoldRequest.IsManualHold,
                    assessmentHoldRequest.HoldReasonId
                });

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Execute(GetCreateAssessmentHoldSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }
        }

        private static string GetCreateAssessmentHoldSql()
        {
            return @"INSERT INTO [dbo].[AssessmentHold] (AssessmentID, HoldTypeID, HoldDate, CreatedByUserID, ManualHold, HoldReasonID)
VALUES (@AssessmentID, @HoldTypeID, GETDATE(), @UserID, @IsManualHold, @HoldReasonID);";
        }

        public IEnumerable<ExceptionSuppressReportSection> LoadExceptionSuppressReportSections(int exceptionMappingId)
        {
            IEnumerable<ExceptionSuppressReportSection> suppressReportSections;

            var parameters = new DynamicParameters();
            parameters.Add("@ExceptionMappingID", exceptionMappingId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                suppressReportSections = cn.Query<ExceptionSuppressReportSection>(GetLoadExceptionSuppressReportSectionActionsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return suppressReportSections;
        }

        private static string GetLoadExceptionSuppressReportSectionActionsSql()
        {
            return @"SELECT [ExceptionMappingSuppressReportSectionID], [ExceptionMappingID], [ReportSection]
FROM [Enterprise].[dbo].[ExceptionMappingSuppressReportSection]
WHERE ExceptionMappingID = @ExceptionMappingID;";
        }

        public IEnumerable<AssessmentExceptionSuppressReportSection> LoadAssessmentExceptionSuppressReportSections(int assessmentId)
        {
            IEnumerable<AssessmentExceptionSuppressReportSection> suppressReportSections;

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                suppressReportSections = cn.Query<AssessmentExceptionSuppressReportSection>(GetLoadAssessmentExceptionSuppressReportSectionsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return suppressReportSections;
        }

        private static string GetLoadAssessmentExceptionSuppressReportSectionsSql()
        {
            return @"SELECT AssessmentExceptionSuppressReportSectionID, AssessmentExceptionID, AssessmentID, ReportSection
FROM AssessmentExceptionSuppressReportSection
WHERE (AssessmentID = @AssessmentID);";
        }

        public int CreateAssessmentSuppressReport(AssessmentSuppressReportRequest assessmentSuppressReportRequest)
        {
            if (assessmentSuppressReportRequest == null) throw new ArgumentNullException(nameof(assessmentSuppressReportRequest));

            var parameters = new DynamicParameters();
            parameters.AddDynamicParams(
                new
                {
                    assessmentSuppressReportRequest.AssessmentId,
                    assessmentSuppressReportRequest.ExceptionCaseId,
                    assessmentSuppressReportRequest.Description,
                    assessmentSuppressReportRequest.ReportSection,
                    assessmentSuppressReportRequest.AssessmentExceptionId
                });

            int retrievedAssessmentExceptionId;

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                retrievedAssessmentExceptionId = cn.QueryFirst<int>(GetCreateAssessmentSuppressReportSql(), parameters,
                    commandType: System.Data.CommandType.Text);
            }

            return retrievedAssessmentExceptionId;
        }

        private static string GetCreateAssessmentSuppressReportSql()
        {
            return @"MERGE dbo.AssessmentException AS t
USING (
    SELECT @AssessmentExceptionID AS AssessmentExceptionID 
) AS s ON s.AssessmentExceptionID = t.AssessmentExceptionID
WHEN NOT MATCHED BY TARGET THEN
INSERT(AssessmentID, ExceptionCaseID, CreateDate, Description, IsOverridden, ConditionsMet, ActionsTaken)
VALUES (@AssessmentID, @ExceptionCaseID, GETDATE(), @Description, 0, '', '');

SET @AssessmentExceptionID = ISNULL(@AssessmentExceptionID, SCOPE_IDENTITY());

INSERT INTO [dbo].[AssessmentExceptionSuppressReportSection]
(AssessmentExceptionID, AssessmentID, ReportSection)
VALUES(@AssessmentExceptionID, @AssessmentID, @ReportSection);

SELECT @AssessmentExceptionID;";
        }

        public void ClearAssessmentPricing(int assessmentId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Execute("spClearAssessmentPricing",
                    parameters, commandType: System.Data.CommandType.StoredProcedure);
            }
        }

        public string LoadPickListDisplayTextById(int pickListId, string code)
        {
            string displayText;

            var parameters = new DynamicParameters();
            parameters.Add("@PickListID", pickListId);
            parameters.Add("@Code", code);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                displayText = cn.QueryFirstOrDefault<string>(GetLoadPickListDisplayTextByIdSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return displayText ?? string.Empty;
        }

        private static string GetLoadPickListDisplayTextByIdSql()
        {
            return @"SELECT pli.DisplayText
FROM dbo.PickListItem pli
WHERE pli.PickListID = @PickListID AND pli.Code = @Code;";
        }

        public string LoadPickListDisplayTextByViewName(string viewName, string code)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ViewName", viewName);
            parameters.Add("@Code", code);

            string displayText;
            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                displayText = cn.QueryFirstOrDefault<string>(GetLoadPickListDisplayTextByViewNameSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return displayText ?? string.Empty;
        }

        private static string GetLoadPickListDisplayTextByViewNameSql()
        {
            return @"SELECT pli.DisplayText
FROM dbo.PickListItem pli
INNER JOIN dbo.PickList pl ON pli.PickListID = pl.PickListID
WHERE pl.ViewName = @ViewName AND pli.Code = @Code;";
        }

        public string LoadPickListDisplayTextDynamic(string lookupTable, string lookupColumn, string lookupReturnColumn, string code)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@ReturnColumn", lookupReturnColumn);
            parameters.Add("@LookupTable", lookupTable);
            parameters.Add("@LookupColumn", lookupColumn);
            parameters.Add("@Code", code);

            string displayText;
            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                displayText = cn.QueryFirstOrDefault<string>(GetLoadPickListDisplayTextDynamicSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return displayText ?? string.Empty;
        }

        private static string GetLoadPickListDisplayTextDynamicSql()
        {
            return @"DECLARE @SqlToExecute VARCHAR(MAX);

SELECT @SqlToExecute = 'SELECT ' + QUOTENAME(@ReturnColumn) + ' AS DisplayText FROM ' + QUOTENAME(@LookupTable) + ' WHERE ' +
QUOTENAME(@LookupColumn) + ' = ''' + @Code + ''';'

EXEC (@SqlToExecute);";
        }

        public int UpdateAssessmentException(int assessmentId, int exceptionCaseId, Collection<string> actionsTaken, string conditionsMet, int? assessmentExceptionId)
        {
            if (actionsTaken == null) throw new ArgumentNullException(nameof(actionsTaken));

            var actions = new StringBuilder();

            foreach(var actionTaken in actionsTaken)
            {
                actions.Append(actionTaken);
            }

            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@ExceptionCaseID", exceptionCaseId);
            parameters.Add("@ActionsTaken", actions.ToString());
            parameters.Add("@ConditionsMet", conditionsMet);
            parameters.Add("@AssessmentExceptionID", assessmentExceptionId);


            int retrievedAssessmentExceptionId;
            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                retrievedAssessmentExceptionId = cn.QueryFirst<int>(GetUpdateAssessmentExceptionSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return retrievedAssessmentExceptionId;
        }

        private static string GetUpdateAssessmentExceptionSql()
        {
            return @"MERGE dbo.AssessmentException AS t
USING (
    SELECT @AssessmentExceptionID AS AssessmentExceptionID 
) AS s ON s.AssessmentExceptionID = t.AssessmentExceptionID
WHEN MATCHED THEN
UPDATE SET @AssessmentExceptionID = t.AssessmentExceptionID, ConditionsMet = @ConditionsMet, ActionsTaken = @ActionsTaken
WHEN NOT MATCHED BY TARGET THEN
INSERT(AssessmentID, ExceptionCaseID, CreateDate, Description, IsOverridden, ConditionsMet, ActionsTaken)
VALUES (@AssessmentID, @ExceptionCaseID, GETDATE(), NULL, 0, @ConditionsMet, @ActionsTaken);

SET @AssessmentExceptionID = ISNULL(@AssessmentExceptionID, SCOPE_IDENTITY());

SELECT @AssessmentExceptionID AS AssessmentExceptionID;";
        }

        public void SetAssessmentStatusToHold(int assessmentId, string holdStatusName)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@HoldStatusName", holdStatusName);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Execute(GetSetAssessmentStatusToHoldSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }
        }

        private static string GetSetAssessmentStatusToHoldSql()
        {
            return @"UPDATE dbo.Assessment
SET [Status] = @HoldStatusName
WHERE AssessmentID = @AssessmentID;";
        }

        public void UpdateLastValidationAssessmentExceptionIds(int assessmentId, string assessmentExceptionIds)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@AssessmentExceptionIds", assessmentExceptionIds);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Execute(GetUpdateLastValidationAssessmentExceptionIdsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }
        }

        private static string GetUpdateLastValidationAssessmentExceptionIdsSql()
        {
            return @"UPDATE dbo.Assessment
SET LastValidationAssessmentExceptionIDs = @AssessmentExceptionIds
WHERE AssessmentID = @AssessmentID;";
        }

        public void ClearAssessmentHolds(int assessmentId, int userId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@UserID", userId);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Execute(GetClearAssessmentHoldsSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }
        }

        private static string GetClearAssessmentHoldsSql()
        {
            return @"UPDATE dbo.AssessmentHold
SET ClearedDate = GETDATE(), ClearedByUserID = @UserID
WHERE AssessmentID = @AssessmentID AND ClearedDate IS NULL AND ManualHold = 0;";
        }

        public void ClearAssessmentStatus(int assessmentId, string holdStatusName)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);
            parameters.Add("@HoldStatusName", holdStatusName);

            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                cn.Execute(GetClearAssessmentStatusSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }
        }

        private static string GetClearAssessmentStatusSql()
        {
            return @"UPDATE a
SET a.[Status] = NULL
FROM dbo.Assessment a
WHERE a.AssessmentID = @AssessmentID AND ISNULL(a.[Status],'') = @HoldStatusName
AND NOT EXISTS (
	SELECT 0 FROM dbo.AssessmentHold ah 
	WHERE ah.ClearedDate IS NULL AND ah.AssessmentID = a.AssessmentID
);";
        }

        public string LoadDisplayControlNumber(int assessmentId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@AssessmentID", assessmentId);

            string displayControlNumber;
            using (var cn = new SqlConnection(_connectionString))
            {
                cn.Open();
                displayControlNumber = cn.QueryFirst<string>(GetLoadDisplayControlNumberSql(),
                    parameters, commandType: System.Data.CommandType.Text);
            }

            return displayControlNumber;
        }

        private static string GetLoadDisplayControlNumberSql()
        {
            return @"SELECT DisplayControlNumber
FROM Assessment
WHERE AssessmentID = @AssessmentID;";
        }
    }
}
