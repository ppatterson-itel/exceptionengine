﻿namespace ExceptionEngine.Repositories
{
    public class AssessmentCommentRequest
    {
        public int AssessmentId { get; set; }
        public int ExceptionCaseId { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public string Comment { get; set; }
        public string ReportSection { get; set; }
        public int? SourceCommentId { get; set; }
        public int? AssessmentExceptionId { get; set; }
    }
}
