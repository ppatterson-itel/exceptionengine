﻿namespace ExceptionEngine
{
    public class AssessmentActionsRequest
    {
        public int ExceptionId { get; set; }
        public int ExceptionCaseId { get; set; }
        public string Description { get; set; }
    }
}