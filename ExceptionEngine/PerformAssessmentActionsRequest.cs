﻿using System.Collections.Generic;

namespace ExceptionEngine
{
    public class PerformAssessmentActionsRequest
    {
        public int AssessmentId { get; set; }

        public int UserId { get; set; }
        public string UserEmail { get; set; }
        public IEnumerable<AssessmentActionsRequest> AssessmentActionsRequests { get; set; }
    }
}