﻿namespace ExceptionEngine
{
    public enum ProcessExceptionsResult
    {
        ErrorOccurred,
        ValidationSkipped,
        ValidationPerformed
    }
}
