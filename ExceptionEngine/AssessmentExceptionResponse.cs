﻿using ExceptionEngine.Exceptions.DataTransferObjects;
using System.Collections.ObjectModel;

namespace ExceptionEngine
{
    public class AssessmentExceptionResponse
    {
        public AssessmentExceptionResponse()
        {
            ExceptionResponses = new Collection<ExceptionResponse>();
        }

        public int AssessmentId { get; set; }

        public Collection<ExceptionResponse> ExceptionResponses { get; }
    }
}
