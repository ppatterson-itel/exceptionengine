﻿// ReSharper disable CommentTypo
namespace ExceptionEngine
{
    public enum FilterOperator
    {
        Absent = 0,         // filter isn't set for this attribute
        Complex = 1,        // complex

        ValueExists = 2,    // any value exists except null
        ValueIsAbsent = 3,  // there is no row or value is null

        Equal = 4,          // number, datetime, lookup, bit
        NotEqual = 5,       // number, datetime, lookup
        Less = 6,           // number, datetime
        Greater = 7,        // number, datetime
        Between = 8,		// number, datetime
        NotBetween = 16,    // number, datetime

        StartsWith = 9,     // string
        Contains = 10,      // string
        DoesNotContain = 11, // string

        FitRegularExpression = 12,     // string                 // used only for: ExceptionCondition
        NotFitRegularExpression = 13,  // string                 // used only for: ExceptionCondition
        InList = 14,        // string, number, lookup // used only for: ExceptionCondition, TableMaintFilter
        NotInList = 15      // string, number, lookup // used only for: ExceptionCondition, TableMaintFilter
    }
}
