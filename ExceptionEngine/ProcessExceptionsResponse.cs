﻿namespace ExceptionEngine
{
    public class ProcessExceptionsResponse
    {
        public ProcessExceptionsResult Result { get; set; }

        public AssessmentExceptionResponse AssessmentExceptionResponse { get; set; }

        public string ResultMessage { get; internal set; }
    }
}
