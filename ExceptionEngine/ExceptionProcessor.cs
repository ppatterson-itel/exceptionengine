﻿using ExceptionEngine.Actions;
using ExceptionEngine.Exceptions;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExceptionEngine
{
    public class ExceptionProcessor
    {
        private readonly IExceptionRepository _exceptionRepository;
        private readonly IActionRepository _actionRepository;
        private readonly ISendEmails _emailSender;

        public ExceptionProcessor(IExceptionRepository exceptionRepository, IActionRepository actionRepository, ISendEmails emailSender)
        {
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
            _actionRepository = actionRepository ?? throw new ArgumentNullException(nameof(actionRepository));
            _emailSender = emailSender ?? throw new ArgumentNullException(nameof(emailSender));
        }

        public ProcessExceptionsResponse ProcessExceptions(int assessmentId)
        {
            var assessmentExists = _exceptionRepository.AssessmentExists(assessmentId);

            if (!assessmentExists)
            {
                return new ProcessExceptionsResponse
                {
                    ResultMessage = $"{assessmentId} doesn't exist.",
                    Result = ProcessExceptionsResult.ErrorOccurred
                };
            }

            Assessment assessment = new Assessment(assessmentId, _exceptionRepository);

            IEnumerable<ExceptionCaseResponse> result = CheckPreruleConditions(assessment).ToList();

            if (result.Any())
            {
                if(result.All(r => r.Result))
                {
                    return GetPreruleResponse(result, assessment);
                }
            }

            result = CheckExceptions(assessment);

            return GetResponse(result, assessment);
        }

        public ProcessExceptionsResponse ProcessExceptions(int assessmentId, IEnumerable<string> groupNames)
        {
            var assessmentExists = _exceptionRepository.AssessmentExists(assessmentId);

            if (!assessmentExists)
            {
                return new ProcessExceptionsResponse
                {
                    ResultMessage = $"{assessmentId} doesn't exist.",
                    Result = ProcessExceptionsResult.ErrorOccurred
                };
            }

            Assessment assessment = new Assessment(assessmentId, _exceptionRepository);

            IEnumerable<ExceptionCaseResponse> result = CheckPreruleConditions(assessment).ToList();

            if (result.Any())
            {
                if (result.All(r => r.Result))
                {
                    return GetPreruleResponse(result, assessment);
                }
            }

            result = groupNames != null && groupNames.Any() ? CheckExceptions(assessment, groupNames) : CheckExceptions(assessment);

            return GetResponse(result, assessment);
        }

        public PerformAssessmentActionsResponse PerformAssessmentActions(PerformAssessmentActionsRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var processResponse = new PerformAssessmentActionsResponse();
            var processedExceptions = new List<int>();

            var assessmentExceptionIDs = new StringBuilder();
            var systemSettings = new SystemSettings(_exceptionRepository);

            if (request.AssessmentActionsRequests.Any())
            {
                //Potentialy parallel foreach
                //throw new NotImplementedException("try out Parallel.ForEach");
                foreach (var assessmentActionsRequest in request.AssessmentActionsRequests)
                {
                    var response = PerformActions(assessmentActionsRequest.ExceptionId, assessmentActionsRequest.ExceptionCaseId, 
                        request.AssessmentId, assessmentActionsRequest.Description, request.UserEmail,
                        request.UserId, processedExceptions.Contains(assessmentActionsRequest.ExceptionId),
                        systemSettings);

                    assessmentExceptionIDs.Append($"{response.AssessmentExceptionId};");

                    processResponse.ActionsResponses.Add(response);
                    processedExceptions.Add(assessmentActionsRequest.ExceptionId);
                }
            }
            else
            {
                ClearHolds(request.AssessmentId, request.UserId, systemSettings);
            }

            _emailSender.SendQueuedEmails();

            _actionRepository.UpdateLastValidationAssessmentExceptionIds(request.AssessmentId, assessmentExceptionIDs.ToString());

            return processResponse;
        }

        private void ClearHolds(int assessmentId, int userId, SystemSettings systemSettings)
        {

            _actionRepository.ClearAssessmentHolds(assessmentId, userId);
            if (systemSettings.ClearHoldIfNoExceptions == "1")
            {
                _actionRepository.ClearAssessmentStatus(assessmentId, systemSettings.HoldStatusName);
            }
        }

        private ActionsResponse PerformActions(int exceptionId, int exceptionCaseId, int assessmentId, string description,
            string userEmail, int userId, bool exceptionProcessed, SystemSettings systemSettings)
        {
            var exceptionActionFactory = new ExceptionActionFactory(_actionRepository, _emailSender);
            var actionsToPerform = exceptionActionFactory.GetExceptionActions(exceptionId, exceptionCaseId, assessmentId, description,
                userEmail, userId, systemSettings).ToList();
            var responses = new List<ExceptionActionResponse>(actionsToPerform.Count);

            int? assessmentExceptionId = null;

            foreach (var action in actionsToPerform)
            {
                var actionResponse = action.PerformExceptionAction(exceptionProcessed, assessmentExceptionId);

                assessmentExceptionId = actionResponse.AssessmentExceptionId;

                if (!actionResponse.ActionSkipped)
                {
                    responses.Add(actionResponse);
                }
            }

            var exceptionCaseData = _actionRepository.LoadExceptionCaseData(exceptionCaseId);

            var actionsResponse = new ActionsResponse
            {
                ConditionsMet = exceptionCaseData.GetConditionsMet(_actionRepository, exceptionCaseId)
            };

            foreach (var response in responses)
            {
                actionsResponse.ActionsTaken.Add(response.ActionsTaken);
            }

            actionsResponse.AssessmentExceptionId = _actionRepository.UpdateAssessmentException(assessmentId, exceptionCaseId, 
                actionsResponse.ActionsTaken, actionsResponse.ConditionsMet, assessmentExceptionId);

            return actionsResponse;
        }

        private IEnumerable<ExceptionCaseResponse> CheckExceptions(Assessment assessment)
        {
            var assessmentExceptions = _exceptionRepository.LoadAssessmentExceptions(assessment.AssessmentId);
            var responses = new BlockingCollection<ExceptionCaseResponse>();

            //Parallel.ForEach(assessmentExceptions.Exceptions, exception =>
            //{
            //    var factory = new ExceptionCaseFactory(_exceptionRepository);
            //    var exceptionCases = factory.GetExceptionCases(exception, assessment);

            //    Parallel.ForEach(exceptionCases,
            //        exceptionCase => { responses.Add(exceptionCase.EvaluateExceptionCase()); });
            //});

            foreach(var exception in assessmentExceptions.Exceptions)
            {
                var factory = new ExceptionCaseFactory(_exceptionRepository);
                var exceptionCases = factory.GetExceptionCases(exception, assessment);
                foreach (var exceptionCase in exceptionCases)
                {
                    responses.Add(exceptionCase.EvaluateExceptionCase());
                }
            }

            return responses;
        }

        private IEnumerable<ExceptionCaseResponse> CheckExceptions(Assessment assessment, IEnumerable<string> groupNames)
        {
            var assessmentExceptions = _exceptionRepository.LoadAssessmentExceptions(assessment.AssessmentId);
            var responses = new List<ExceptionCaseResponse>();
            var enumerable = groupNames.ToList();

            foreach (var exception in assessmentExceptions.Exceptions)
            {
                if (!enumerable.Contains(exception.ExceptionGroup)) continue;

                var factory = new ExceptionCaseFactory(_exceptionRepository);
                var exceptionCases = factory.GetExceptionCases(exception, assessment);

                responses.AddRange(exceptionCases.Select(exceptionCase => exceptionCase.EvaluateExceptionCase()));
            }

            return responses;
        }

        private static ProcessExceptionsResponse GetResponse(IEnumerable<ExceptionCaseResponse> exceptionCaseResponses, Assessment assessment)
        {
            var response = new ProcessExceptionsResponse
            {
                ResultMessage = "Validation successfully performed.",
                Result = ProcessExceptionsResult.ValidationPerformed
            };

            var assessmentResponse = new AssessmentExceptionResponse
            {
                AssessmentId = assessment.AssessmentId
            };

            var enumerable = exceptionCaseResponses.ToList();

            if (enumerable.Any(r => r.Result))
            {
                var caseResponses = GenerateExceptionResponses(enumerable.Where(r => r.Result));

                foreach (var caseResponse in caseResponses)
                {
                    assessmentResponse.ExceptionResponses.Add(caseResponse);
                }
            }

            response.AssessmentExceptionResponse = assessmentResponse;

            return response;
        }

        private static IEnumerable<ExceptionResponse> GenerateExceptionResponses(IEnumerable<ExceptionCaseResponse> exceptionCaseResponses)
        {
            var caseResponses = exceptionCaseResponses.ToList();
            var exceptionResponses = new List<ExceptionResponse>(caseResponses.Count);

            foreach(var response in caseResponses)
            {
                exceptionResponses.Add(new ExceptionResponse
                {
                    ExceptionId = response.ExceptionId,
                    ExceptionName = response.ExceptionName,
                    Condition = response.Condition,
                    Description = response.Description,
                    ExceptionCaseId = response.ExceptionCaseId,
                    ExceptionCaseName = response.ExceptionCaseName,
                    ExceptionCaseDescription = response.ExceptionCaseDescription,
                    ExceptionCaseDisplayInSummary = response.ExceptionCaseDisplayInSummary,
                    ExceptionGroup = response.ExceptionGroup
                });
            }

            return exceptionResponses;
        }

        private static ProcessExceptionsResponse GetPreruleResponse(IEnumerable<ExceptionCaseResponse> exceptionCaseResponses, Assessment assessment)
        {
            var response = new ProcessExceptionsResponse
            {
                ResultMessage = "Skipped due to prerule validation.",
                Result = ProcessExceptionsResult.ValidationSkipped
            };

            var assessmentResponse = new AssessmentExceptionResponse
            {
                AssessmentId = assessment.AssessmentId
            };

            var enumerable = exceptionCaseResponses.ToList();

            if (!enumerable.Any(r => r.Result))
                return response;

            var caseResponses = GenerateExceptionResponses(enumerable.Where(r => r.Result));

            foreach (var caseResponse in caseResponses)
            {
                assessmentResponse.ExceptionResponses.Add(caseResponse);
            }

            return response;
        }

        private IEnumerable<ExceptionCaseResponse> CheckPreruleConditions(Assessment assessment)
        {
            var preruleExceptionConditions = _exceptionRepository.LoadPreRuleConditions(assessment.AssessmentId);
            var responses = new List<ExceptionCaseResponse>();

            foreach (var exception in preruleExceptionConditions.Exceptions)
            {
                var factory = new ExceptionCaseFactory(_exceptionRepository);
                var exceptionCases = factory.GetExceptionCases(exception, assessment);

                responses.AddRange(exceptionCases.Select(exceptionCase => exceptionCase.EvaluateExceptionCase()));
            }

            return responses;
        }
    }
}
