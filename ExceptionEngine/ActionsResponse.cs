﻿using System.Collections.ObjectModel;

namespace ExceptionEngine
{
    public class ActionsResponse
    {
        public int AssessmentExceptionId { get; set; }
        public string ConditionsMet { get; set; }

        public Collection<string> ActionsTaken { get; }

        public ActionsResponse()
        {
            ActionsTaken = new Collection<string>();
        }
    }
}