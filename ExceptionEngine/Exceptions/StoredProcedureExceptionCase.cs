﻿using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System;
using Exception = ExceptionEngine.Exceptions.DataTransferObjects.Exception;

namespace ExceptionEngine.Exceptions
{
    public class StoredProcedureExceptionCase : IExceptionCase
    {
        private readonly string _conditionStoredProcedure;
        private readonly int _assessmentId;
        private readonly IExceptionRepository _exceptionRepository;
        private readonly Exception _exception;
        private readonly ExceptionCaseDto _exceptionCase;

        public StoredProcedureExceptionCase(string conditionStoredProcedure, int assessmentId, ExceptionCaseDto exceptionCase, IExceptionRepository exceptionRepository,
            Exception exception)
        {
            _conditionStoredProcedure = conditionStoredProcedure ?? throw new ArgumentNullException(nameof(conditionStoredProcedure));
            _assessmentId = assessmentId;
            _exceptionCase = exceptionCase ?? throw new ArgumentNullException(nameof(exceptionCase));
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
            _exception = exception ?? throw new ArgumentNullException(nameof(exception));
        }

        public ExceptionCaseResponse EvaluateExceptionCase()
        {
            return _exceptionRepository.ExecuteExceptionStoredProcedure(_conditionStoredProcedure, _assessmentId, _exceptionCase, _exception);
        }
    }
}
