﻿using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions
{
    public interface IExceptionCase
    {
        ExceptionCaseResponse EvaluateExceptionCase();
    }
}
