﻿using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using ExceptionEngine.Actions;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using System.Text;
using System.Collections.ObjectModel;

namespace ExceptionEngine.Exceptions
{
    public class AssessmentPropertiesExceptionCase : IExceptionCase
    {
        private readonly ExceptionCaseDto _exceptionCase;
        private readonly int _assessmentId;
        private readonly IExceptionRepository _exceptionRepository;
        private readonly DataTransferObjects.Exception _exception;
        private readonly AssessmentViews _assessmentViews;

        public Collection<ExceptionCondition> ExceptionConditions
        {
            get { return _exceptionCase.ExceptionConditions; }
        }

        public AssessmentPropertiesExceptionCase(ExceptionCaseDto exceptionCase, int assessmentId,
            IExceptionRepository exceptionRepository, DataTransferObjects.Exception exception, AssessmentViews assessmentViews)
        {
            _exceptionCase = exceptionCase ?? throw new ArgumentNullException(nameof(exceptionCase));
            _assessmentId = assessmentId;
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
            _exception = exception ?? throw new ArgumentNullException(nameof(exception));
            _assessmentViews = assessmentViews ?? throw new ArgumentNullException(nameof(assessmentViews));
        }

        public ExceptionCaseResponse EvaluateExceptionCase()
        {
            if (!ExceptionConditions.Any())
                return new ExceptionCaseResponse(_exceptionCase, _exception)
                {
                    Condition = "There are no exception conditions to evaluate against.",
                    Result = false
                };
            
            var propertyLookup = _assessmentViews.GetAssessmentView(_exceptionCase.ExceptionViewId);

            if (!ProductTypesValidForEvaluation(propertyLookup))
                return new ExceptionCaseResponse(_exceptionCase, _exception)
                {
                    Condition = $"Exception Case evaluation for ExceptionCaseID {_exceptionCase.ExceptionCaseId} skipped because ProductTypes are invalid.",
                    Result = false
                };

            StringBuilder descriptionBuilder = new StringBuilder();

            foreach (var conditionToCheck in ExceptionConditions)
            {
                var matched = false;
                dynamic propertyToCheck = null;

                if (propertyLookup.Count > 0)
                {
                    propertyToCheck = propertyLookup[conditionToCheck.AttributeColumn];
                    matched = WasExceptionConditionMatched(propertyToCheck, conditionToCheck);
                }

                if (matched)
                {
                    descriptionBuilder.Append(conditionToCheck.AttributeColumn);
                    if (propertyToCheck == null)
                        descriptionBuilder.Append(" is null");
                    else
                        descriptionBuilder.Append($" = {propertyToCheck}");

                    descriptionBuilder.Append("; ");

                    continue;
                }

                return new ExceptionCaseResponse(_exceptionCase, _exception)
                {
                    Condition = "Property did not match for exception case",
                    Description = string.Empty,
                    Result = false
                };
            }

            return new ExceptionCaseResponse(_exceptionCase, _exception)
            {
                Condition = "All properties matched for exception case",
                Description = descriptionBuilder.ToString(),
                Result = true
            };
        }

        private bool ProductTypesValidForEvaluation(IReadOnlyDictionary<string, dynamic> propertyLookup)
        {
            int? productTypeId = null;

            if (propertyLookup.ContainsKey("ProductTypeID"))
            {
                productTypeId = propertyLookup["ProductTypeID"];
            }

            var assessmentViewProductData = _assessmentViews.GetAssessmentViewProductData(productTypeId);

            if (assessmentViewProductData.AssessmentProductType == null)
                return true;
            if (string.IsNullOrEmpty(assessmentViewProductData.ExceptionViewProductType))
                return true;

            return EqualityEvaluator.AreEqual(assessmentViewProductData.AssessmentProductType, assessmentViewProductData.ExceptionViewProductType);
        }

        private bool WasExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            var filterOperator = (FilterOperator)conditionToCheck.Operator;

            var conditionFilterFactory = new ExceptionConditionFilterFactory(_exceptionRepository);

            return conditionFilterFactory.GetConditionFilter(filterOperator).ExceptionConditionMatched(propertyToCheck, conditionToCheck);
        }
    }
}