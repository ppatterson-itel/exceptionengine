﻿using System;
using System.Linq;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class InListExceptionConditionFilter : IExceptionConditionFilter
    {
        private readonly IExceptionRepository _exceptionRepository;

        public InListExceptionConditionFilter(IExceptionRepository exceptionRepository)
        {
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
        }

        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            if (conditionToCheck == null) throw new ArgumentNullException(nameof(conditionToCheck));

            if (propertyToCheck == null) return false;

            var conditionValuesList = _exceptionRepository.LoadExceptionConditionValues(conditionToCheck.ExceptionConditionId);
            return conditionValuesList.Any(conditionValue => propertyToCheck.ToString().ToLower() == conditionValue.ConditionValue.ToString().ToLower());
        }
    }
}