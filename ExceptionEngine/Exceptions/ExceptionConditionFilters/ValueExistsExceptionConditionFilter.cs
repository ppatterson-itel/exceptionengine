﻿using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class ValueExistsExceptionConditionFilter : IExceptionConditionFilter
    {
        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            return propertyToCheck != null && propertyToCheck.ToString() != "";
        }
    }
}
