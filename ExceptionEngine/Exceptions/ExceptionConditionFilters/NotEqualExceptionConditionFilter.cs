﻿using System;
using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class NotEqualExceptionConditionFilter : IExceptionConditionFilter
    {
        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            if (conditionToCheck == null) throw new ArgumentNullException(nameof(conditionToCheck));

            var valueFrom = conditionToCheck.ValueFrom;

            return !valueFrom.Equals(propertyToCheck);
        }
    }
}