﻿using System;
using System.Globalization;
using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class ContainsExceptionConditionFilter : IExceptionConditionFilter
    {
        // https://stackoverflow.com/a/15464440 - better compare used
        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            if (conditionToCheck == null) throw new ArgumentNullException(nameof(conditionToCheck));

            var valueFrom = conditionToCheck.ValueFrom;

            return propertyToCheck != null && CultureInfo.InvariantCulture.CompareInfo.IndexOf(propertyToCheck.ToString(),
                       valueFrom.ToString(), CompareOptions.IgnoreCase) >= 0;
        }
    }
}