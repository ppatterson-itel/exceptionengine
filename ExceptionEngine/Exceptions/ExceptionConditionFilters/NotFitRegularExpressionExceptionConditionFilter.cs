﻿using System;
using System.Text.RegularExpressions;
using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class NotFitRegularExpressionExceptionConditionFilter : IExceptionConditionFilter
    {
        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            if (conditionToCheck == null) throw new ArgumentNullException(nameof(conditionToCheck));

            var valueFrom = conditionToCheck.ValueFrom;
            var regexToMatch = new Regex(valueFrom.ToString());

            return propertyToCheck != null && !regexToMatch.IsMatch(propertyToCheck.ToString());
        }
    }
}