﻿using System;
using System.Globalization;
using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    // https://stackoverflow.com/a/15464440 - better compare used
    public class DoesNotContainExceptionConditionFilter : IExceptionConditionFilter
    {

        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            if (conditionToCheck == null) throw new ArgumentNullException(nameof(conditionToCheck));

            var valueFrom = conditionToCheck.ValueFrom;

            return propertyToCheck != null && !(CultureInfo.InvariantCulture.CompareInfo.IndexOf(propertyToCheck.ToString(),
                valueFrom.ToString(), CompareOptions.IgnoreCase) >= 0);
        }
    }
}