﻿using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public interface IExceptionConditionFilter
    {
        bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck);
    }
}
