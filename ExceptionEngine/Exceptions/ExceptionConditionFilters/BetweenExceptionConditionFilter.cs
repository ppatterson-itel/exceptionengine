﻿using System;
using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class BetweenExceptionConditionFilter : IExceptionConditionFilter
    {
        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            if (conditionToCheck == null) throw new ArgumentNullException(nameof(conditionToCheck));

            var valueFrom = conditionToCheck.ValueFrom;
            var valueTo = conditionToCheck.ValueTo;

            return propertyToCheck != null && (((IComparable)propertyToCheck).CompareTo(valueFrom) > 0 &&
                        ((IComparable)propertyToCheck).CompareTo(valueTo) < 0);
        }
    }
}