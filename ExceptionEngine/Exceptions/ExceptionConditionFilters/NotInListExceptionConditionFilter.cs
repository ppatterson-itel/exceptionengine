﻿using System;
using System.Linq;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class NotInListExceptionConditionFilter : IExceptionConditionFilter
    {
        private readonly IExceptionRepository _exceptionRepository;

        public NotInListExceptionConditionFilter(IExceptionRepository exceptionRepository)
        {
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
        }

        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            if (conditionToCheck == null) throw new ArgumentNullException(nameof(conditionToCheck));

            if (propertyToCheck == null) return true;

            var conditionValuesList = _exceptionRepository.LoadExceptionConditionValues(conditionToCheck.ExceptionConditionId);

            return conditionValuesList.All(conditionValue => propertyToCheck.ToString().ToLower() != conditionValue.ConditionValue.ToString().ToLower());
        }
    }
}