﻿using System;
using ExceptionEngine.Exceptions.DataTransferObjects;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class StartsWithExceptionConditionFilter : IExceptionConditionFilter
    {
        public bool ExceptionConditionMatched(dynamic propertyToCheck, ExceptionCondition conditionToCheck)
        {
            if (conditionToCheck == null) throw new ArgumentNullException(nameof(conditionToCheck));

            string valueFrom = conditionToCheck.ValueFrom.ToString();

            if (propertyToCheck == null)
                return false;

            var propertyString = propertyToCheck.ToString();

            return propertyString.StartsWith(valueFrom, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}