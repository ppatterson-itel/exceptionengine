﻿using System;
using System.Collections.Generic;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Exceptions.ExceptionConditionFilters
{
    public class ExceptionConditionFilterFactory
    {
        private readonly Dictionary<FilterOperator, IExceptionConditionFilter> _lookupDictionary = new Dictionary<FilterOperator, IExceptionConditionFilter>();

        public ExceptionConditionFilterFactory(IExceptionRepository exceptionRepository)
        {
            if (exceptionRepository == null) throw new ArgumentNullException(nameof(exceptionRepository));

            _lookupDictionary.Add(FilterOperator.ValueExists, new ValueExistsExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.ValueIsAbsent, new ValueIsAbsentExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.Equal, new EqualExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.NotEqual, new NotEqualExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.Less, new LessThanExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.Greater, new GreaterThanExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.Between, new BetweenExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.NotBetween, new NotBetweenExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.StartsWith, new StartsWithExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.Contains, new ContainsExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.DoesNotContain, new DoesNotContainExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.FitRegularExpression, new FitRegularExpressionExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.NotFitRegularExpression, new NotFitRegularExpressionExceptionConditionFilter());
            _lookupDictionary.Add(FilterOperator.InList, new InListExceptionConditionFilter(exceptionRepository));
            _lookupDictionary.Add(FilterOperator.NotInList, new NotInListExceptionConditionFilter(exceptionRepository));
        }

        public IExceptionConditionFilter GetConditionFilter(FilterOperator filterOperator)
        {
            if (_lookupDictionary.ContainsKey(filterOperator))
                return _lookupDictionary[filterOperator];
            throw new ArgumentException($"No condition filter defined for filterOperator '{filterOperator:G}'.", nameof(filterOperator));
        }
    }
}
