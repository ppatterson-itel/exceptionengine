﻿using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExceptionEngine.Exceptions
{
    public class ExceptionCaseFactory
    {
        private readonly IExceptionRepository _exceptionRepository;

        public ExceptionCaseFactory(IExceptionRepository exceptionRepository)
        {
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
        }

        public IEnumerable<IExceptionCase> GetExceptionCases(DataTransferObjects.Exception exception, Assessment assessment)
        {
            if (exception == null)
                throw new ArgumentNullException(nameof(exception));

            if (assessment == null)
                throw new ArgumentNullException(nameof(assessment));

            var exceptionCases = new List<IExceptionCase>();

            // if case is overrideable and has been processed in the past and marked as overridden skip evaluation (ie do not even add it to the list)

            if (!string.IsNullOrWhiteSpace(exception.ConditionStoredProcedure))
            {
                var exceptionCase = exception.ExceptionCases.First();
                if (!exceptionCase.AllowOverride || !assessment.GetAssessmentCaseOverrides().Contains(exceptionCase.ExceptionCaseId))
                {
                    exceptionCases.Add(new StoredProcedureExceptionCase(exception.ConditionStoredProcedure, assessment.AssessmentId,
                        exceptionCase, _exceptionRepository, exception));
                }
            }
            else if (!string.IsNullOrWhiteSpace(exception.ConditionStoredMethod))
            {
                var exceptionCase = exception.ExceptionCases.First();
                if (!exceptionCase.AllowOverride || !assessment.GetAssessmentCaseOverrides().Contains(exceptionCase.ExceptionCaseId))
                {
                    var createdCase = GetExceptionCase(exception.ConditionStoredMethod, assessment.AssessmentId, exceptionCase,
                        exception);
                    if (createdCase != null)
                        exceptionCases.Add(createdCase);
                }
            }
            else
            {
                foreach (var exceptionCase in exception.ExceptionCases)
                {
                    if (!exceptionCase.AllowOverride || !assessment.GetAssessmentCaseOverrides().Contains(exceptionCase.ExceptionCaseId))
                    {
                        exceptionCases.Add(GetExceptionCase(exceptionCase, assessment.AssessmentId, exception, assessment.AssessmentViews));
                    }
                }
            }

            return exceptionCases;
        }

        private IExceptionCase GetExceptionCase(ExceptionCaseDto exceptionCase, int assessmentId, DataTransferObjects.Exception exception,
            AssessmentViews assessmentViews)
        {
            var propertyExceptionCase = new AssessmentPropertiesExceptionCase(exceptionCase, assessmentId, _exceptionRepository, exception,
                assessmentViews);

            return propertyExceptionCase;
        }

        private IExceptionCase GetExceptionCase(string conditionStoredMethod, int assessmentId, ExceptionCaseDto exceptionCase, DataTransferObjects.Exception exception)
        {

            switch(conditionStoredMethod)
            {
                case "CheckForAdjusterHolds":
                    return new CheckForAdjusterHolds(assessmentId, _exceptionRepository, exceptionCase, exception);
                case "CheckForCustomerARHolds":
                    return new CheckForCustomerArHolds(assessmentId, _exceptionRepository, exceptionCase, exception);
                default: return null;
            }
        }
    }
}
