﻿using System;
using System.Collections.Generic;
using ExceptionEngine.Repositories;

namespace ExceptionEngine.Exceptions
{
    public class AssessmentViews
    {
        private readonly int _assessmentId;
        private readonly IExceptionRepository _exceptionRepository;
        private Dictionary<int, Dictionary<string, dynamic>> _assessmentViews = new Dictionary<int, Dictionary<string, dynamic>>();
        private Dictionary<int, AssessmentViewProductData> _assessmentViewProductDatas =
            new Dictionary<int, AssessmentViewProductData>();
        private AssessmentViewProductData _nullProductData;

        private object _assessmentViewLock = new object(), _productDataLock = new object();

        public AssessmentViews(int assessmentId, IExceptionRepository exceptionRepository)
        {
            _assessmentId = assessmentId;
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
        }

        public Dictionary<string, dynamic> GetAssessmentView(int exceptionViewId)
        {
            if (!_assessmentViews.ContainsKey(exceptionViewId))
            {
                lock (_assessmentViewLock)
                {
                    if (!_assessmentViews.ContainsKey(exceptionViewId))
                    {
                        var assessmentViewData =
                            _exceptionRepository.LoadAssessmentViewData(_assessmentId, exceptionViewId);

                        _assessmentViews.Add(exceptionViewId, assessmentViewData);
                    }
                }
            }

            return _assessmentViews[exceptionViewId];
        }

        public AssessmentViewProductData GetAssessmentViewProductData(int? productTypeId)
        {
            if (productTypeId.HasValue)
                return GetNonNullAssessmentProductData(productTypeId.Value);
            else
                return GetNullAssessmentProductData();
        }

        private AssessmentViewProductData GetNullAssessmentProductData()
        {
            if (_nullProductData == null)
            {
                lock (_productDataLock)
                {
                    if (_nullProductData == null)
                    {
                        _nullProductData = _exceptionRepository.LoadAssessmentAndViewProductData(_assessmentId, null);
                    }
                }
            }

            return _nullProductData;
        }

        private AssessmentViewProductData GetNonNullAssessmentProductData(int productTypeId)
        {
            if (!_assessmentViewProductDatas.ContainsKey(productTypeId))
            {
                lock (_productDataLock)
                {
                    if (!_assessmentViewProductDatas.ContainsKey(productTypeId))
                    {
                        var assessmentViewProductData =
                            _exceptionRepository.LoadAssessmentAndViewProductData(_assessmentId, productTypeId);
                        _assessmentViewProductDatas.Add(productTypeId, assessmentViewProductData);
                    }
                }
            }

            return _assessmentViewProductDatas[productTypeId];
        }
    }
}
