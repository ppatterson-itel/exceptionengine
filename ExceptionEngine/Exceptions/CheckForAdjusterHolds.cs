﻿using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using System;

namespace ExceptionEngine.Exceptions
{
    public class CheckForAdjusterHolds : IExceptionCase
    {
        private readonly int _assessmentId;
        private readonly IExceptionRepository _exceptionRepository;
        private readonly ExceptionCaseDto _exceptionCase;
        private readonly DataTransferObjects.Exception _exception;

        public CheckForAdjusterHolds(int assessmentId, IExceptionRepository exceptionRepository, ExceptionCaseDto exceptionCase,
            DataTransferObjects.Exception exception)
        {
            _assessmentId = assessmentId;
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));
            _exceptionCase = exceptionCase ?? throw new ArgumentNullException(nameof(exceptionCase));
            _exception = exception ?? throw new ArgumentNullException(nameof(exception));
        }
        public ExceptionCaseResponse EvaluateExceptionCase()
        {
            var response = _exceptionRepository.CheckForAdjustHoldOnAssessment(_assessmentId, _exceptionCase, _exception);

            return response;
        }
    }
}
