﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace ExceptionEngine.Exceptions.DataTransferObjects
{
    public class ExceptionCaseDto
    {
        [Required]
        public int ExceptionCaseId { get; set; }

        [Required]
        public int ExceptionId { get; set; }

        [Required]
        public int ExceptionAccessGroupId { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        [Required]
        public int CreatedByUserId { get; set; }

        [Required]
        public DateTime CreateDate { get; set; }

        [Required]
        public int LastModifiedByUserId { get; set; }

        [Required]
        public DateTime LastModifiedDate { get; set; }

        [Required]
        public int ExceptionViewId { get; set; }

        [Required]
        public bool AllowOverride { get; set; }

        [Required]
        [MaxLength(200)]
        public string Description { get; set; }

        [Required]
        public bool DisplayInSummary { get; set; }

        [Required]
        public bool DisplayDetailedExceptionInSummary { get; set; }

        public Collection<ExceptionCondition> ExceptionConditions { get; } = new Collection<ExceptionCondition>();

    }


}
