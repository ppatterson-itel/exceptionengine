﻿namespace ExceptionEngine.Exceptions.DataTransferObjects
{
    public class ExceptionCondition
    {
        public int ExceptionConditionId { get; set; }
        public int ExceptionAttributeMetadataId { get; set; }
        public int Operator { get; set; }
        public dynamic ValueFrom { get; set; }
        public dynamic ValueTo { get; set; }
        public string AttributeKey { get; set; }
        public string AttributeColumn { get; set; }
        public string DataType { get; set; }
        public string LookupTable { get; set; }
        public string LookupColumn { get; set; }
        public string LookupReturnColumn { get; set; }
        public int ExceptionViewId { get; set; }
    }
}
