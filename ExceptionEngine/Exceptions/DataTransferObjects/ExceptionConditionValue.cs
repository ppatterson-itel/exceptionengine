﻿namespace ExceptionEngine.Exceptions.DataTransferObjects
{
    public class ExceptionConditionValue
    {
        public int ExceptionConditionValuesListId { get; set; }
        public dynamic ConditionValue { get; set; }
    }
}
