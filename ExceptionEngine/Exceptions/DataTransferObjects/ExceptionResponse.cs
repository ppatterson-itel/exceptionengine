﻿namespace ExceptionEngine.Exceptions.DataTransferObjects
{
    public class ExceptionResponse
    {
        public int ExceptionId { get; set; }
        public string ExceptionName { get; set; }
        public string Description { get; set; }
        public string Condition { get; set; }
        public int ExceptionCaseId { get; set; }
        public string ExceptionCaseName { get; set; }
        public string ExceptionCaseDescription { get; set; }
        public bool ExceptionCaseDisplayInSummary { get; set; }
        public string ExceptionGroup { get; set; }
    }
}