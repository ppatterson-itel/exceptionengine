﻿using System;

namespace ExceptionEngine.Exceptions.DataTransferObjects
{
    public class ExceptionCaseResponse
    {
        private readonly ExceptionCaseDto _exceptionCaseDto;
        private readonly Exception _exception;

        public ExceptionCaseResponse(ExceptionCaseDto exceptionCase, Exception exception)
        {
            _exceptionCaseDto = exceptionCase ?? throw new ArgumentNullException(nameof(exceptionCase));
            _exception = exception ?? throw new ArgumentNullException(nameof(exception));
        }

        public bool Result { get; set; }

        public string Description { get; set; }
        public string Condition { get; set; }

        public int ExceptionId => _exceptionCaseDto.ExceptionId;
        public string ExceptionName => _exception.Name;
        public int ExceptionCaseId => _exceptionCaseDto.ExceptionCaseId;
        public string ExceptionCaseName => _exceptionCaseDto.Name;
        public string ExceptionCaseDescription => _exceptionCaseDto.Description;
        public bool ExceptionCaseDisplayInSummary => _exceptionCaseDto.DisplayInSummary;
        public string ExceptionGroup => _exception.ExceptionGroup;
    }
}
