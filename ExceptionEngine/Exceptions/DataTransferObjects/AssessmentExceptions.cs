﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ExceptionEngine.Exceptions.DataTransferObjects
{
    public class AssessmentExceptions
    {
        public AssessmentExceptions(int assessmentId)
        {
            AssessmentId = assessmentId;
            Exceptions = new Collection<Exception>();
        }

        public void AddAssessmentExceptions(IEnumerable<Exception> assessmentExceptions)
        {
            if (assessmentExceptions == null) return;

            foreach (var assessmentException in assessmentExceptions)
            {
                Exceptions.Add(assessmentException);
            }
        }

        public int AssessmentId { get; }
        public Collection<Exception> Exceptions { get; }
    }
}
