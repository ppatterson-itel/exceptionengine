﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace ExceptionEngine.Exceptions.DataTransferObjects
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")]
    public class Exception
    {
        public Exception()
        {
            ExceptionCases = new Collection<ExceptionCaseDto>();
        }

        [Required]
        public int ExceptionId { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public bool IsSystem { get; set; }

        [MaxLength(100)]
        public string ConditionStoredProcedure { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        [Required]
        public DateTime CreateDate { get; set; }

        [Required]
        public DateTime LastModifiedDate { get; set; }

        [MaxLength(100)]
        public string ConditionStoredMethod { get; set; }

        [Required]
        public int ExecutionOrder { get; set; }

        [Required]
        public bool Active { get; set; }

        [MaxLength(255)]
        public string ExceptionGroup { get; set; }

        [Required]
        public bool PreRule { get; set; }

        public Collection<ExceptionCaseDto> ExceptionCases { get; }
    }


}
