﻿using ExceptionEngine.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ExceptionEngine.Exceptions
{
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    public class Assessment
    {
        private readonly IExceptionRepository _exceptionRepository;
        private IEnumerable<int> _assessmentCaseOverrides;
        private bool _loaded;
        private object _lockObject = new object();

        public Assessment(int assessmentId, IExceptionRepository exceptionRepository)
        {
            AssessmentId = assessmentId;
            _exceptionRepository = exceptionRepository ?? throw new ArgumentNullException(nameof(exceptionRepository));

            AssessmentViews = new AssessmentViews(assessmentId, exceptionRepository);
        }

        public int AssessmentId { get; private set; }

        public AssessmentViews AssessmentViews { get; private set; }

        public IEnumerable<int> GetAssessmentCaseOverrides()
        {
            if (!_loaded)
            {
                lock (_lockObject)
                {
                    if (!_loaded)
                    {
                        _assessmentCaseOverrides = _exceptionRepository.LoadAssessmentCaseOverrides(AssessmentId);
                        _loaded = true;
                    }
                }
            }

            return _assessmentCaseOverrides;
        }
    }
}