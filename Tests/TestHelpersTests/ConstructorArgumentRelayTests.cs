﻿using System;
using TestHelpers;
using Xunit;

namespace Tests.TestHelpersTests
{
    public class ConstructorArgumentRelayTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void Create_ThrowsArgumentNullExceptionOnNullContext(
            ConstructorArgumentRelay<TestClass<string>, string> sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.Create(null, null));
        }
    }

    public class TestClass<T>
    {
        public TestClass(T value1, T value2)
        {
            Value1 = value1;
            Value2 = value2;
        }

        public T Value1 { get; private set; }
        public T Value2 { get; private set; }
    }
}
