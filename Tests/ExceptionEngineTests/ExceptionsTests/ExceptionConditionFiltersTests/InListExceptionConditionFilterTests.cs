﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.Xunit2;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class InListExceptionConditionFilterTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfActionRepositoryIsNull()
        {
            var fixture = new Fixture().ConstructorArgumentFor<InListExceptionConditionFilter, IExceptionRepository>("exceptionRepository", null);

            var exception = Record.Exception(() => fixture.Create<InListExceptionConditionFilter>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(
            InListExceptionConditionFilter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.ExceptionConditionMatched(null, null));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValue_ForListOfIntegers(
            ExceptionCondition exceptionCondition, [Frozen]Mock<IExceptionRepository> exceptionRepositoryMock,
            InListExceptionConditionFilter sut)
        {
            var intCollection = new List<ExceptionConditionValue>()
            {
                new ExceptionConditionValue() { ConditionValue = 15 },
                new ExceptionConditionValue() { ConditionValue = 32 },
                new ExceptionConditionValue() { ConditionValue = 75 }
            };

            exceptionRepositoryMock.Setup(x => x.LoadExceptionConditionValues(It.IsAny<int>())).Returns(intCollection);
            var response = sut.ExceptionConditionMatched(15, exceptionCondition);

            Assert.True(response);

            response = sut.ExceptionConditionMatched(16, exceptionCondition);

            Assert.False(response);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValue_ForListOfStrings(
            ExceptionCondition exceptionCondition, [Frozen]Mock<IExceptionRepository> exceptionRepositoryMock,
            InListExceptionConditionFilter sut)
        {
            var intCollection = new List<ExceptionConditionValue>()
            {
                new ExceptionConditionValue() { ConditionValue = "a" },
                new ExceptionConditionValue() { ConditionValue = "b" },
                new ExceptionConditionValue() { ConditionValue = "c" }
            };

            exceptionRepositoryMock.Setup(x => x.LoadExceptionConditionValues(It.IsAny<int>())).Returns(intCollection);
            var response = sut.ExceptionConditionMatched("a", exceptionCondition);

            Assert.True(response);

            response = sut.ExceptionConditionMatched("d", exceptionCondition);

            Assert.False(response);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValue_ForNullPropertyToLookup(
            ExceptionCondition exceptionCondition, [Frozen]Mock<IExceptionRepository> exceptionRepositoryMock,
            InListExceptionConditionFilter sut)
        {
            var intCollection = new List<ExceptionConditionValue>()
            {
                new ExceptionConditionValue() { ConditionValue = "a" },
                new ExceptionConditionValue() { ConditionValue = "b" },
                new ExceptionConditionValue() { ConditionValue = "c" }
            };

            exceptionRepositoryMock.Setup(x => x.LoadExceptionConditionValues(It.IsAny<int>())).Returns(intCollection);
            var response = sut.ExceptionConditionMatched(null, exceptionCondition);

            Assert.False(response);
        }
    }
}
