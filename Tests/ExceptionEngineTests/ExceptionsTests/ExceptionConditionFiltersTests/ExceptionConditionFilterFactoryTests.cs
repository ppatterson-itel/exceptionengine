﻿using System;
using AutoFixture;
using ExceptionEngine;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using ExceptionEngine.Repositories;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class ExceptionConditionFilterFactoryTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfActionRepositoryIsNull()
        {
            var fixture = new Fixture().ConstructorArgumentFor<ExceptionConditionFilterFactory, IExceptionRepository>("exceptionRepository", null);

            var exception = Record.Exception(() => fixture.Create<ExceptionConditionFilterFactory>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(FilterOperator.ValueExists, typeof(ValueExistsExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.ValueIsAbsent, typeof(ValueIsAbsentExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.Equal, typeof(EqualExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.NotEqual, typeof(NotEqualExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.Less, typeof(LessThanExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.Greater, typeof(GreaterThanExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.Between, typeof(BetweenExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.NotBetween, typeof(NotBetweenExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.StartsWith, typeof(StartsWithExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.Contains, typeof(ContainsExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.DoesNotContain, typeof(DoesNotContainExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.FitRegularExpression, typeof(FitRegularExpressionExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.NotFitRegularExpression, typeof(NotFitRegularExpressionExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.InList, typeof(InListExceptionConditionFilter))]
        [InlineAutoMoqData(FilterOperator.NotInList, typeof(NotInListExceptionConditionFilter))]
        public void GetConditionFilter_ReturnsCorrectFilter(FilterOperator filterOperator, Type expectedType,
            ExceptionConditionFilterFactory sut)
        {
            var result = sut.GetConditionFilter(filterOperator);

            Assert.Equal(expectedType, result.GetType());
        }

        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(FilterOperator.Absent)]
        [InlineAutoMoqData(FilterOperator.Complex)]
        public void GetConditionFilter_ThrowsArgumentException_IfTypeIsNotValid(FilterOperator filterOperator,
            ExceptionConditionFilterFactory sut)
        {
            Assert.Throws<ArgumentException>(() => sut.GetConditionFilter(filterOperator));
        }
    }
}
