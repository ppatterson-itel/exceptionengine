﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using TestHelpers;
using Xunit;
// ReSharper disable StringLiteralTypo

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class NotFitRegularExpressionExceptionConditionFilterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void ExceptionConditionMatched_ThrowsArgumentNullException_IfExceptionConditionNull(
            NotFitRegularExpressionExceptionConditionFilter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.ExceptionConditionMatched(null, null));
        }

        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData("Metlife", "^METL|^SAFE|^TRAVR|^USAA|^ALLSC0007|^ENCC0001|^LIBM|^AMEF|^COCA|^GOLD|^INDI|^LIBN|^MONI|^PEER|^OHCA", true)]
        [InlineAutoMoqData("METLIFE", "^METL|^SAFE|^TRAVR|^USAA|^ALLSC0007|^ENCC0001|^LIBM|^AMEF|^COCA|^GOLD|^INDI|^LIBN|^MONI|^PEER|^OHCA", false)]
        public void ExceptionConditionMatched_ReturnsCorrectResponse(string propertyToCheck, string regex, bool expectedResult,
            ExceptionCondition ec, NotFitRegularExpressionExceptionConditionFilter sut)
        {
            ec.ValueFrom = regex;

            var result = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.Equal(expectedResult, result);
        }
    }
}
