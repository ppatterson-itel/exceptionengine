﻿using System;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class BetweenExceptionConditionFilterTests
    {
        [Theory, AutoMoqData]
        [Trait("Category","Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(
            BetweenExceptionConditionFilter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.ExceptionConditionMatched(null, null));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues_ForDateTimes(DateTime x, ExceptionCondition ec,
            BetweenExceptionConditionFilter sut)
        {
            var propertyToCheck = x;
            ec.ValueFrom = propertyToCheck.AddDays(-1);
            ec.ValueTo = propertyToCheck.AddDays(1);

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);

            propertyToCheck = propertyToCheck.AddYears(-1);

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues_ForIntegers(int x, ExceptionCondition ec,
            BetweenExceptionConditionFilter sut)
        {
            var propertyToCheck = x;
            ec.ValueFrom = propertyToCheck - 1;
            ec.ValueTo = propertyToCheck + 1;

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);

            propertyToCheck = propertyToCheck - 2;

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);
        }
    }
}
