﻿using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class ValueIsAbsentExceptionConditionFilterTests
    {
        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(null, true)]
        [InlineAutoMoqData("", true)]
        [InlineAutoMoqData("value", false)]
        [InlineAutoMoqData(5, false)]
        [InlineAutoMoqData(34.5, false)]
        public void ExceptionConditionMatched_ReturnsCorrectValuesForVariousInputs(object propertyToCheck,
            bool expectedResult, ExceptionCondition exceptionCondition, ValueIsAbsentExceptionConditionFilter sut)
        {
            var result = sut.ExceptionConditionMatched(propertyToCheck, exceptionCondition);

            Assert.Equal(expectedResult, result);
        }
    }
}
