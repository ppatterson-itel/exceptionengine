﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class StartsWithExceptionConditionFilterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(
            StartsWithExceptionConditionFilter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.ExceptionConditionMatched(null, null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues_ForDateTimes(string x, string y, ExceptionCondition ec,
            StartsWithExceptionConditionFilter sut)
        {
            var propertyToCheck = $"{x}-{y}";
            ec.ValueFrom = x;

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);

            propertyToCheck = $"{y}-{x}";

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValue_ForNullProperties(ExceptionCondition ec,
            StartsWithExceptionConditionFilter sut)
        {
            var response = sut.ExceptionConditionMatched(null, ec);

            Assert.False(response);
        }
    }
}
