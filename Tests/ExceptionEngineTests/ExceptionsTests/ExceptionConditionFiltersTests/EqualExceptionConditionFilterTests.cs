﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class EqualExceptionConditionFilterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(
            EqualExceptionConditionFilter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.ExceptionConditionMatched(null, null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues_ForStrings(string x, string y, ExceptionCondition ec,
            EqualExceptionConditionFilter sut)
        {
            var propertyToCheck = x;
            ec.ValueFrom = x;

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);

            propertyToCheck = y;

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues_ForDateTimes(DateTime x, DateTime y, ExceptionCondition ec,
            EqualExceptionConditionFilter sut)
        {
            var propertyToCheck = x;
            ec.ValueFrom = x;

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);

            propertyToCheck = y;

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues_ForIntegers(int x, int y, ExceptionCondition ec,
            EqualExceptionConditionFilter sut)
        {
            var propertyToCheck = x;
            ec.ValueFrom = x;

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);

            propertyToCheck = y;

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);
        }
    }
}
