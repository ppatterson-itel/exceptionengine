﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class LessThanExceptionConditionFilterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(
            LessThanExceptionConditionFilter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.ExceptionConditionMatched(null, null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues_ForDateTimes(DateTime x, ExceptionCondition ec,
            LessThanExceptionConditionFilter sut)
        {
            var propertyToCheck = x.AddDays(1);
            ec.ValueFrom = x;

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);

            propertyToCheck = x.AddDays(-1);

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues_ForIntegers(int x, ExceptionCondition ec,
            LessThanExceptionConditionFilter sut)
        {
            var propertyToCheck = x + 1;
            ec.ValueFrom = x;

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);

            propertyToCheck = x - 1;

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);
        }
    }
}
