﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class DoesNotContainExceptionConditionFilterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(
            DoesNotContainExceptionConditionFilter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.ExceptionConditionMatched(null, null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsCorrectValues(string x, string y, ExceptionCondition ec,
            DoesNotContainExceptionConditionFilter sut)
        {
            var propertyToCheck = $"{x}-{y}";
            ec.ValueFrom = x;

            var response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.False(response);

            propertyToCheck = y;

            response = sut.ExceptionConditionMatched(propertyToCheck, ec);

            Assert.True(response);
        }
    }
}
