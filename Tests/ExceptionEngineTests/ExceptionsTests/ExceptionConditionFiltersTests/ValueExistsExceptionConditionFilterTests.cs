﻿using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Exceptions.ExceptionConditionFilters;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ExceptionsTests.ExceptionConditionFiltersTests
{
    public class ValueExistsExceptionConditionFilterTests
    {
        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(null, false)]
        [InlineAutoMoqData("", false)]
        [InlineAutoMoqData("value", true)]
        [InlineAutoMoqData(5, true)]
        [InlineAutoMoqData(34.5, true)]
        public void ExceptionConditionMatched_ReturnsCorrectValuesForVariousInputs(object propertyToCheck,
            bool expectedResult, ExceptionCondition exceptionCondition, ValueExistsExceptionConditionFilter sut)
        {
            var result = sut.ExceptionConditionMatched(propertyToCheck, exceptionCondition);

            Assert.Equal(expectedResult, result);
        }
    }
}
