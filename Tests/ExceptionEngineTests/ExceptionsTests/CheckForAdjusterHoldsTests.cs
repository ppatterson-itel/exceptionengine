﻿using System;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using ExceptionEngine.Exceptions;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;
using Exception = ExceptionEngine.Exceptions.DataTransferObjects.Exception;

namespace Tests.ExceptionEngineTests.ExceptionsTests
{
    public class CheckForAdjusterHoldsTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void EvaluateExceptionCase_ReturnsResponseFromRepository(ExceptionCaseResponse caseResponse,
            [Frozen] Mock<IExceptionRepository> exceptionRepositoryMock,
            CheckForAdjusterHolds sut)
        {
            exceptionRepositoryMock.Setup(r =>
                    r.CheckForAdjustHoldOnAssessment(It.IsAny<int>(), It.IsAny<ExceptionCaseDto>(),
                        It.IsAny<Exception>()))
                .Returns(caseResponse);

            var response = sut.EvaluateExceptionCase();

            Assert.Equal(caseResponse, response);
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfExceptionRepositoryIsNull()
        {
            var fixture = new Fixture().ConstructorArgumentFor<CheckForAdjusterHolds, IExceptionRepository>("exceptionRepository", null);

            var exception = Record.Exception(() => fixture.Create<CheckForAdjusterHolds>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfExceptionCaseIsNull()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization())
                .ConstructorArgumentFor<CheckForAdjusterHolds, ExceptionCaseDto>("exceptionCase", null);

            var exception = Record.Exception(() => fixture.Create<CheckForAdjusterHolds>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfExceptionIsNull()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization())
                .ConstructorArgumentFor<CheckForAdjusterHolds, Exception>("exception", null);

            var exception = Record.Exception(() => fixture.Create<CheckForAdjusterHolds>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }
    }
}
