﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using ExceptionEngine;
using ExceptionEngine.Exceptions;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;
using Exception = ExceptionEngine.Exceptions.DataTransferObjects.Exception;

namespace Tests.ExceptionEngineTests.ExceptionsTests
{
    public class AssessmentPropertiesExceptionCaseTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfExceptionRepositoryIsNull()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization())
                .ConstructorArgumentFor<AssessmentPropertiesExceptionCase, IExceptionRepository>("exceptionRepository", null);

            var exception = Record.Exception(() => fixture.Create<AssessmentPropertiesExceptionCase>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfExceptionCaseIsNull()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization())
                .ConstructorArgumentFor<AssessmentPropertiesExceptionCase, ExceptionCaseDto>("exceptionCase", null);

            var exception = Record.Exception(() => fixture.Create<AssessmentPropertiesExceptionCase>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfExceptionIsNull()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization())
                .ConstructorArgumentFor<AssessmentPropertiesExceptionCase, Exception>("exception", null);

            var exception = Record.Exception(() => fixture.Create<AssessmentPropertiesExceptionCase>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void EvaluateExceptionCase_ReturnsFalseWhenAPropertyDoesNotMatch(int productTypeId, string productType, 
            Dictionary<string, dynamic> propertyLookups, [Frozen]Mock<IExceptionRepository> exceptionRepositoryMock,
            AssessmentViewProductData productData, AssessmentPropertiesExceptionCase sut)
        {
            var fixture = new Fixture();
            sut.ExceptionConditions.AddMany(() =>
                fixture.Build<ExceptionCondition>()
                    .With(c => c.Operator, (int)FilterOperator.ValueIsAbsent)
                    .Create(), 3);

            foreach (var conditionToCheck in sut.ExceptionConditions)
            {
                propertyLookups.Add(conditionToCheck.AttributeColumn, "value");
            }

            propertyLookups.Add("ProductTypeID", productTypeId);

            productData.ExceptionViewProductType = productType;
            productData.AssessmentProductType = productType;
            exceptionRepositoryMock.Setup(r => r.LoadAssessmentAndViewProductData(It.IsAny<int>(), It.IsAny<int?>()))
                .Returns(productData);
            exceptionRepositoryMock.Setup(r => r.LoadAssessmentViewData(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(propertyLookups);
            var response = sut.EvaluateExceptionCase();

            Assert.False(response.Result);
            Assert.StartsWith("Property did not match for exception case", response.Condition);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void EvaluateExceptionCase_ReturnsTrueWhenAllPropertiesMatch(Dictionary<string, dynamic> propertyLookups,
            string product, [Frozen]Mock<IExceptionRepository> exceptionRepositoryMock,
            AssessmentViewProductData data, AssessmentPropertiesExceptionCase sut)
        {
            var fixture = new Fixture();
            sut.ExceptionConditions.AddMany(() =>
                fixture.Build<ExceptionCondition>()
                    .With(c => c.Operator, (int)FilterOperator.ValueExists)
                    .Create(), 3);

            foreach (var conditionToCheck in sut.ExceptionConditions)
            {
                propertyLookups.Add(conditionToCheck.AttributeColumn, "value");
            }

            data.AssessmentProductType = product;
            data.ExceptionViewProductType = product;

            exceptionRepositoryMock.Setup(r => r.LoadAssessmentViewData(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(propertyLookups);
            exceptionRepositoryMock.Setup(r => r.LoadAssessmentAndViewProductData(It.IsAny<int>(), It.IsAny<int?>()))
                .Returns(data);
            var response = sut.EvaluateExceptionCase();

            Assert.True(response.Result);
        }


        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentException_IfExceptionConditionsEmpty()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization())
                .ConstructorArgumentFor<AssessmentPropertiesExceptionCase, IEnumerable<ExceptionCondition>>("exceptionConditions", new List<ExceptionCondition>());

            var sut = fixture.Create<AssessmentPropertiesExceptionCase>();

            var response = sut.EvaluateExceptionCase();

            Assert.Equal("There are no exception conditions to evaluate against.", response.Condition);
            Assert.False(response.Result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void EvaluateExceptionCase_ReturnsFalse_IfProductTypesDoNotMatch(Dictionary<string, dynamic> propertyLookups,
            string product, string product2, AssessmentViewProductData data, [Frozen]Mock<IExceptionRepository> exceptionRepositoryMock,
            AssessmentPropertiesExceptionCase sut)
        {
            var fixture = new Fixture();
            sut.ExceptionConditions.AddMany(() =>
                fixture.Build<ExceptionCondition>()
                    .With(c => c.Operator, (int)FilterOperator.ValueExists)
                    .Create(), 3);

            foreach (var conditionToCheck in sut.ExceptionConditions)
            {
                propertyLookups.Add(conditionToCheck.AttributeColumn, "value");
            }

            data.AssessmentProductType = product;
            data.ExceptionViewProductType = product2;

            exceptionRepositoryMock.Setup(r => r.LoadAssessmentViewData(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(propertyLookups);
            exceptionRepositoryMock.Setup(r => r.LoadAssessmentAndViewProductData(It.IsAny<int>(), It.IsAny<int?>()))
                .Returns(data);
            var response = sut.EvaluateExceptionCase();

            Assert.False(response.Result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void EvaluateExceptionCase_ReturnsTrue_IfAssessmentProductTypeIsNull(Dictionary<string, dynamic> propertyLookups,
            AssessmentViewProductData data, [Frozen]Mock<IExceptionRepository> exceptionRepositoryMock,
            AssessmentPropertiesExceptionCase sut)
        {
            var fixture = new Fixture();
            sut.ExceptionConditions.AddMany(() =>
                fixture.Build<ExceptionCondition>()
                    .With(c => c.Operator, (int)FilterOperator.ValueExists)
                    .Create(), 3);

            foreach (var conditionToCheck in sut.ExceptionConditions)
            {
                propertyLookups.Add(conditionToCheck.AttributeColumn, "value");
            }

            data.AssessmentProductType = null;

            exceptionRepositoryMock.Setup(r => r.LoadAssessmentViewData(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(propertyLookups);
            exceptionRepositoryMock.Setup(r => r.LoadAssessmentAndViewProductData(It.IsAny<int>(), It.IsAny<int?>()))
                .Returns(data);
            var response = sut.EvaluateExceptionCase();

            Assert.True(response.Result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void EvaluateExceptionCase_ReturnsTrue_IfExceptionViewProductTypeIsNullOrEmpty(Dictionary<string, dynamic> propertyLookups,
            AssessmentViewProductData data, [Frozen]Mock<IExceptionRepository> exceptionRepositoryMock,
            AssessmentPropertiesExceptionCase sut)
        {
            var fixture = new Fixture();
            sut.ExceptionConditions.AddMany(() =>
                fixture.Build<ExceptionCondition>()
                    .With(c => c.Operator, (int)FilterOperator.ValueExists)
                    .Create(), 3);

            foreach (var conditionToCheck in sut.ExceptionConditions)
            {
                propertyLookups.Add(conditionToCheck.AttributeColumn, "value");
            }

            data.ExceptionViewProductType = null;

            exceptionRepositoryMock.Setup(r => r.LoadAssessmentViewData(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(propertyLookups);
            exceptionRepositoryMock.Setup(r => r.LoadAssessmentAndViewProductData(It.IsAny<int>(), It.IsAny<int?>()))
                .Returns(data);
            var response = sut.EvaluateExceptionCase();

            Assert.True(response.Result);

            data.ExceptionViewProductType = string.Empty;

            response = sut.EvaluateExceptionCase();
            Assert.True(response.Result);
        }
    }
}
