﻿using AutoFixture;
using AutoFixture.Xunit2;
using ExceptionEngine.Exceptions;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using TestHelpers;
using Xunit;
using Exception = ExceptionEngine.Exceptions.DataTransferObjects.Exception;

namespace Tests.ExceptionEngineTests.ExceptionsTests
{
    public class ExceptionCaseFactoryTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfExceptionRepositoryIsNull()
        {
            var fixture = new Fixture().ConstructorArgumentFor<ExceptionCaseFactory, IExceptionRepository>("exceptionRepository", null);

            var exception = Record.Exception(() => fixture.Create<ExceptionCaseFactory>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void GetExceptionCases_ThrowsArgumentNullException(Assessment assessment, ExceptionCaseFactory sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.GetExceptionCases(null, assessment));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void GetExceptionCases_CanReturnStoredProcedureExceptionCase_NotOverridable(Exception exception,
            ExceptionCaseDto exceptionCase, Assessment assessment, ExceptionCaseFactory sut)
        {
            exception.ConditionStoredProcedure = "testProcedure";
            exceptionCase.AllowOverride = false;
            exception.ExceptionCases.Add(exceptionCase);

            var response = sut.GetExceptionCases(exception, assessment);

            var createdCase = Assert.Single(response);

            Assert.IsType<StoredProcedureExceptionCase>(createdCase);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void GetExceptionCases_CanReturnStoredProcedureExceptionCase_NotOverridden(Assessment assessment, Exception exception,
            ExceptionCaseDto exceptionCase, [Frozen]Mock<IExceptionRepository> exceptionRepository,
            ExceptionCaseFactory sut)
        {
            exception.ConditionStoredProcedure = "testProcedure";
            exceptionCase.AllowOverride = true;
            exception.ExceptionCases.Add(exceptionCase);
            exceptionRepository.Setup(r => r.AssessmentCaseOverridden(assessment.AssessmentId, It.IsAny<int>())).Returns(false);

            var response = sut.GetExceptionCases(exception, assessment);

            var createdCase = Assert.Single(response);

            Assert.IsType<StoredProcedureExceptionCase>(createdCase);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void GetExceptionCases_ReturnsEmptyExceptionCaseList_OverriddenStoredProcedure(
            Exception exception, ExceptionCaseDto exceptionCase, [Frozen]Mock<IExceptionRepository> exceptionRepository,
            Assessment assessment, ExceptionCaseFactory sut)
        {
            List<int> overriddenCases = new List<int>
            {
                exceptionCase.ExceptionCaseId
            };
            exception.ConditionStoredProcedure = "testProcedure";
            exceptionCase.AllowOverride = true;
            exception.ExceptionCases.Add(exceptionCase);
            exceptionRepository.Setup(r => r.LoadAssessmentCaseOverrides(assessment.AssessmentId)).Returns(overriddenCases);

            var response = sut.GetExceptionCases(exception, assessment);

            Assert.Empty(response);
        }

        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData("CheckForAdjusterHolds", typeof(CheckForAdjusterHolds))]
        [InlineAutoMoqData("CheckForCustomerARHolds", typeof(CheckForCustomerArHolds))]
        public void GetExceptionCases_CanReturnStoredMethodExceptionCases_NotOverridable(string storedMethod, Type expectedType, 
            Assessment assessment, Exception exception, ExceptionCaseDto exceptionCase,
            ExceptionCaseFactory sut)
        {
            exception.ConditionStoredProcedure = null;
            exception.ConditionStoredMethod = storedMethod;
            exceptionCase.AllowOverride = false;
            exception.ExceptionCases.Add(exceptionCase);

            var response = sut.GetExceptionCases(exception, assessment);

            var createdCase = Assert.Single(response);

            Assert.IsType(expectedType, createdCase);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void GetExceptionCases_CanReturnStoredMethodExceptionCase_NotOverridden(Assessment assessment, Exception exception,
            ExceptionCaseDto exceptionCase, [Frozen]Mock<IExceptionRepository> exceptionRepository,
            ExceptionCaseFactory sut)
        {
            exception.ConditionStoredProcedure = null;
            exception.ConditionStoredMethod = "CheckForAdjusterHolds";
            exceptionCase.AllowOverride = true;
            exception.ExceptionCases.Add(exceptionCase);
            exceptionRepository.Setup(r => r.AssessmentCaseOverridden(assessment.AssessmentId, It.IsAny<int>())).Returns(false);

            var response = sut.GetExceptionCases(exception, assessment);

            var createdCase = Assert.Single(response);

            Assert.IsType<CheckForAdjusterHolds>(createdCase);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void GetExceptionCases_ReturnsEmptyExceptionCaseList_OverriddenStoredMethod(Exception exception,
            ExceptionCaseDto exceptionCase, [Frozen]Mock<IExceptionRepository> exceptionRepository, Assessment assessment,
            ExceptionCaseFactory sut)
        {
            List<int> overriddenCases = new List<int>
            {
                exceptionCase.ExceptionCaseId
            };
            exception.ConditionStoredProcedure = null;
            exception.ConditionStoredMethod = "CheckForAdjusterHolds";
            exceptionCase.AllowOverride = true;
            exception.ExceptionCases.Add(exceptionCase);
            exceptionRepository.Setup(r => r.LoadAssessmentCaseOverrides(assessment.AssessmentId)).Returns(overriddenCases);

            var response = sut.GetExceptionCases(exception, assessment);

            Assert.Empty(response);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void GetExceptionCases_ReturnsEmptyExceptionCaseList_IfNoMatchOnStoredMethod(Assessment assessment, Exception exception,
            ExceptionCaseDto exceptionCase, [Frozen]Mock<IExceptionRepository> exceptionRepository,
            ExceptionCaseFactory sut)
        {
            exception.ConditionStoredProcedure = null;
            exception.ConditionStoredMethod = "does not exist";
            exceptionCase.AllowOverride = true;
            exception.ExceptionCases.Add(exceptionCase);
            exceptionRepository.Setup(r => r.AssessmentCaseOverridden(assessment.AssessmentId, It.IsAny<int>())).Returns(false);

            var response = sut.GetExceptionCases(exception, assessment);

            Assert.Empty(response);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void GetExceptionCases_ReturnsAssessmentPropertiesExceptionCases_ForEachExceptionCase(Assessment assessment,
            Exception exception, List<ExceptionCaseDto> exceptionCases, IEnumerable<ExceptionConditionValue> exceptionConditionValues, 
            [Frozen]Mock<IExceptionRepository> exceptionRepository, ExceptionCaseFactory sut)
        {
            exception.ConditionStoredProcedure = null;
            exception.ConditionStoredMethod = null;
            exceptionCases.ForEach(dto =>
            {
                dto.AllowOverride = false;
                exception.ExceptionCases.Add(dto);
            });

            exceptionRepository.Setup(r => r.LoadExceptionConditionValues(It.IsAny<int>()))
                .Returns(exceptionConditionValues);

            var response = sut.GetExceptionCases(exception, assessment).ToList();

            Assert.All(response, exceptionCase => Assert.IsType<AssessmentPropertiesExceptionCase>(exceptionCase));
            Assert.Equal(3, response.Count());
        }
    }
}
