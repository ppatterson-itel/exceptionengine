﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using ExceptionEngine;
using ExceptionEngine.Actions;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class ExceptionActionFactoryTests
    {
        private readonly IFixture _fixture;

        public ExceptionActionFactoryTests()
        {
            _fixture = new Fixture().Customize(new AutoMoqCustomization());
        }

        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(ExceptionActionType.AlertMessage, typeof(AlertMessageAction))]
        [InlineAutoMoqData(ExceptionActionType.CustomAction, typeof(CustomAction))]
        [InlineAutoMoqData(ExceptionActionType.EmailNotification, typeof(EmailNotificationAction))]
        [InlineAutoMoqData(ExceptionActionType.Hold, typeof(HoldAction))]
        [InlineAutoMoqData(ExceptionActionType.InternalNote, typeof(InternalNoteAction))]
        [InlineAutoMoqData(ExceptionActionType.ReportComment, typeof(ReportCommentAction))]
        [InlineAutoMoqData(ExceptionActionType.SuppressReportSection, typeof(SuppressReportSectionAction))]
        public void GetExceptionActions_ReturnsTheSevenValidActionTypes(ExceptionActionType exceptionActionType, 
            Type expectedType, ExceptionActionRequest request, [Frozen] Mock<IActionRepository> actionRepositoryMock, 
            int exceptionId, SystemSettings systemSettings, ExceptionActionFactory sut)
        {
            request.ExceptionActionType = exceptionActionType;
            var requests = new List<ExceptionActionRequest> { request };

            actionRepositoryMock.Setup(r => r.LoadExceptionActions(exceptionId)).Returns(requests);

            var response = sut.GetExceptionActions(exceptionId, _fixture.Create<int>(), _fixture.Create<int>(), _fixture.Create<string>(),
                _fixture.Create<string>(), _fixture.Create<int>(), systemSettings);

            Assert.Equal(response.First().GetType(), expectedType);
        }

        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(ExceptionActionType.None)]
        [InlineAutoMoqData(-1)]
        public void GetExceptionActions_ThrowsArgumentException_IfTypeNotSupported(int exceptionActionType,
            ExceptionActionRequest request, SystemSettings systemSettings, [Frozen] Mock<IActionRepository> actionRepositoryMock, 
            ExceptionActionFactory sut)
        {
            request.ExceptionActionType = (ExceptionActionType)exceptionActionType;
            var requests = new List<ExceptionActionRequest> { request };

            actionRepositoryMock.Setup(r => r.LoadExceptionActions(It.IsAny<int>())).Returns(requests);

            Assert.Throws<InvalidOperationException>(() => sut.GetExceptionActions(_fixture.Create<int>(),
                _fixture.Create<int>(), _fixture.Create<int>(), _fixture.Create<string>(),
                _fixture.Create<string>(), _fixture.Create<int>(), systemSettings));
        }
    }
}
