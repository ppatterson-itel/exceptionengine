﻿using System.Collections.Generic;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class AlertMessageActionTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CallsCreateNotification_IfNotificationIsNotADuplicate(ExceptionNotification exceptionNotification, 
            [Frozen] Mock<IActionRepository> actionRepositoryMock, AlertMessageAction sut)
        {
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(sut.ExceptionActionRequest.ExceptionMappingId)).Returns(exceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description, exceptionNotification.UserId, 
                exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Once());

            Assert.Equal("Send Alert Messages; ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CallsCreateNotification_IfNotificationIsNotADuplicate_UserIDsNoMatch(ExceptionNotification exceptionNotification,
            AssessmentExceptionNotification assessmentExceptionNotification, [Frozen] Mock<IActionRepository> actionRepositoryMock, AlertMessageAction sut)
        {
            exceptionNotification.EmailAddress = assessmentExceptionNotification.Email;
            assessmentExceptionNotification.ExceptionCaseId = sut.ExceptionCaseId;
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            var assessmentExceptionNotifications = new List<AssessmentExceptionNotification> { assessmentExceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(It.IsAny<int>())).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadAssessmentExceptionNotifications(It.IsAny<int>())).Returns(assessmentExceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description, exceptionNotification.UserId,
                exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Once());

            Assert.Equal("Send Alert Messages; ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CallsCreateNotification_IfNotificationIsNotADuplicate_EmailsNoMatch(ExceptionNotification exceptionNotification,
            AssessmentExceptionNotification assessmentExceptionNotification, [Frozen] Mock<IActionRepository> actionRepositoryMock, AlertMessageAction sut)
        {
            exceptionNotification.UserId = assessmentExceptionNotification.UserId;
            assessmentExceptionNotification.ExceptionCaseId = sut.ExceptionCaseId;
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            var assessmentExceptionNotifications = new List<AssessmentExceptionNotification> { assessmentExceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(It.IsAny<int>())).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadAssessmentExceptionNotifications(It.IsAny<int>())).Returns(assessmentExceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description, exceptionNotification.UserId,
                exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Once());

            Assert.Equal("Send Alert Messages; ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CallsCreateNotification_IfNotificationIsNotADuplicate_ExceptionCaseIDNoMatch(ExceptionNotification exceptionNotification,
            AssessmentExceptionNotification assessmentExceptionNotification, [Frozen] Mock<IActionRepository> actionRepositoryMock, AlertMessageAction sut)
        {
            exceptionNotification.EmailAddress = assessmentExceptionNotification.Email;
            exceptionNotification.UserId = assessmentExceptionNotification.UserId;
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            var assessmentExceptionNotifications = new List<AssessmentExceptionNotification> { assessmentExceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(It.IsAny<int>())).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadAssessmentExceptionNotifications(It.IsAny<int>())).Returns(assessmentExceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description, exceptionNotification.UserId,
                exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Once());

            Assert.Equal("Send Alert Messages; ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCallCreateNotification_IfNotificationIsADuplicate(ExceptionNotification exceptionNotification,
            AssessmentExceptionNotification assessmentExceptionNotification, [Frozen] Mock<IActionRepository> actionRepositoryMock, AlertMessageAction sut)
        {
            exceptionNotification.EmailAddress = assessmentExceptionNotification.Email;
            exceptionNotification.UserId = assessmentExceptionNotification.UserId;
            assessmentExceptionNotification.ExceptionCaseId = sut.ExceptionCaseId;
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            var assessmentExceptionNotifications = new List<AssessmentExceptionNotification> { assessmentExceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(It.IsAny<int>())).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadAssessmentExceptionNotifications(It.IsAny<int>())).Returns(assessmentExceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description, exceptionNotification.UserId,
                exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Never());

            Assert.Equal("Send Alert Messages; ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CallsCreateNotification_IfDuplicatesAreAllowed(ExceptionNotification exceptionNotification,
            AssessmentExceptionNotification assessmentExceptionNotification, [Frozen] Mock<IActionRepository> actionRepositoryMock, AlertMessageAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = true;
            exceptionNotification.EmailAddress = assessmentExceptionNotification.Email;
            exceptionNotification.UserId = assessmentExceptionNotification.UserId;
            assessmentExceptionNotification.ExceptionCaseId = sut.ExceptionCaseId;
            exceptionNotification.ProcessingInspector = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            var assessmentExceptionNotifications = new List<AssessmentExceptionNotification> { assessmentExceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(It.IsAny<int>())).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadAssessmentExceptionNotifications(It.IsAny<int>())).Returns(assessmentExceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description, exceptionNotification.UserId,
                exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Once());

            Assert.Equal("Send Alert Messages; ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCallCreateNotification_IfProcessingInspectorIsTrue(ExceptionNotification exceptionNotification,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, AlertMessageAction sut)
        {
            exceptionNotification.ProcessingInspector = true;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(sut.ExceptionActionRequest.ExceptionMappingId)).Returns(exceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description, exceptionNotification.UserId,
                exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Never());

            Assert.Equal("Send Alert Messages; ", response.ActionsTaken);
        }
    }
}
