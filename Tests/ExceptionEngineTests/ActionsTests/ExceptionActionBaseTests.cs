﻿using ExceptionEngine.Actions;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class ExceptionActionBaseTests
    {
        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(10, "25", false, 10)]
        [InlineAutoMoqData(10, null, true, 10)]
        [InlineAutoMoqData(10, "25", true, 25)]
        [InlineAutoMoqData(10, "invalid", true, 10)]
        public void GetUserId_ReturnsCorrectValueForVariousInputs(int userId, string systemUserId,
            bool isSystemUser, int expectedResult, Mock<ExceptionActionBase> actionBaseMock)
        {
            var sut = actionBaseMock.Object;
            sut.ExceptionActionRequest.IsSystemUser = isSystemUser;

            var response = sut.GetUserId(userId, systemUserId);

            Assert.Equal(expectedResult, response);
        }
    }
}
