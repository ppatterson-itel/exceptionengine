﻿using System.Collections.Generic;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class EmailNotificationActionTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CallsCreateNotification_IfNotificationIsNotADuplicate(ExceptionNotification exceptionNotification,
            [Frozen]Mock<IActionRepository> actionRepositoryMock, [Frozen]Mock<ISendEmails> emailSenderMock,
            string displayControlNumber, EmailNotificationAction sut)
        {
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(It.IsAny<int>())).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadDisplayControlNumber(It.IsAny<int>())).Returns(displayControlNumber);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description, 
                exceptionNotification.UserId, exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Once());
            emailSenderMock.Verify(mock => mock.QueueNotificationEmail(exceptionNotification.EmailAddress, sut.Description, 
                $"Exception, Assessment Control # {displayControlNumber}"));

            Assert.Equal("Send Email Notification: ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_QueuesAdditionalEmail_IfRequestValueIsPlusCurrentUser(ExceptionNotification exceptionNotification,
            [Frozen]Mock<IActionRepository> actionRepositoryMock, [Frozen]Mock<ISendEmails> emailSenderMock, 
            string displayControlNumber, EmailNotificationAction sut)
        {
            sut.ExceptionActionRequest.Value = "plus current user";
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(It.IsAny<int>())).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadDisplayControlNumber(It.IsAny<int>())).Returns(displayControlNumber);

            var response = sut.PerformExceptionAction(false, null);

            emailSenderMock.Verify(mock => mock.QueueNotificationEmail(sut.UserEmail, sut.Description,
                $"Exception, Assessment Control # {displayControlNumber}"));

            Assert.Equal("Send Email Notification: ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCallCreateNotification_IfNotificationIsADuplicate(ExceptionNotification exceptionNotification,
            AssessmentExceptionNotification assessmentExceptionNotification, [Frozen] Mock<IActionRepository> actionRepositoryMock,
            [Frozen]Mock<ISendEmails> emailSenderMock, EmailNotificationAction sut)
        {
            exceptionNotification.EmailAddress = assessmentExceptionNotification.Email;
            exceptionNotification.UserId = assessmentExceptionNotification.UserId;
            assessmentExceptionNotification.ExceptionCaseId = sut.ExceptionCaseId;
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            var assessmentExceptionNotifications = new List<AssessmentExceptionNotification> { assessmentExceptionNotification };

            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(sut.ExceptionActionRequest.ExceptionMappingId)).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadAssessmentExceptionNotifications(sut.AssessmentId)).Returns(assessmentExceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int?>(),
                It.IsAny<string>(), It.IsAny<int>(), null), Times.Never());
            emailSenderMock.Verify(mock => mock.QueueNotificationEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never());

            Assert.Equal("Send Email Notification: ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCallCreateNotification_IfProcessingInspectorIsTrue(ExceptionNotification exceptionNotification,
            [Frozen]Mock<IActionRepository> actionRepositoryMock, [Frozen]Mock<ISendEmails> emailSenderMock, EmailNotificationAction sut)
        {
            exceptionNotification.ProcessingInspector = true;
            sut.ExceptionActionRequest.AllowDuplicates = false;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(It.IsAny<int>())).Returns(exceptionNotifications);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<int?>(),
                It.IsAny<string>(), It.IsAny<int>(), null), Times.Never());
            emailSenderMock.Verify(mock => mock.QueueNotificationEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never());

            Assert.Equal("Send Email Notification: ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesNotification_IfDuplicatesAreAllowed(ExceptionNotification exceptionNotification,
            AssessmentExceptionNotification assessmentExceptionNotification, [Frozen] Mock<IActionRepository> actionRepositoryMock,
            [Frozen]Mock<ISendEmails> emailSenderMock, string displayControlNumber, EmailNotificationAction sut)
        {
            exceptionNotification.EmailAddress = assessmentExceptionNotification.Email;
            exceptionNotification.UserId = assessmentExceptionNotification.UserId;
            assessmentExceptionNotification.ExceptionCaseId = sut.ExceptionCaseId;
            exceptionNotification.ProcessingInspector = false;
            sut.ExceptionActionRequest.AllowDuplicates = true;
            var exceptionNotifications = new List<ExceptionNotification> { exceptionNotification };
            var assessmentExceptionNotifications = new List<AssessmentExceptionNotification> { assessmentExceptionNotification };

            actionRepositoryMock.Setup(repository => repository.LoadExceptionActionNotifications(sut.ExceptionActionRequest.ExceptionMappingId)).Returns(exceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadAssessmentExceptionNotifications(sut.AssessmentId)).Returns(assessmentExceptionNotifications);
            actionRepositoryMock.Setup(repository => repository.LoadDisplayControlNumber(It.IsAny<int>())).Returns(displayControlNumber);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.CreateAlertNotification(sut.AssessmentId, sut.ExceptionCaseId, sut.Description,
                exceptionNotification.UserId, exceptionNotification.EmailAddress, exceptionNotification.ExceptionMappingId, null), Times.Once());
            emailSenderMock.Verify(mock => mock.QueueNotificationEmail(exceptionNotification.EmailAddress, sut.Description,
                $"Exception, Assessment Control # {displayControlNumber}"));

            Assert.Equal("Send Email Notification: ", response.ActionsTaken);
        }
    }
}
