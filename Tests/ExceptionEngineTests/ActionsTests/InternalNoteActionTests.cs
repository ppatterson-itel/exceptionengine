﻿using System.Collections.Generic;
using AutoFixture;
using AutoFixture.Xunit2;
using ExceptionEngine;
using ExceptionEngine.Actions;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class InternalNoteActionTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CallsCreateAssessmentNote_IfIncludeNoteWithException(ExceptionCaseData exceptionCaseData,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, InternalNoteAction sut)
        {
            sut.ExceptionActionRequest.IncludeNoteWithException = true;
            var fixture = new Fixture();
            exceptionCaseData.Conditions.AddMany(() => fixture.Build<ExceptionCaseCondition>()
            .With(c => c.Operator, FilterOperator.ValueExists).Create(), 3);
            AssessmentNoteRequest assessmentNoteRequest = null;

            actionRepositoryMock.Setup(r => r.LoadExceptionCaseData(It.IsAny<int>())).Returns(exceptionCaseData);
            actionRepositoryMock.Setup(m => m.CreateAssessmentNote(It.IsAny<AssessmentNoteRequest>()))
                .Callback<AssessmentNoteRequest>((ahr) => assessmentNoteRequest = ahr);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentNote(It.IsAny<AssessmentNoteRequest>()), Times.Once());
            Assert.Equal(sut.AssessmentId, assessmentNoteRequest.AssessmentId);
            Assert.Equal("VALNOTE", assessmentNoteRequest.NoteType);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentNote_IfNotADuplicate(IEnumerable<InternalNote> internalNotes,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, InternalNoteAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = false;
            sut.ExceptionActionRequest.IncludeNoteWithException = false;

            actionRepositoryMock.Setup(m => m.LoadExceptionActionInternalNotes(It.IsAny<int>())).
                Returns(internalNotes);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentNote(It.IsAny<AssessmentNoteRequest>()), Times.Exactly(3));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCreateAssessmentNote_IfNotesAreDuplicate(List<InternalNote> internalNotes,
            List<AssessmentNote> assessmentNotes, [Frozen] Mock<IActionRepository> actionRepositoryMock, InternalNoteAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = false;
            sut.ExceptionActionRequest.IncludeNoteWithException = false;

            internalNotes.ForEach(note => { note.Note = "This is a note."; note.NoteType = "INTERNAL"; });
            assessmentNotes.ForEach(note => { note.Note = "This is a note."; note.NoteType = "INTERNAL"; });

            actionRepositoryMock.Setup(m => m.LoadExceptionActionInternalNotes(It.IsAny<int>())).Returns(internalNotes);
            actionRepositoryMock.Setup(m => m.LoadAssessmentNotes(It.IsAny<int>())).Returns(assessmentNotes);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentNote(It.IsAny<AssessmentNoteRequest>()), Times.Never());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentNotes_IfDuplicatesAreAllowed(List<InternalNote> internalNotes,
            List<AssessmentNote> assessmentNotes, [Frozen] Mock<IActionRepository> actionRepositoryMock, InternalNoteAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = true;
            sut.ExceptionActionRequest.IncludeNoteWithException = false;

            internalNotes.ForEach(note => { note.Note = "This is a note."; note.NoteType = "INTERNAL"; });
            assessmentNotes.ForEach(note => { note.Note = "This is a note."; note.NoteType = "INTERNAL"; });

            actionRepositoryMock.Setup(m => m.LoadExceptionActionInternalNotes(It.IsAny<int>())).Returns(internalNotes);
            actionRepositoryMock.Setup(m => m.LoadAssessmentNotes(It.IsAny<int>())).Returns(assessmentNotes);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentNote(It.IsAny<AssessmentNoteRequest>()), Times.Exactly(3));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_SkipsAction_IfExceptionAlreadyProcessed(InternalNoteAction sut)
        {
            sut.ExceptionActionRequest.IncludeNoteWithException = false;

            var response = sut.PerformExceptionAction(true, null);

            Assert.True(response.ActionSkipped);
        }
    }
}
