﻿using ExceptionEngine.Actions;
using Xunit;
// ReSharper disable ExpressionIsAlwaysNull

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class EqualityEvaluatorTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsTrueForNullObjectsOfSameType()
        {
            int? first = null, second = null;

            Assert.True(EqualityEvaluator.AreEqual(first, second));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsTrueForEqualObjectsOfSameType()
        {
            int? first = -25, second = -25;

            Assert.True(EqualityEvaluator.AreEqual(first, second));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsFalseIfOneOfObjectsAreNullOfSameType()
        {
            int? first = -25, second = null;

            Assert.False(EqualityEvaluator.AreEqual(first, second));

            first = null;
            second = -25;

            Assert.False(EqualityEvaluator.AreEqual(first, second));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsFalseIfObjectsAreOfDifferentTypes()
        {
            int? first = null;
            string second = null;

            Assert.False(EqualityEvaluator.AreEqual(first, second));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsTrueIfObjectsAreSameDynamic()
        {
            dynamic first = -25, second = -25;

            Assert.True(EqualityEvaluator.AreEqual(first, second));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsFalseIfObjectsAreOfDifferentTypesDynamic()
        {
            dynamic first = 25, second = "25";

            Assert.False(EqualityEvaluator.AreEqual(first, second));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsTrueIfObjectsAreDynamicAndNull()
        {
            dynamic first = null, second = null;

            Assert.True(EqualityEvaluator.AreEqual(first, second));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsTrueIfOneObjectAndOneDynamicAreNull()
        {
            dynamic first = null;
            int? second = null;

            Assert.True(EqualityEvaluator.AreEqual(first, second));
            Assert.True(EqualityEvaluator.AreEqual(second, first));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsFalseIfOneObjectNotNullAndDynamicIsNull()
        {
            int? first = -25;
            dynamic second = null;

            Assert.False(EqualityEvaluator.AreEqual(first, second));
            Assert.False(EqualityEvaluator.AreEqual(second, first));
        }

        [Fact]
        [Trait("Category", "Unit")]
        public void AreEqual_ReturnsFalseIfOneObjectNullAndDynamicIsNotNull()
        {
            int? first = null;
            dynamic second = -25;

            Assert.False(EqualityEvaluator.AreEqual(first, second));
            Assert.False(EqualityEvaluator.AreEqual(second, first));
        }
    }
}
