﻿using System.Collections.Generic;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class SuppressReportSectionActionTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentComment_IfNotADuplicate([Frozen]Mock<IActionRepository> actionRepositoryMock,
            IEnumerable<ExceptionSuppressReportSection> exceptionSuppressReportSections, SuppressReportSectionAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = false;

            actionRepositoryMock.Setup(m => m.LoadExceptionSuppressReportSections(It.IsAny<int>())).Returns(exceptionSuppressReportSections);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentSuppressReport(It.IsAny<AssessmentSuppressReportRequest>()), Times.Exactly(3));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCreateAssessmentComment_IfCommentsAreDuplicate([Frozen]Mock<IActionRepository> actionRepositoryMock,
            List<ExceptionSuppressReportSection> exceptionSuppressReportSections, 
            List<AssessmentExceptionSuppressReportSection> assessmentExceptionSuppressReportSections, SuppressReportSectionAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = false;
            exceptionSuppressReportSections.ForEach(section => { section.ReportSection = "Section5"; });
            assessmentExceptionSuppressReportSections.ForEach(section => { section.ReportSection = "Section5"; });

            actionRepositoryMock.Setup(m => m.LoadExceptionSuppressReportSections(It.IsAny<int>())).Returns(exceptionSuppressReportSections);
            actionRepositoryMock.Setup(m => m.LoadAssessmentExceptionSuppressReportSections(It.IsAny<int>())).Returns(assessmentExceptionSuppressReportSections);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentSuppressReport(It.IsAny<AssessmentSuppressReportRequest>()), Times.Never());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentComments_IfCommentsNotMatchAsDuplicate([Frozen]Mock<IActionRepository> actionRepositoryMock,
            List<ExceptionSuppressReportSection> exceptionSuppressReportSections,
            List<AssessmentExceptionSuppressReportSection> assessmentExceptionSuppressReportSections, SuppressReportSectionAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = false;
            exceptionSuppressReportSections.ForEach(section => { section.ReportSection = "Section5"; });
            assessmentExceptionSuppressReportSections.ForEach(section => { section.ReportSection = "Section6"; });

            actionRepositoryMock.Setup(m => m.LoadExceptionSuppressReportSections(It.IsAny<int>())).Returns(exceptionSuppressReportSections);
            actionRepositoryMock.Setup(m => m.LoadAssessmentExceptionSuppressReportSections(It.IsAny<int>())).Returns(assessmentExceptionSuppressReportSections);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentSuppressReport(It.IsAny<AssessmentSuppressReportRequest>()), Times.Exactly(3));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCreateAssessmentComment_EvenIfDuplicatesAllowed([Frozen]Mock<IActionRepository> actionRepositoryMock,
            List<ExceptionSuppressReportSection> exceptionSuppressReportSections,
            List<AssessmentExceptionSuppressReportSection> assessmentExceptionSuppressReportSections, SuppressReportSectionAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = true;
            exceptionSuppressReportSections.ForEach(section => { section.ReportSection = "Section5"; });
            assessmentExceptionSuppressReportSections.ForEach(section => { section.ReportSection = "Section5"; });

            actionRepositoryMock.Setup(m => m.LoadExceptionSuppressReportSections(It.IsAny<int>())).Returns(exceptionSuppressReportSections);
            actionRepositoryMock.Setup(m => m.LoadAssessmentExceptionSuppressReportSections(It.IsAny<int>())).Returns(assessmentExceptionSuppressReportSections);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentSuppressReport(It.IsAny<AssessmentSuppressReportRequest>()), Times.Never());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_SkipsProcessing_IfExceptionAlreadyProcessed(SuppressReportSectionAction sut)
        {
            var response = sut.PerformExceptionAction(true, null);

            Assert.True(response.ActionSkipped);
        }
    }
}
