﻿using System.Collections.Generic;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class EmailSenderTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void QueueNotificationEmail_RunsWithoutExceptionWithValidInput([Frozen] Mock<IExceptionRepository> exceptionRepository,
            EmailSender sut)
        {
            exceptionRepository.Setup(_ => _.LoadSystemSettings()).Returns(new Dictionary<string, string>
                {
                    {"Exception_EMailNotification_FromAddress", "unittest@test.com"},
                    {"Exception_EMailNotification_FromDisplayName", "test display" }
                });

            sut.QueueNotificationEmail("toAddress@test.com", "email body", "test email subject");
            sut.QueueNotificationEmail("toAddress2@test.com", "email body", "test email subject");

            Assert.Equal(2, sut.TotalEmailsQueued);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void QueueNotificationEmail_ConcatenatesBodiesFromRequestsToSameToEmail([Frozen] Mock<IExceptionRepository> exceptionRepository,
            EmailSender sut)
        {
            exceptionRepository.Setup(_ => _.LoadSystemSettings()).Returns(new Dictionary<string, string>
                {
                    {"Exception_EMailNotification_FromAddress", "unittest@test.com"},
                    {"Exception_EMailNotification_FromDisplayName", "test display" }
                });

            sut.QueueNotificationEmail("toAddress@test.com", "email body", "test email subject");
            sut.QueueNotificationEmail("toAddress@test.com", "second email body", "test email subject");

            Assert.Equal(1, sut.TotalEmailsQueued);
        }

        [Theory(Skip = "This probably works but I'm unsure which smtp host to use"), AutoMoqData]
        [Trait("Category", "Unit")]
        public void SendQueuedEmails_Works([Frozen] Mock<IExceptionRepository> exceptionRepository,
            EmailSender sut)
        {
            exceptionRepository.Setup(_ => _.LoadSystemSettings()).Returns(new Dictionary<string, string>
                {
                    {"Exception_EMailNotification_FromAddress", "unittest@test.com"},
                    {"Exception_EMailNotification_FromDisplayName", "test display" },
                    {"SMTPHost", "fsmail" },
                    {"SMTPPort", "25" }
                });

            sut.QueueNotificationEmail("ppatterson@itelinc.com", "test email body", "unit test email");
            sut.SendQueuedEmails();

            Assert.Equal(0, sut.TotalEmailsQueued);
        }
    }
}
