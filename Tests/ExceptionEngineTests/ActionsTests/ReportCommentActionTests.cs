﻿using System.Collections.Generic;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class ReportCommentActionTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentComment_IfNotADuplicate([Frozen]Mock<IActionRepository> actionRepositoryMock,
            IEnumerable<ExceptionReportComment> exceptionReportComments, ReportCommentAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = false;

            actionRepositoryMock.Setup(m => m.LoadExceptionReportComments(It.IsAny<int>())).Returns(exceptionReportComments);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentComment(It.IsAny<AssessmentCommentRequest>()), Times.Exactly(3));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCreateAssessmentComment_IfCommentsAreDuplicate([Frozen]Mock<IActionRepository> actionRepositoryMock,
            List<ExceptionReportComment> exceptionReportComments, List<AssessmentComment> assessmentComments, ReportCommentAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = false;

            exceptionReportComments.ForEach(comment => { comment.Comment = "This is a comment."; comment.ReportSection = "Section5"; });
            assessmentComments.ForEach(comment => { comment.Comment = "This is a comment."; comment.ReportSection = "Section5"; });

            actionRepositoryMock.Setup(m => m.LoadExceptionReportComments(It.IsAny<int>())).Returns(exceptionReportComments);
            actionRepositoryMock.Setup(m => m.LoadAssessmentComments(It.IsAny<int>())).Returns(assessmentComments);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentComment(It.IsAny<AssessmentCommentRequest>()), Times.Never());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_DoesNotCreateAssessmentComment_EvenIfDuplicatesAllowed([Frozen]Mock<IActionRepository> actionRepositoryMock,
            List<ExceptionReportComment> exceptionReportComments, List<AssessmentComment> assessmentComments, ReportCommentAction sut)
        {
            sut.ExceptionActionRequest.AllowDuplicates = true;

            exceptionReportComments.ForEach(comment => { comment.Comment = "This is a comment."; comment.ReportSection = "Section5"; });
            assessmentComments.ForEach(comment => { comment.Comment = "This is a comment."; comment.ReportSection = "Section5"; });

            actionRepositoryMock.Setup(m => m.LoadExceptionReportComments(It.IsAny<int>())).Returns(exceptionReportComments);
            actionRepositoryMock.Setup(m => m.LoadAssessmentComments(It.IsAny<int>())).Returns(assessmentComments);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentComment(It.IsAny<AssessmentCommentRequest>()), Times.Never());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_SkipsProcessing_IfExceptionAlreadyProcessed(ReportCommentAction sut)
        {
            var response = sut.PerformExceptionAction(true, null);

            Assert.True(response.ActionSkipped);
        }
    }
}
