﻿using System.Collections.Generic;
using AutoFixture;
using AutoFixture.Xunit2;
using ExceptionEngine;
using ExceptionEngine.Actions;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class HoldActionTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CallsCreateAssessmentNote_IfIncludeNoteWithException(ExceptionCaseData exceptionCaseData, 
            [Frozen] Mock<IActionRepository> actionRepositoryMock, HoldReason holdReason, HoldAction sut)
        {
            var fixture = new Fixture();
            exceptionCaseData.Conditions.AddMany(() =>
                fixture.Build<ExceptionCaseCondition>()
                .With(c => c.Operator, FilterOperator.ValueExists)
                .Create(), 5);
            sut.ExceptionActionRequest.IncludeNoteWithException = true;

            actionRepositoryMock.Setup(r => r.LoadExceptionCaseData(It.IsAny<int>())).Returns(exceptionCaseData);
            actionRepositoryMock.Setup(r => r.LoadHoldReason(It.IsAny<int>())).Returns(holdReason);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentNote(It.IsAny<AssessmentNoteRequest>()), Times.Once());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_ClearsHoldsByHoldType(IEnumerable<ExceptionHoldType> exceptionHoldTypes,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, HoldAction sut)
        {

            actionRepositoryMock.Setup(r => r.LoadExceptionHoldTypes(It.IsAny<int>())).Returns(exceptionHoldTypes);
            actionRepositoryMock.Setup(r => r.ClearHoldsByHoldType(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(1);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.ClearHoldsByHoldType(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce());

            Assert.Contains("Clear Assessment Holds before validation with type:", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_ClearsHoldsByHoldType_OnlyWritesMessageIfRowsWereUpdated(IEnumerable<ExceptionHoldType> exceptionHoldTypes,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, HoldAction sut)
        {
            actionRepositoryMock.Setup(r => r.LoadExceptionHoldTypes(It.IsAny<int>())).Returns(exceptionHoldTypes);
            actionRepositoryMock.Setup(r => r.ClearHoldsByHoldType(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>())).Returns(0);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.ClearHoldsByHoldType(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()), Times.AtLeastOnce());

            Assert.DoesNotContain("Clear Assessment Holds before validation with type:", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_SetsAssessmentStatusToHold_IfExceptionNotAlreadyProcessed([Frozen] Mock<IActionRepository> actionRepositoryMock, 
            HoldAction sut)
        {
            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.SetAssessmentStatusToHold(It.IsAny<int>(), "ONHOLD"), Times.Once());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentHoldComments_ForeachExceptionMappingHoldComment(IEnumerable<ExceptionHoldComment> exceptionHoldComments,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, HoldAction sut)
        {
            sut.ExceptionActionRequest.IncludeNoteWithException = false;

            actionRepositoryMock.Setup(m => m.LoadExceptionHoldComments(It.IsAny<int>())).Returns(exceptionHoldComments);
            actionRepositoryMock.Setup(m => m.AssessmentHoldExists(It.IsAny<int>(), It.IsAny<int>())).Returns(false);

            sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentNote(It.IsAny<AssessmentNoteRequest>()), Times.Exactly(3));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentHold_IfHoldDoesNotAlreadyExist([Frozen] Mock<IActionRepository> actionRepositoryMock,
            HoldAction sut)
        {
            AssessmentHoldRequest assessmentHoldRequest = null;

            actionRepositoryMock.Setup(m => m.AssessmentHoldExists(It.IsAny<int>(), It.IsAny<int>())).Returns(false);
            actionRepositoryMock.Setup(m => m.CreateAssessmentHold(It.IsAny<AssessmentHoldRequest>()))
                .Callback<AssessmentHoldRequest>((ahr) => assessmentHoldRequest = ahr);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentHold(It.IsAny<AssessmentHoldRequest>()), Times.Once());
            Assert.Equal(sut.AssessmentId, assessmentHoldRequest.AssessmentId);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentHold_SetsHoldReasonIdIfHoldReasonExists([Frozen] Mock<IActionRepository> actionRepositoryMock,
            HoldAction sut)
        {
            var holdReason = new HoldReason { HoldReasonId = 55, Reason = "Sample Too Smelly" };

            AssessmentHoldRequest assessmentHoldRequest = null;

            actionRepositoryMock.Setup(m => m.AssessmentHoldExists(It.IsAny<int>(), It.IsAny<int>())).Returns(false);
            actionRepositoryMock.Setup(m => m.LoadHoldReason(It.IsAny<int>())).Returns(holdReason);
            actionRepositoryMock.Setup(m => m.CreateAssessmentHold(It.IsAny<AssessmentHoldRequest>()))
                .Callback<AssessmentHoldRequest>((ahr) => assessmentHoldRequest = ahr);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentHold(It.IsAny<AssessmentHoldRequest>()), Times.Once());
            Assert.Equal(55, assessmentHoldRequest.HoldReasonId);
            Assert.Contains("HoldReason: Sample Too Smelly", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_CreatesAssessmentHold_SetsHoldReasonIdNullIfNoHoldReasonExists([Frozen] Mock<IActionRepository> actionRepositoryMock,
            HoldAction sut)
        {
            AssessmentHoldRequest assessmentHoldRequest = null;

            actionRepositoryMock.Setup(m => m.AssessmentHoldExists(It.IsAny<int>(), It.IsAny<int>())).Returns(false);
            actionRepositoryMock.Setup(m => m.LoadHoldReason(It.IsAny<int>())).Returns(value: null);
            actionRepositoryMock.Setup(m => m.CreateAssessmentHold(It.IsAny<AssessmentHoldRequest>()))
                .Callback<AssessmentHoldRequest>((ahr) => assessmentHoldRequest = ahr);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentHold(It.IsAny<AssessmentHoldRequest>()), Times.Once());
            Assert.Null(assessmentHoldRequest.HoldReasonId);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_SkipsCreateAssessmentHold_IfHoldAlreadyExists([Frozen] Mock<IActionRepository> actionRepositoryMock,
            HoldAction sut)
        {
            actionRepositoryMock.Setup(m => m.AssessmentHoldExists(It.IsAny<int>(), It.IsAny<int>())).Returns(true);

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentHold(It.IsAny<AssessmentHoldRequest>()), Times.Never());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_SkipsAction_IfExceptionAlreadyProcessed([Frozen] Mock<IActionRepository> actionRepositoryMock,
            HoldAction sut)
        {
            var response = sut.PerformExceptionAction(true, null);

            actionRepositoryMock.Verify(m => m.CreateAssessmentHold(It.IsAny<AssessmentHoldRequest>()), Times.Never());

            Assert.True(response.ActionSkipped);
        }
    }
}
