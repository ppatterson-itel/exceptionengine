﻿using System;
using AutoFixture;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class BetweenFormatterTests
    {
        private readonly IFixture _fixture;

        public BetweenFormatterTests()
        {
            _fixture = new Fixture();
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(BetweenFormatter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.FormatExceptionCaseCondition(null));            
        }

        // To do this we need to create a DataMemberAutoData attribute combining the functionality of DataMember in xUnit
        // with AutoData in AutoFixture
        // or could do something similar to this instead https://andrewlock.net/creating-strongly-typed-xunit-theory-test-data-with-theorydata/
        //[Theory]
        //[InlineAutoData(new DateTime(2018, 1, 1), new DateTime(2018, 2, 1), "is between 01/01/2018 00:00 and 02/01/2018 00:00"))]
        //public void FormatExceptionCaseCondition_ReturnsExpectedString(object valueFrom, object valueTo, string expectedResult, BetweenFormatter sut)
        //{
        //    var ecc = fixture.Build<ExceptionCaseCondition>().With(e => e.ValueFrom, valueFrom)
        //        .With(e => e.ValueTo, valueTo).Create<ExceptionCaseCondition>();

        //    var result = sut.FormatExceptionCaseCondition(ecc);

        //    Assert.Equal(expectedResult, result);
        //}

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_returnsDateTimeString_IfValueFromAndValueToAreDateTime(BetweenFormatter sut)
        {
            var ecc = _fixture.Build<ExceptionCaseCondition>().With(e => e.ValueFrom, new DateTime(2018,1,1))
                .With(e => e.ValueTo, new DateTime(2018, 2, 1)).Create();

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("is between 01/01/2018 00:00 and 02/01/2018 00:00", result);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_returnsDefaultString_IfValueFromAndValueToAreIntegers(BetweenFormatter sut)
        {
            var ecc = _fixture.Build<ExceptionCaseCondition>().With(e => e.ValueFrom, 25)
                .With(e => e.ValueTo, 32).Create();

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("is between 25 and 32", result);
        }
    }
}
