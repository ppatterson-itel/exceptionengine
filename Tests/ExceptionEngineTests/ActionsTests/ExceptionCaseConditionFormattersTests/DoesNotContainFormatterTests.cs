﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class DoesNotContainFormatterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(DoesNotContainFormatter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.FormatExceptionCaseCondition(null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsString(ExceptionCaseCondition ecc, DoesNotContainFormatter sut)
        {
            ecc.ValueFrom = "state farm";

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("doesn't contain 'state farm'", result);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsInt(ExceptionCaseCondition ecc, DoesNotContainFormatter sut)
        {
            ecc.ValueFrom = 34;

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("doesn't contain '34'", result);
        }
    }
}
