﻿using AutoFixture;
using ExceptionEngine;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using ExceptionEngine.Repositories;
using System;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class ExceptionCaseFormatterFactoryTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfActionRepositoryIsNull()
        {
            var fixture = new Fixture().ConstructorArgumentFor<ExceptionCaseFormatterFactory, IActionRepository>("actionRepository", null);

            var exception = Record.Exception(() => fixture.Create<ExceptionCaseFormatterFactory>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(FilterOperator.ValueExists, typeof(ValueExistsFormatter))]
        [InlineAutoMoqData(FilterOperator.ValueIsAbsent, typeof(ValueIsAbsentFormatter))]
        [InlineAutoMoqData(FilterOperator.Equal, typeof(EqualFormatter))]
        [InlineAutoMoqData(FilterOperator.NotEqual, typeof(NotEqualFormatter))]
        [InlineAutoMoqData(FilterOperator.Less, typeof(LessFormatter))]
        [InlineAutoMoqData(FilterOperator.Greater, typeof(GreaterFormatter))]
        [InlineAutoMoqData(FilterOperator.Between, typeof(BetweenFormatter))]
        [InlineAutoMoqData(FilterOperator.NotBetween, typeof(NotBetweenFormatter))]
        [InlineAutoMoqData(FilterOperator.StartsWith, typeof(StartsWithFormatter))]
        [InlineAutoMoqData(FilterOperator.Contains, typeof(ContainsFormatter))]
        [InlineAutoMoqData(FilterOperator.DoesNotContain, typeof(DoesNotContainFormatter))]
        [InlineAutoMoqData(FilterOperator.FitRegularExpression, typeof(FitRegularExpressionFormatter))]
        [InlineAutoMoqData(FilterOperator.NotFitRegularExpression, typeof(NotFitRegularExpressionFormatter))]
        [InlineAutoMoqData(FilterOperator.InList, typeof(InListFormatter))]
        [InlineAutoMoqData(FilterOperator.NotInList, typeof(NotInListFormatter))]
        public void GetConditionFilter_ReturnsCorrectFilter(FilterOperator filterOperator, Type expectedType,
            ExceptionCaseFormatterFactory sut)
        {
            var result = sut.GetFormatter(filterOperator);

            Assert.Equal(expectedType, result.GetType());
        }

        [Theory]
        [Trait("Category", "Unit")]
        [InlineAutoMoqData(FilterOperator.Absent)]
        [InlineAutoMoqData(FilterOperator.Complex)]
        public void GetConditionFilter_ThrowsArgumentException_IfTypeIsNotValid(FilterOperator filterOperator,
            ExceptionCaseFormatterFactory sut)
        {
            Assert.Throws<ArgumentException>(() => sut.GetFormatter(filterOperator));
        }
    }
}
