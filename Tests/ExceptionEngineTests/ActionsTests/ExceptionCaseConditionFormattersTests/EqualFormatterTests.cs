﻿using System;
using AutoFixture;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class EqualFormatterTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfActionRepositoryIsNull()
        {
            var fixture = new Fixture().ConstructorArgumentFor<EqualFormatter, IActionRepository>("actionRepository", null);

            var exception = Record.Exception(() => fixture.Create<EqualFormatter>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }

            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(EqualFormatter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.FormatExceptionCaseCondition(null));
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsBoolean(ExceptionCaseCondition ecc,
            EqualFormatter sut)
        {
            ecc.ValueFrom = true;
            ecc.LookupTable = string.Empty;

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("is checked", result);

            ecc.ValueFrom = false;
            result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("is unchecked", result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsDateTime(ExceptionCaseCondition ecc,
            EqualFormatter sut)
        {
            ecc.ValueFrom = new DateTime(2018, 1, 1);
            ecc.LookupTable = string.Empty;

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("= 01/01/2018 00:00", result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsInt(ExceptionCaseCondition ecc,
            EqualFormatter sut)
        {
            ecc.ValueFrom = 42;
            ecc.LookupTable = string.Empty;

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("= 42", result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFromPickListByID_IfLookupTableStartsWithPoundSign(ExceptionCaseCondition ecc,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, EqualFormatter sut)
        {
            ecc.ValueFrom = "code";
            ecc.LookupTable = "#42";

            actionRepositoryMock.Setup(r => r.LoadPickListDisplayTextById(42, "code")).Returns("returnedValue");

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("= returnedValue", result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFromViewName_IfLookupTableStartsWithvwplist_(ExceptionCaseCondition ecc,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, EqualFormatter sut)
        {
            ecc.ValueFrom = "Florida";
            ecc.LookupTable = "vwplist_table";
            ecc.LookupColumn = "state";

            actionRepositoryMock.Setup(r => r.LoadPickListDisplayTextByViewName("state", "Florida")).Returns("FL");

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("= FL", result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFromDynamicLookup_VariousConditions(ExceptionCaseCondition ecc,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, EqualFormatter sut)
        {
            ecc.ValueFrom = "value";
            ecc.LookupTable = "table";
            ecc.LookupColumn = "column";
            ecc.LookupReturnColumn = "returnColumn";

            actionRepositoryMock.Setup(r => r.LoadPickListDisplayTextDynamic("table", "column", "returnColumn", "value")).Returns("yeet");

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("= yeet", result);

            ecc.LookupTable = "vwplist_table";
            ecc.LookupColumn = "code";

            actionRepositoryMock.Setup(r => r.LoadPickListDisplayTextDynamic("vwplist_table", "code", "returnColumn", "value")).Returns("yeet2");
            result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("= yeet2", result);

            ecc.LookupTable = "vwplist_table";
            ecc.LookupColumn = "column";
            ecc.LookupReturnColumn = "displaytext";

            actionRepositoryMock.Setup(r => r.LoadPickListDisplayTextDynamic("vwplist_table", "column", "displaytext", "value")).Returns("yeet3");
            result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("= yeet3", result);
        }
    }
}
