﻿using AutoFixture.Xunit2;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class ValueIsAbsentFormatterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(ValueIsAbsentFormatter sut)
        {
            var result = sut.FormatExceptionCaseCondition(null);

            Assert.Equal("value is absent", result);
        }
    }
}
