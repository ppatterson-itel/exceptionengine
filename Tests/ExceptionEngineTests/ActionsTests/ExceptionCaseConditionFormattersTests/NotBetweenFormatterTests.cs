﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class NotBetweenFormatterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(NotBetweenFormatter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.FormatExceptionCaseCondition(null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsDateTime(ExceptionCaseCondition ecc, NotBetweenFormatter sut)
        {
            ecc.ValueFrom = new DateTime(2018, 1, 1);
            ecc.ValueTo = new DateTime(2018, 2, 1);

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("is not between 01/01/2018 00:00 and 02/01/2018 00:00", result);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsInt(ExceptionCaseCondition ecc, NotBetweenFormatter sut)
        {
            ecc.ValueFrom = 42;
            ecc.ValueTo = 52;

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("is not between 42 and 52", result);
        }
    }
}
