﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class NotFitRegularExpressionFormatterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(NotFitRegularExpressionFormatter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.FormatExceptionCaseCondition(null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsBoolean(ExceptionCaseCondition ecc,
            NotFitRegularExpressionFormatter sut)
        {
            ecc.ValueFrom = "regularExpression";

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("doesn't fit regexp 'regularExpression'", result);
        }
    }
}
