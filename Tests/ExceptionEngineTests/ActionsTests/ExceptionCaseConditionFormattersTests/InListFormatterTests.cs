﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class InListFormatterTests
    {
        [Fact]
        [Trait("Category", "Unit")]
        public void Constructor_ThrowsArgumentNullException_IfActionRepositoryIsNull()
        {
            var fixture = new Fixture().ConstructorArgumentFor<InListFormatter, IActionRepository>("actionRepository", null);

            var exception = Record.Exception(() => fixture.Create<InListFormatter>());

            while (exception?.InnerException != null)
            {
                exception = exception.InnerException;
            }
            
            Assert.Equal(typeof(ArgumentNullException), exception?.GetType());
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsStringFormat_OfEachItemInList(ExceptionCaseCondition ecc,
            [Frozen] Mock<IActionRepository> actionRepositoryMock, InListFormatter sut)
        {
            var intCollection = new List<ConditionValue>()
            {
                new ConditionValue { Value = 15} , new ConditionValue { Value = 32}, new ConditionValue { Value = 75 }
            };

            actionRepositoryMock.Setup(d => d.LoadConditionValues(It.IsAny<int>())).Returns(intCollection);

            var response = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("is in list[15, 32, 75]", response);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(
            InListFormatter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.FormatExceptionCaseCondition(null));
        }
    }
}
