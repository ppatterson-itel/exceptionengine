﻿using AutoFixture.Xunit2;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class ValueExistsTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(ValueExistsFormatter sut)
        {
            var result = sut.FormatExceptionCaseCondition(null);

            Assert.Equal("value exists", result);
        }
    }
}
