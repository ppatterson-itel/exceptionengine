﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class GreaterFormatterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(GreaterFormatter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.FormatExceptionCaseCondition(null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfExceptionCaseConditionIsDate(ExceptionCaseCondition ecc,
            GreaterFormatter sut)
        {
            ecc.ValueFrom = new DateTime(2018, 1, 1);

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("> 01/01/2018 00:00", result);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfExceptionCaseConditionIsInt(ExceptionCaseCondition ecc,
            GreaterFormatter sut)
        {
            ecc.ValueFrom = 42;

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("> 42", result);
        }
    }
}
