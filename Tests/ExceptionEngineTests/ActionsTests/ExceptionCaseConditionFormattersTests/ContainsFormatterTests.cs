﻿using System;
using AutoFixture.Xunit2;
using ExceptionEngine.Actions.DataTransferObjects;
using ExceptionEngine.Actions.ExceptionCaseConditionFormatters;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests.ExceptionCaseConditionFormattersTests
{
    public class ContainsFormatterTests
    {
        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ThrowsArgumentNullException_IfExceptionCaseConditionIsNull(ContainsFormatter sut)
        {
            Assert.Throws<ArgumentNullException>(() => sut.FormatExceptionCaseCondition(null));
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsString(ExceptionCaseCondition ecc, ContainsFormatter sut)
        {
            ecc.ValueFrom = "state farm";

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("contains 'state farm'", result);
        }

        [Theory, AutoData]
        [Trait("Category", "Unit")]
        public void FormatExceptionCaseCondition_ReturnsFormattedString_IfValueFromIsInt(ExceptionCaseCondition ecc, ContainsFormatter sut)
        {
            ecc.ValueFrom = 34;

            var result = sut.FormatExceptionCaseCondition(ecc);

            Assert.Equal("contains '34'", result);
        }
    }
}
