﻿using AutoFixture.Xunit2;
using ExceptionEngine.Actions;
using ExceptionEngine.Repositories;
using Moq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests.ActionsTests
{
    public class CustomActionTests
    {
        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_RunsClearAssessmentPricingAction_WhenValueEquals_ClearAssessmentPricing([Frozen] Mock<IActionRepository> actionRepositoryMock,
            CustomAction sut)
        {
            sut.ExceptionActionRequest.Value = "ClearAssessmentPricing";

            var response = sut.PerformExceptionAction(false, null);

            actionRepositoryMock.Verify(mock => mock.ClearAssessmentPricing(sut.AssessmentId), Times.Once());

            Assert.Equal("Custom Action: ClearAssessmentPricing; ", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_ReturnsErrorMessage_WhenValueIsInvalid(CustomAction sut)
        {
            sut.ExceptionActionRequest.Value = "win";

            var response = sut.PerformExceptionAction(false, null);

            Assert.Contains("Custom Action: Method with name 'win' have raised exception: 'method win does not exist.", response.ActionsTaken);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void PerformExceptionAction_ReturnsEmptyMessage_WhenValueIsWhitespace(CustomAction sut)
        {
            sut.ExceptionActionRequest.Value = string.Empty;

            var response = sut.PerformExceptionAction(false, null);

            Assert.Equal(string.Empty, response.ActionsTaken);
        }
    }
}
