﻿using System;
using System.Linq;
using System.Text;
using AutoFixture;
using AutoFixture.AutoMoq;
using ExceptionEngine.Exceptions.DataTransferObjects;
using ExceptionEngine.Repositories;
using TestHelpers;
using Xunit;
using Exception = ExceptionEngine.Exceptions.DataTransferObjects.Exception;

namespace Tests.ExceptionEngineTests
{
    public class ExceptionRepositoryTests
    {
        private readonly string _connectionString = "Data Source = DVSQL01.tsitel.com; Initial Catalog = Enterprise; User ID = iteladmin; Password = sxt56#L2A1NZ;";
        private readonly ExceptionRepository _repository;

        public ExceptionRepositoryTests()
        {
            _repository = new ExceptionRepository(_connectionString);
        }

        //[Theory]
        //[Trait("Category", "Unit")]
        //[MemberData(nameof(ExceptionRepositoryTestData.GetLoadAssessmentsData), MemberType = typeof(ExceptionRepositoryTestData))]
        //public void LoadAssessment_ReturnsValidAssessmentFromDB(int assessmentId, Dictionary<string, dynamic> expectedPropertyValues)
        //{
        //    var fixture = new Fixture().Customize(new AutoMoqCustomization())
        //        .ConstructorArgumentFor<ExceptionRepository, string>("connectionString", _connectionString);

        //    var sut = fixture.Create<ExceptionRepository>();

        //    var assessment = sut.LoadAssessment(assessmentId);

        //    Assert.NotNull(assessment);

        //    var properties = assessment.GetType().GetProperties();
        //    StringBuilder unmatchedProperties = new StringBuilder();

        //    foreach (var property in properties)
        //    {
        //        var lookupKey = property.Name.ToLowerInvariant();
        //        if (!expectedPropertyValues.ContainsKey(lookupKey))
        //            unmatchedProperties.AppendLine(
        //                $"Property {property.Name} wasn't found in the expected values dictionary.");
        //        else
        //        {
        //            var expected = expectedPropertyValues[lookupKey];
        //            var actual = property.GetValue(assessment, null);
        //            if (expected == null)
        //            {
        //                if (actual != null)
        //                {
        //                    unmatchedProperties.AppendLine($"Property mismatch for [{property.Name}]; expectedValue [{expected}], actual value [{actual}]");
        //                }
        //            }
        //            else if (!expected.Equals(actual))
        //            {
        //                unmatchedProperties.AppendLine($"Property mismatch for [{property.Name}]; expectedValue [{expected}], actual value [{actual}]");
        //            }
        //        }
        //    }

        //    Assert.True(unmatchedProperties.Length == 0, unmatchedProperties.ToString());
        //}

        [Theory]
        [Trait("Category", "Unit")]
        [MemberData(nameof(ExceptionRepositoryTestData.GetLoadPreRuleConditionsData), MemberType = typeof(ExceptionRepositoryTestData))]
        public void LoadPreruleConditions_ReturnsValidAssessmentExceptionsFromDB(int assessmentId, ExpectedPropertyValuesCollection expectedExceptions,
            ExpectedPropertyValuesCollection expectedExceptionCases, ExpectedPropertyValuesCollection expectedExceptionConditions)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization())
                .ConstructorArgumentFor<ExceptionRepository, string>("connectionString", _connectionString);

            var sut = fixture.Create<ExceptionRepository>();

            var assessmentExceptions = sut.LoadPreRuleConditions(assessmentId);

            Assert.NotNull(assessmentExceptions);
            Assert.Equal(assessmentId, assessmentExceptions.AssessmentId);

            StringBuilder unmatchedProperties = new StringBuilder();

            foreach (var exception in assessmentExceptions.Exceptions)
            {
                unmatchedProperties.AppendJoin(Environment.NewLine,
                    PropertyValuesValidator.GetUnmatchedProperties(exception, expectedExceptions, exception.ExceptionId));

                foreach (var exceptionCase in exception.ExceptionCases)
                {
                    unmatchedProperties.AppendJoin(Environment.NewLine,
                        PropertyValuesValidator.GetUnmatchedProperties(exceptionCase, expectedExceptionCases, exceptionCase.ExceptionCaseId));

                    foreach (var exceptionCondition in exceptionCase.ExceptionConditions)
                    {
                        unmatchedProperties.AppendJoin(Environment.NewLine,
                            PropertyValuesValidator.GetUnmatchedProperties(exceptionCondition, expectedExceptionConditions, exceptionCondition.ExceptionConditionId));
                    }
                }
            }

            Assert.True(unmatchedProperties.Length == 0, unmatchedProperties.ToString());
        }

        [Theory]
        [Trait("Category", "Unit")]
        [MemberData(nameof(ExceptionRepositoryTestData.GetLoadAssessmentExceptionsData), MemberType = typeof(ExceptionRepositoryTestData))]
        public void LoadAssessmentExceptions_ReturnsExceptionsAndCasesFromDB(int assessmentId, ExpectedPropertyValuesCollection expectedExceptions,
            ExpectedPropertyValuesCollection expectedExceptionCases, ExpectedPropertyValuesCollection expectedExceptionConditions)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization())
                .ConstructorArgumentFor<ExceptionRepository, string>("connectionString", _connectionString);

            var sut = fixture.Create<ExceptionRepository>();

            var assessmentExceptions = sut.LoadAssessmentExceptions(assessmentId);

            Assert.NotNull(assessmentExceptions);
            Assert.Equal(assessmentId, assessmentExceptions.AssessmentId);

            StringBuilder unmatchedProperties = new StringBuilder();

            foreach (var exception in assessmentExceptions.Exceptions)
            {
                unmatchedProperties.AppendJoin(Environment.NewLine,
                    PropertyValuesValidator.GetUnmatchedProperties(exception, expectedExceptions, exception.ExceptionId));

                foreach (var exceptionCase in exception.ExceptionCases)
                {
                    unmatchedProperties.AppendJoin(Environment.NewLine,
                        PropertyValuesValidator.GetUnmatchedProperties(exceptionCase, expectedExceptionCases, exceptionCase.ExceptionCaseId));

                    foreach (var exceptionCondition in exceptionCase.ExceptionConditions)
                    {
                        unmatchedProperties.AppendJoin(Environment.NewLine,
                            PropertyValuesValidator.GetUnmatchedProperties(exceptionCondition, expectedExceptionConditions, exceptionCondition.ExceptionConditionId));
                    }
                }
            }

            Assert.True(unmatchedProperties.Length == 0, unmatchedProperties.ToString());
        }

        [Fact]
        public void ExecuteExceptionStoredProcedure_ReturnsValidAssessmentExceptionsFromDB()
        {
            var exceptionCase = new ExceptionCaseDto
            {
                ExceptionCaseId = 361
            };
            var exception = new Exception();

            var exceptionCaseResponse = _repository.ExecuteExceptionStoredProcedure("spExceptionArkansasFarmBureauPilotClearFees", 1935210, exceptionCase, exception);

            Assert.NotNull(exceptionCaseResponse);
        }

        [Fact]
        public void CheckForAdjustHoldOnAssessment_ReturnsValidExceptionCaseResponseFromDB()
        {
            var exceptionCase = new ExceptionCaseDto();
            var exception = new Exception();

            var exceptionCaseResponse = _repository.CheckForAdjustHoldOnAssessment(1926089, exceptionCase, exception);

            Assert.True(exceptionCaseResponse.Result);
        }

        [Fact]
        public void CheckForCustomerArHoldOnAssessment_ReturnsValidExceptionCaseResponseFromDB()
        {
            var exceptionCase = new ExceptionCaseDto();
            var exception = new Exception();

            var exceptionCaseResponse = _repository.CheckForCustomerArHoldOnAssessment(1846832, exceptionCase, exception);

            Assert.True(exceptionCaseResponse.Result);
        }

        [Fact]
        public void LoadExceptionConditions_ReturnsValidCollectionFromDB()
        {
            var exceptionConditions = _repository.LoadExceptionConditions(418);

            Assert.True(exceptionConditions.Count() > 0);
        }

        [Fact]
        public void LoadAssessmentConditionOverridden_ReturnsValidResponseFromDB()
        {
            var response = _repository.AssessmentCaseOverridden(23487, 87);
            var response2 = _repository.AssessmentCaseOverridden(23487, 90);

            Assert.True(response && !response2);
        }

        [Fact]
        public void LoadExceptionConditionValues_ReturnsValidResponseFromDB()
        {
            var response = _repository.LoadExceptionConditionValues(1351);

            Assert.Equal(29, response.Count());
        }

        [Fact]
        public void LoadSystemSettings_RetrievesAppSettingsFromDB()
        {
            var response = _repository.LoadSystemSettings();

            Assert.Equal(7, response.Count());
        }

        [Fact]
        public void LoadAssessmentViewData_ReturnsForValidAssessmentIDAndExceptionViewID()
        {
            var response = _repository.LoadAssessmentViewData(59, 1);

            Assert.Equal(141, response.Count);

            response = _repository.LoadAssessmentViewData(417063, 2);

            Assert.Equal(3, response.Count);
        }

        [Fact]
        public void LoadAssessmentAndViewProductData_ReturnsForValidAssessmentIDAndExceptionViewID()
        {
            var response = _repository.LoadAssessmentAndViewProductData(59, 1);

            Assert.Equal("S", response.AssessmentProductType);
            Assert.Equal("C", response.ExceptionViewProductType);
        }

        [Fact]
        public void LoadAssessmentAndViewProductData_ReturnsNullExceptionViewProductTypeIfNotExists()
        {
            var response = _repository.LoadAssessmentAndViewProductData(59, null);

            Assert.Null(response.ExceptionViewProductType);

            response = _repository.LoadAssessmentAndViewProductData(59, -1);

            Assert.Null(response.ExceptionViewProductType);
        }
    }
}
