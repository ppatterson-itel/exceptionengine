﻿using System.Collections.ObjectModel;
using System.Linq;
using ExceptionEngine.Repositories;
using Xunit;

namespace Tests.ExceptionEngineTests
{
    public class ActionRepositoryTests
    {
        private readonly string _connectionString = "Data Source=DVSQL01.tsitel.com;Initial Catalog=Enterprise;User ID=iteladmin;Password=sxt56#L2A1NZ;";
        private readonly ActionRepository _repository;

        public ActionRepositoryTests()
        {
            _repository = new ActionRepository(_connectionString);
        }

        [Fact]
        public void LoadAssessmentExceptionNotifications_ReturnsValidDataFromDB()
        {
            var response = _repository.LoadAssessmentExceptionNotifications(1951260);

            Assert.True(response.Any());
        }

        [Fact]
        public void LoadExceptionActionNotifications_ReturnsNotificationsFromDB()
        {
            var response = _repository.LoadExceptionActionNotifications(157);

            Assert.True(response.Any());
        }

        [Fact]
        public void LoadExceptionActions_ReturnsActionRequestsFromDB()
        {
            var response = _repository.LoadExceptionActions(28);

            Assert.Equal(3, response.Count());
        }

        [Fact(Skip ="Do not want to create rows in DB every time test is run.")]
        public void CreateAlertNotification_RunsWithoutRaisingException()
        {
            const string description = "Analyst: ASPEN. Discontinued Product. Service Type = M; Account Number = ASPC0001; Invoice Date = 6/13/2012 12:00:00 AM.";
            _repository.CreateAlertNotification(888476, 183, description, 192, null, 119, null);
        }

        [Fact]
        public void LoadExceptionActionInternalNotes_ReturnsFromDB()
        {
            var response = _repository.LoadExceptionActionInternalNotes(72);

            Assert.Equal(2, response.Count());
        }

        [Fact]
        public void LoadAssessmentNotes_ReturnsNotesFromDB()
        {
            var response = _repository.LoadAssessmentNotes(1862211);

            Assert.Equal(183, response.Count());
        }

        [Fact]
        public void CreateAssessmentNote_RunsWithoutRaisingException()
        {
            _repository.CreateAssessmentNote(new AssessmentNoteRequest
            {
                AssessmentId = 1389,
                Note = "Test Note from Unit Test",
                SourceNoteId = null,
                NoteType = "INTNOTE",
                UserId = 1
            });
        }

        [Fact]
        public void LoadExceptionCaseData_ReturnsFromDB()
        {
            var response = _repository.LoadExceptionCaseData(103);

            Assert.NotNull(response);
            Assert.Equal(3, response.Conditions.Count());

            response = _repository.LoadExceptionCaseData(378);

            Assert.NotNull(response);
            Assert.Equal(36, response.ConditionValues.Count());
        }

        [Fact]
        public void LoadExceptionReportComments_ReturnsValidFromDB()
        {
            var response = _repository.LoadExceptionReportComments(70);

            Assert.True(response.Count() == 1);
        }

        [Fact]
        public void LoadAssessmentComments_ReturnsValidFromDB()
        {
            var response = _repository.LoadAssessmentComments(1487285);

            Assert.Equal(21, response.Count());
        }

        [Fact(Skip = "Do not want to create rows in DB every time test is run.")]
        public void CreateAssessmentComment_RunsWithoutRaisingException()
        {
            _repository.CreateAssessmentComment(new AssessmentCommentRequest
            {
                UserId = 1,
                AssessmentId = 1080180,
                Comment = "Test Comment from unit test",
                Description = "test description from unit test",
                ExceptionCaseId = 1,
                ReportSection = "FLOORANAL",
                SourceCommentId =  null
            });
        }

        [Fact]
        public void LoadExceptionHoldComments_ReturnsValidFromDB()
        {
            var response = _repository.LoadExceptionHoldComments(153);

            Assert.Single(response);
        }

        [Fact]
        public void LoadExceptionHoldTypes_ReturnsValidFromDB()
        {
            var response = _repository.LoadExceptionHoldTypes(47);

            Assert.Equal(2, response.Count());
        }

        [Fact]
        public void LoadHoldReason_LoadsValidFromDB()
        {
            var response = _repository.LoadHoldReason(156);

            Assert.Equal("Reason 1", response.Reason);
        }

        [Fact]
        public void ClearHoldsByHoldType_RunsWithoutRaisingException()
        {
            _repository.ClearHoldsByHoldType(1951214, 1, 1);
        }

        [Fact]
        public void AssessmentHoldExists_ReturnsValidFromDB()
        {
            var response = _repository.AssessmentHoldExists(1951145, 1);

            Assert.True(response);

            Assert.False(_repository.AssessmentHoldExists(1951508, 1));
        }

        [Fact]
        public void LoadHoldType_ReturnsFromDB()
        {
            var response = _repository.LoadHoldType(45);

            Assert.Equal("Order Entry", response.DisplayText);
        }

        [Fact]
        public void CreateAssessmentHold_RunsWithoutRaisingException()
        {
            _repository.CreateAssessmentHold(new AssessmentHoldRequest
            {
                AssessmentId = 1951214,
                HoldReasonId = null,
                HoldTypeId = 1,
                IsManualHold = false,
                UserId = 1
            });
        }

        [Fact]
        public void LoadExceptionSuppressReportSections_ReturnsValidFromDB()
        {
            var response = _repository.LoadExceptionSuppressReportSections(1);

            Assert.NotNull(response);
            //Assert.True(response.Count() > 0); TABLE IS EMPTY IN DEV
        }

        [Fact]
        public void LoadAssessmentExceptionSuppressReportSections_ReturnsValidFromDB()
        {
            var response = _repository.LoadAssessmentExceptionSuppressReportSections(1);

            Assert.NotNull(response);
            Assert.True(response.Count() > 0);
        }

        [Fact]
        public void CreateAssessmentSuppressReport_RunsWithoutRaisingException()
        {
            _repository.CreateAssessmentSuppressReport(new AssessmentSuppressReportRequest
            {
                AssessmentId = 1,
                Description = "Test Description",
                ExceptionCaseId = 1,
                ReportSection = "RSection1"
            });
        }

        [Fact]
        public void ClearAssessmentPricing_RunsWithoutRaisingException()
        {
            _repository.ClearAssessmentPricing(1951261);
        }

        [Fact]
        public void LoadPickListDiplayTextByID_ReturnsValidResultFromDB()
        {
            var response = _repository.LoadPickListDisplayTextById(1, "F");
            Assert.Equal("Graphics Tufted", response);
        }

        [Fact]
        public void LoadPickListDisplayTextByViewName_ReturnsValidResultFromDB()
        {
            var response = _repository.LoadPickListDisplayTextByViewName("vwPLIST_ManufacturingTechnique", "F");
            Assert.Equal("Graphics Tufted", response);
        }

        [Fact]
        public void LoadPickListDisplayTextDynamic_ReturnsValidFromDB()
        {
            var response = _repository.LoadPickListDisplayTextDynamic("State", "Code", "Name", "WA");

            Assert.Equal("Washington", response);
        }

        [Fact]
        public void UpdateAssessmentException_CreatesNewOrUpdatesExistingAssessmentException()
        {
            var response = _repository.UpdateAssessmentException(1, 1, new Collection<string>
            {
                "ACTION 1; ", "ACTION 2"
            }, "Unit test run successfully", null);

            Assert.True(response > 0);
        }

        [Fact]
        public void SetAssessmentStatusToHold_UpdatesAssessmentInDB()
        {
            _repository.SetAssessmentStatusToHold(1, "DONE");
        }

        [Fact]
        public void UpdateLastValidationAssessmentExceptionIds_UpdatesAssessmentInDB()
        {
            _repository.UpdateLastValidationAssessmentExceptionIds(1, "1,2,3");

            _repository.UpdateLastValidationAssessmentExceptionIds(1, null);
        }

        [Fact]
        public void ClearAssessmentHolds_UpdatesAssessmentHoldInDB()
        {
            _repository.ClearAssessmentHolds(1951214, 1);
        }

        [Fact]
        public void ClearAssessmentStatus_UpdatesAssessmentInDB()
        {
            _repository.ClearAssessmentStatus(1951214, "INPROGRESS");
        }
    }
}
