﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests
{
    public static class ExceptionRepositoryTestData
    {
        private static readonly string ConnectionString =
            "Data Source=DVSQL01.tsitel.com;Initial Catalog=Enterprise;User ID=iteladmin;Password=sxt56#L2A1NZ;";

        static ExceptionRepositoryTestData()
        {

        }

        public static TheoryData<int, Dictionary<string, dynamic>> GetLoadAssessmentsData()
        {
            TheoryData<int, Dictionary<string, dynamic>> assessmentIdAndData =
                new TheoryData<int, Dictionary<string, dynamic>>();
            IDictionary<string, object> retrievedViewData;

            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                retrievedViewData = (IDictionary<string, object>) cn.Query(GetLoadAssessmentSql(),
                    commandType: System.Data.CommandType.Text).FirstOrDefault();
            }

            var columnNames = retrievedViewData.Keys;

            Dictionary<string, dynamic> assessmentData = new Dictionary<string, dynamic>();
            foreach (var column in columnNames)
            {
                assessmentData.Add(column.ToLowerInvariant(), retrievedViewData[column]);
                if (column.Equals("AssessmentID", StringComparison.InvariantCultureIgnoreCase))
                    assessmentIdAndData.Add((int) retrievedViewData[column], assessmentData);
            }

            return assessmentIdAndData;
        }

        private static string GetLoadAssessmentSql()
        {
            return @"SELECT TOP 1 *
FROM Assessment
ORDER BY AssessmentID DESC";
        }

        public static TheoryData<int,ExpectedPropertyValuesCollection,ExpectedPropertyValuesCollection,
            ExpectedPropertyValuesCollection> GetLoadPreRuleConditionsData()
        {
            var assessmentIdAndData =
                new TheoryData<int, ExpectedPropertyValuesCollection, ExpectedPropertyValuesCollection,
                    ExpectedPropertyValuesCollection>();
            IEnumerable<dynamic> retrievedExceptionData, retrievedExceptionCaseData, retrievedExceptionConditionData;
            int assessmentId;
            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                using (var multiple =
                    cn.QueryMultiple(GetLoadPreRuleConditionsSql(), commandType: System.Data.CommandType.Text))
                {
                    {
                        assessmentId = multiple.ReadFirst<int>();
                        retrievedExceptionData = multiple.Read();
                        retrievedExceptionCaseData = multiple.Read();
                        retrievedExceptionConditionData = multiple.Read();
                    }
                }

                var exceptionData = new ExpectedPropertyValuesCollection();
                var exceptionCaseData = new ExpectedPropertyValuesCollection();
                var exceptionConditionData = new ExpectedPropertyValuesCollection();

                assessmentIdAndData.Add(assessmentId, exceptionData, exceptionCaseData, exceptionConditionData);

                foreach (var row in retrievedExceptionData)
                {
                    int exceptionId = 0;
                    var convertedRow = (IDictionary<string, object>) row;
                    var expectedPropertyValues = new ExpectedPropertyValues();
                    var columnNames = convertedRow.Keys;
                    foreach (var column in columnNames)
                    {
                        expectedPropertyValues.Add(column.ToLowerInvariant(), convertedRow[column]);
                        if (column.Equals("ExceptionID", StringComparison.InvariantCultureIgnoreCase))
                            exceptionId = (int) convertedRow[column];
                    }

                    exceptionData.Add(exceptionId, expectedPropertyValues);
                }

                foreach (var row in retrievedExceptionCaseData)
                {
                    int exceptionCaseId = 0;
                    var convertedRow = (IDictionary<string, object>) row;
                    var expectedPropertyValues = new ExpectedPropertyValues();
                    var columnNames = convertedRow.Keys;
                    foreach (var column in columnNames)
                    {
                        expectedPropertyValues.Add(column.ToLowerInvariant(), convertedRow[column]);
                        if (column.Equals("ExceptionCaseID", StringComparison.InvariantCultureIgnoreCase))
                            exceptionCaseId = (int) convertedRow[column];
                    }

                    exceptionCaseData.Add(exceptionCaseId, expectedPropertyValues);
                }

                foreach (var row in retrievedExceptionConditionData)
                {
                    int exceptionConditionId = 0;

                    var convertedRow = (IDictionary<string, object>) row;
                    var expectedPropertyValues = new ExpectedPropertyValues();
                    var columnNames = convertedRow.Keys;
                    foreach (var column in columnNames)
                    {
                        expectedPropertyValues.Add(column.ToLowerInvariant(), convertedRow[column]);
                        if (column.Equals("ExceptionConditionID", StringComparison.InvariantCultureIgnoreCase))
                            exceptionConditionId = (int) convertedRow[column];
                    }

                    exceptionConditionData.Add(exceptionConditionId, expectedPropertyValues);
                }

                return assessmentIdAndData;
            }
        }

        private static string GetLoadPreRuleConditionsSql()
        {
            return @"SELECT TOP 1 AssessmentID
FROM Assessment
ORDER BY AssessmentID DESC;

SELECT e.[ExceptionID]
      ,e.[Name]
      ,e.[IsSystem]
      ,e.[ConditionStoredProcedure]
      ,e.[IsDeleted]
      ,e.[CreateDate]
      ,e.[LastModifiedDate]
      ,e.[ConditionStoredMethod]
      ,e.[ExecutionOrder]
      ,e.[Active]
      ,e.[ExceptionGroup]
      ,e.[PreRule]
  FROM [dbo].[Exception] e
  WHERE e.IsDeleted = 0 AND e.Active = 1 AND e.PreRule = 1
  ORDER BY e.ExecutionOrder;

SELECT ec.[ExceptionCaseID]
      ,ec.[ExceptionID]
      ,ec.[ExceptionAccessGroupID]
      ,ec.[Name]
      ,ec.[IsDeleted]
      ,ec.[CreatedByUserID]
      ,ec.[CreateDate]
      ,ec.[LastModifiedByUserID]
      ,ec.[LastModifiedDate]
      ,ec.[ExceptionViewID]
      ,ec.[AllowOverride]
      ,ec.[Description]
      ,ec.[DisplayInSummary]
      ,ec.[DisplayDetailedExceptionInSummary]
  FROM [dbo].[Exception] e
  INNER JOIN [dbo].[ExceptionCase] ec ON e.ExceptionID = ec.ExceptionID
  WHERE e.IsDeleted = 0 AND e.Active = 1 AND e.PreRule = 1 AND ec.IsDeleted = 0
  ORDER BY e.ExecutionOrder, ec.ExceptionCaseID;

SELECT eco.[ExceptionConditionID],eco.[ExceptionAttributeMetadataID],eco.[Operator],eco.[ValueFrom],eco.[ValueTo]
,eam.[Key] AS AttributeKey, eam.ColumnName AS AttributeColumn, eam.DataType AS DataType,
eam.LookupTable AS LookupTable, eam.LookupColumn AS LookupColumn, eam.LookupReturnColumn AS LookupReturnColumn,
eam.ExceptionViewID
FROM [dbo].[Exception] e
INNER JOIN [dbo].[ExceptionCase] ec ON e.ExceptionID = ec.ExceptionID
LEFT JOIN [dbo].[ExceptionCondition] eco ON ec.ExceptionCaseID =  eco.ExceptionCaseID
LEFT JOIN [dbo].[ExceptionAttributeMetadata]  eam ON eco.ExceptionAttributeMetadataID = eam.ExceptionAttributeMetadataID
WHERE e.IsDeleted = 0 AND e.Active = 1 AND e.PreRule = 1 AND ec.IsDeleted = 0
ORDER BY e.ExecutionOrder, ec.ExceptionCaseID, eco.ExceptionConditionID;";
        }

        //

        public static TheoryData<int, ExpectedPropertyValuesCollection, ExpectedPropertyValuesCollection,
            ExpectedPropertyValuesCollection> GetLoadAssessmentExceptionsData()
        {
            var assessmentIdAndData =
                new TheoryData<int, ExpectedPropertyValuesCollection, ExpectedPropertyValuesCollection,
                    ExpectedPropertyValuesCollection>();
            IEnumerable<dynamic> retrievedExceptionData, retrievedExceptionCaseData, retrievedExceptionConditionData;
            int assessmentId;
            using (var cn = new SqlConnection(ConnectionString))
            {
                cn.Open();
                using (var multiple =
                    cn.QueryMultiple(GetLoadAssessmentExceptionsSql(), commandType: System.Data.CommandType.Text))
                {
                    assessmentId = multiple.ReadFirst<int>();
                    retrievedExceptionData = multiple.Read();
                    retrievedExceptionCaseData = multiple.Read();
                    retrievedExceptionConditionData = multiple.Read();
                }
            }

            var exceptionData = new ExpectedPropertyValuesCollection();
            var exceptionCaseData = new ExpectedPropertyValuesCollection();
            var exceptionConditionData = new ExpectedPropertyValuesCollection();

            assessmentIdAndData.Add(assessmentId, exceptionData, exceptionCaseData, exceptionConditionData);

            foreach (var row in retrievedExceptionData)
            {
                int exceptionId = 0;
                var convertedRow = (IDictionary<string, object>) row;
                var expectedPropertyValues = new ExpectedPropertyValues();
                var columnNames = convertedRow.Keys;
                foreach (var column in columnNames)
                {
                    expectedPropertyValues.Add(column.ToLowerInvariant(), convertedRow[column]);
                    if (column.Equals("ExceptionID", StringComparison.InvariantCultureIgnoreCase))
                        exceptionId = (int)convertedRow[column];
                }

                exceptionData.Add(exceptionId, expectedPropertyValues);
            }

            foreach (var row in retrievedExceptionCaseData)
            {
                int exceptionCaseID = 0;
                var convertedRow = (IDictionary<string, object>) row;
                var expectedPropertyValues = new ExpectedPropertyValues();
                var columnNames = convertedRow.Keys;
                foreach (var column in columnNames)
                {
                    expectedPropertyValues.Add(column.ToLowerInvariant(), convertedRow[column]);
                    if (column.Equals("ExceptionCaseID", StringComparison.InvariantCultureIgnoreCase))
                        exceptionCaseID = (int) convertedRow[column];
                }

                exceptionCaseData.Add(exceptionCaseID, expectedPropertyValues);
            }

            foreach (var row in retrievedExceptionConditionData)
            {
                int exceptionConditionID = 0;

                var convertedRow = (IDictionary<string, object>) row;
                var expectedPropertyValues = new ExpectedPropertyValues();
                var columnNames = convertedRow.Keys;
                foreach (var column in columnNames)
                {
                    expectedPropertyValues.Add(column.ToLowerInvariant(), convertedRow[column]);
                    if (column.Equals("ExceptionConditionID", StringComparison.InvariantCultureIgnoreCase))
                        exceptionConditionID = (int) convertedRow[column];
                }

                exceptionConditionData.Add(exceptionConditionID, expectedPropertyValues);
            }

            return assessmentIdAndData;
        }

        private static string GetLoadAssessmentExceptionsSql()
        {
            return @"SELECT TOP 1 AssessmentID
FROM Assessment
ORDER BY AssessmentID DESC;

SELECT e.[ExceptionID]
      ,e.[Name]
      ,e.[IsSystem]
      ,e.[ConditionStoredProcedure]
      ,e.[IsDeleted]
      ,e.[CreateDate]
      ,e.[LastModifiedDate]
      ,e.[ConditionStoredMethod]
      ,e.[ExecutionOrder]
      ,e.[Active]
      ,e.[ExceptionGroup]
      ,e.[PreRule]
  FROM [dbo].[Exception] e
  WHERE e.IsDeleted = 0 AND e.Active = 1 AND e.PreRule = 0
  ORDER BY e.ExecutionOrder;

SELECT ec.[ExceptionCaseID]
      ,ec.[ExceptionID]
      ,ec.[ExceptionAccessGroupID]
      ,ec.[Name]
      ,ec.[IsDeleted]
      ,ec.[CreatedByUserID]
      ,ec.[CreateDate]
      ,ec.[LastModifiedByUserID]
      ,ec.[LastModifiedDate]
      ,ec.[ExceptionViewID]
      ,ec.[AllowOverride]
      ,ec.[Description]
      ,ec.[DisplayInSummary]
      ,ec.[DisplayDetailedExceptionInSummary]
  FROM [dbo].[Exception] e
  INNER JOIN [dbo].[ExceptionCase] ec ON e.ExceptionID = ec.ExceptionID
  WHERE e.IsDeleted = 0 AND e.Active = 1 AND e.PreRule = 0 AND ec.IsDeleted = 0
  ORDER BY e.ExecutionOrder, ec.ExceptionCaseID;

SELECT eco.[ExceptionConditionID],eco.[ExceptionAttributeMetadataID],eco.[Operator],eco.[ValueFrom],eco.[ValueTo]
,eam.[Key] AS AttributeKey, eam.ColumnName AS AttributeColumn, eam.DataType AS DataType,
eam.LookupTable AS LookupTable, eam.LookupColumn AS LookupColumn, eam.LookupReturnColumn AS LookupReturnColumn,
eam.ExceptionViewID
FROM [dbo].[Exception] e
INNER JOIN [dbo].[ExceptionCase] ec ON e.ExceptionID = ec.ExceptionID
INNER JOIN [dbo].[ExceptionCondition] eco ON ec.ExceptionCaseID =  eco.ExceptionCaseID
LEFT JOIN [dbo].[ExceptionAttributeMetadata]  eam ON eco.ExceptionAttributeMetadataID = eam.ExceptionAttributeMetadataID
WHERE e.IsDeleted = 0 AND e.Active = 1 AND e.PreRule = 0 AND ec.IsDeleted = 0
ORDER BY e.ExecutionOrder, ec.ExceptionCaseID, eco.ExceptionConditionID;";
        }
    }
}
