﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;
using ExceptionEngine;
using ExceptionEngine.Repositories;
using TestHelpers;
using Xunit;

namespace Tests.ExceptionEngineTests
{
    public class ExceptionRepositoryAutoDataAttribute : AutoDataAttribute
    {
        private const string ConnectionString = "Data Source = QASQL01.tsitel.com; Initial Catalog = Enterprise; User ID = iteladmin; Password = sxt56#L2A1NZ;";

        // see https://github.com/AutoFixture/AutoFixture/issues/843 for why func is used in constructor
        public ExceptionRepositoryAutoDataAttribute() :
            base(() =>
            {
                var fixture = new Fixture().Customize(new AutoMoqCustomization());

                fixture.Register<IExceptionRepository>(() => new ExceptionRepository(ConnectionString));

                return fixture;
            })
        {

        }
    }

    public class ExceptionProcessorTests
    {
        [Theory]
        [ExceptionRepositoryAutoData]
        [Trait("Category", "Integration")]
        public void InitialTestToGeneratePotentialApi(ExceptionProcessor exceptionProcessor)
        {
            var assessmentId = 2924040;

            var result = exceptionProcessor.ProcessExceptions(assessmentId);

            Assert.NotNull(result);
        }

        [Theory, AutoMoqData]
        [Trait("Category", "Unit")]
        public void QuickTestForExistingCodeInEnterprise(int key1, int key2)
        {
            var processedExceptions = new Dictionary<Int32, Object>();

            processedExceptions[key1] = null;
            processedExceptions[key2] = null;

            Assert.True(processedExceptions.ContainsKey(key1));
            Assert.True(processedExceptions.ContainsKey(key2));
        }
    }
}
